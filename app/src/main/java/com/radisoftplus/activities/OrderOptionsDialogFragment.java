package com.radisoftplus.activities;

import static android.content.Context.MODE_PRIVATE;
import static com.radisoftplus.utils.SaveLocalStorage.saveToSharedPreferences;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.radisoftplus.R;

public class OrderOptionsDialogFragment extends DialogFragment {

    public static final String SHARED_PREFS = "login_information";

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(requireContext()).inflate(R.layout.order_options_dialog, null);
        LinearLayout btnNewOrder = view.findViewById(R.id.btn_new_order);
        LinearLayout btnDraftOrder = view.findViewById(R.id.btn_draft_order);
        LinearLayout btnSentOrder = view.findViewById(R.id.btn_sent_order);
        LinearLayout btnOrderReport = view.findViewById(R.id.btn_order_report);

        // Set click listeners for the buttons to handle the actions
        btnNewOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Handle "New Order" action
                dismiss();

                SharedPreferences sh = requireContext().getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
                saveToSharedPreferences("find-word_area-work_area",sh.getString("work_area_t", ""), requireContext());
                saveToSharedPreferences("find-word_area-address",sh.getString("name", ""), requireContext());
                saveToSharedPreferences("find-customer-partner",null, requireContext());
                saveToSharedPreferences("find-customer-customer_name",null, requireContext());
                saveToSharedPreferences("order-delivery_date",null, requireContext());
                saveToSharedPreferences("order-payment_mode",null, requireContext());
                saveToSharedPreferences("order-emp_id_cr_team",null, requireContext());
                saveToSharedPreferences("order-list",null, requireContext());
                saveToSharedPreferences("back_where","radiant", requireContext());
                Intent intent = new Intent(requireContext(),OrderActivity.class);
                startActivity(intent);
            }
        });

        btnDraftOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Handle "Draft Order" action
                dismiss();
                saveToSharedPreferences("back_where","radiant", requireContext());
                startActivity(new Intent(requireContext(),OrderDraftActivity.class));
                requireActivity().finish();
            }
        });

        btnSentOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Handle "Sent Order" action
                dismiss();
                saveToSharedPreferences("back_where","radiant", requireContext());
                startActivity(new Intent(requireContext(),SentOrderActivity.class));
//                requireActivity().finish();
            }
        });

        btnOrderReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Handle "Order Report" action
                dismiss();
                saveToSharedPreferences("back_where","radiant", requireContext());
                startActivity(new Intent(requireContext(),OrderReportActivity.class));
//                requireActivity().finish();
            }
        });

        Dialog dialog = new Dialog(requireContext());
        dialog.setContentView(view);

        // Set the width of the dialog to 200dp
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams((int) getResources().getDimension(R.dimen.dialog_width), ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setLayout(layoutParams.width, layoutParams.height);

        // Set the dialog to be centered
        dialog.getWindow().setGravity(Gravity.CENTER);

        return dialog;
    }

    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        super.onDismiss(dialog);

        // Hide the darkOverlay when the dialog is dismissed
        ((HomeRadiantActivity) requireActivity()).hideDarkOverlay();
    }
}

