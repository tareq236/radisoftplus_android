package com.radisoftplus.activities;

import static com.radisoftplus.utils.SaveLocalStorage.getValueFromSharedPreferences;
import static com.radisoftplus.utils.SaveLocalStorage.saveToSharedPreferences;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.radisoftplus.R;
import com.radisoftplus.models.ApiLinkDetailsModel;
import com.radisoftplus.models.ApiMediaModel;
import com.radisoftplus.utils.ApiService;
import com.radisoftplus.utils.BusyDialog;
import com.radisoftplus.utils.RetroClient;
import com.radisoftplus.utils.UIContent;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TrainingProductDetailsCommonActivity extends AppCompatActivity {

    private static final String TAG = "TrainingProductDetailsCommonActivity";
    private Context mContext;
    UIContent uiContent;

    @BindView(R.id.menu)
    LinearLayout _menu;

    @BindView(R.id.ll_back)
    LinearLayout _back;

    @BindView(R.id.txt_title)
    TextView txt_title;

    List<ApiLinkDetailsModel.Results> rList = new ArrayList<>();
    RecyclerView rcvList;

    String item_name, id;

    @BindView(R.id.pdf)
    LinearLayout pdf;
    @BindView(R.id.pdf_1)
    LinearLayout pdf_1;
    @BindView(R.id.pdf_2)
    LinearLayout pdf_2;
    @BindView(R.id.video)
    LinearLayout video;
    //    @BindView(R.id.image)
//    LinearLayout image;

    @BindView(R.id.img_new_1)
    ImageView img_new_1;
    @BindView(R.id.img_new_2)
    ImageView img_new_2;
    @BindView(R.id.img_new_3)
    ImageView img_new_3;

    private BusyDialog mBusyDialog;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_training_product_details_common);
        ButterKnife.bind(this);
        mContext = this;
        uiContent = new UIContent(TrainingProductDetailsCommonActivity.this);

//        rcvList = findViewById(R.id.rcv_list);

        _menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TrainingProductDetailsCommonActivity.this, MainMenuActivity.class);
                startActivity(intent);
            }
        });
        _back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TrainingProductDetailsCommonActivity.this, PromotionalItemsSubActivity.class);
                startActivity(intent);
                finish();
            }
        });

        id = getValueFromSharedPreferences("training_product_id", TrainingProductDetailsCommonActivity.this);
        item_name = getValueFromSharedPreferences("training_product_name", TrainingProductDetailsCommonActivity.this);

        txt_title.setText(item_name);

        pdf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveToSharedPreferences("from_media_temp","TrainingProductDetailsCommonActivity",TrainingProductDetailsCommonActivity.this);
                saveToSharedPreferences("media_activity_media_temp","common",TrainingProductDetailsCommonActivity.this);
                saveToSharedPreferences("media_type_activity_media_temp","pdf",TrainingProductDetailsCommonActivity.this);
                saveToSharedPreferences("media_type_title","Sales Aid Brief",TrainingProductDetailsCommonActivity.this);
                Intent intent = new Intent(TrainingProductDetailsCommonActivity.this, MediaListActivity.class);
                startActivity(intent);
            }
        });
        pdf_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveToSharedPreferences("from_media_temp","TrainingProductDetailsCommonActivity",TrainingProductDetailsCommonActivity.this);
                saveToSharedPreferences("media_activity_media_temp","common1",TrainingProductDetailsCommonActivity.this);
                saveToSharedPreferences("media_type_activity_media_temp","pdf",TrainingProductDetailsCommonActivity.this);
                saveToSharedPreferences("media_type_title","Brand Reminder Brief",TrainingProductDetailsCommonActivity.this);
                Intent intent = new Intent(TrainingProductDetailsCommonActivity.this, MediaListActivity.class);
                startActivity(intent);
            }
        });
        pdf_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveToSharedPreferences("from_media_temp","TrainingProductDetailsCommonActivity",TrainingProductDetailsCommonActivity.this);
                saveToSharedPreferences("media_activity_media_temp","common2",TrainingProductDetailsCommonActivity.this);
                saveToSharedPreferences("media_type_activity_media_temp","pdf",TrainingProductDetailsCommonActivity.this);
                saveToSharedPreferences("media_type_title","Campaigns",TrainingProductDetailsCommonActivity.this);
                Intent intent = new Intent(TrainingProductDetailsCommonActivity.this, MediaListActivity.class);
                intent.putExtra("title","Campaigns");
                startActivity(intent);
            }
        });
//        image.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                saveToSharedPreferences("from_media_temp","TrainingProductDetailsCommonActivity",TrainingProductDetailsCommonActivity.this);
//                saveToSharedPreferences("media_activity_media_temp","training",TrainingProductDetailsCommonActivity.this);
//                saveToSharedPreferences("media_type_activity_media_temp","image",TrainingProductDetailsCommonActivity.this);
//                Intent intent = new Intent(TrainingProductDetailsCommonActivity.this, MediaActivity.class);
//                startActivity(intent);
//            }
//        });
        video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveToSharedPreferences("from_media_temp","TrainingProductDetailsCommonActivity",TrainingProductDetailsCommonActivity.this);
                saveToSharedPreferences("media_activity_media_temp","training",TrainingProductDetailsCommonActivity.this);
                saveToSharedPreferences("media_type_activity_media_temp","video",TrainingProductDetailsCommonActivity.this);
                saveToSharedPreferences("media_type_title","e-SA/Video",TrainingProductDetailsCommonActivity.this);
                Intent intent = new Intent(TrainingProductDetailsCommonActivity.this, MediaActivity.class);
                startActivity(intent);
            }
        });

        loadActivityFromApi();
    }


    public void loadActivityFromApi(){
        mBusyDialog = new BusyDialog(mContext, false, "");
        mBusyDialog.show();
//        final ProgressDialog progressDialog = new ProgressDialog(TrainingProductDetailsCommonActivity.this, R.style.AppTheme_Dark_Dialog);
//        progressDialog.setIndeterminate(true);
//        progressDialog.setMessage("Data loading...");
//        progressDialog.show();

        SharedPreferences sh = getSharedPreferences("login_information", MODE_PRIVATE);
        String team = sh.getString("group_name", "");
        String month = getValueFromSharedPreferences("Promotional_item_month_name", TrainingProductDetailsCommonActivity.this);

        ApiService api = RetroClient.getApiService();
        Call<ApiMediaModel> call = api.API_TRAINING_PRODUCT_DETAILS_COMMON_CALL(team,month);
        call.enqueue(new Callback<ApiMediaModel>() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onResponse(Call<ApiMediaModel> call, Response<ApiMediaModel> response) {

                if (response.isSuccessful()) {
                    Boolean success = response.body().getSuccess();
                    if(success){
                        img_new_1.setVisibility(View.GONE);
                        img_new_2.setVisibility(View.GONE);
                        img_new_3.setVisibility(View.GONE);

                        GsonBuilder gsonBuilder = new GsonBuilder();
                        Gson gson = gsonBuilder.create();
                        String JSONString = gson.toJson(response.body().getResults());
                        saveToSharedPreferences("product_details",JSONString.toString(),TrainingProductDetailsCommonActivity.this);

                        for (int i=0;i<response.body().getResults().size();i++){
                            if(response.body().getResults().get(i).getItem_type().equals("common")){
                                if(response.body().getResults().get(i).getIs_new().equals("New")){
                                    img_new_1.setVisibility(View.VISIBLE);
                                }
                            }
                            if(response.body().getResults().get(i).getItem_type().equals("common1")){
                                if(response.body().getResults().get(i).getIs_new().equals("New")){
                                    img_new_2.setVisibility(View.VISIBLE);
                                }
                            }
                            if(response.body().getResults().get(i).getItem_type().equals("common2")){
                                if(response.body().getResults().get(i).getIs_new().equals("New")){
                                    img_new_3.setVisibility(View.VISIBLE);
                                }
                            }
                        }

                        mBusyDialog.dismiss();
                    }else{
                        onFailed();
                        mBusyDialog.dismiss();
                        uiContent.showExitDialog(TrainingProductDetailsCommonActivity.this,"Error",response.body().getMessage());
                    }
                } else {
                    onFailed();
                    mBusyDialog.dismiss();
                    uiContent.showExitDialog(TrainingProductDetailsCommonActivity.this, "Error", "Server Error !");
                }
            }

            @Override
            public void onFailure(Call<ApiMediaModel> call, Throwable t) {
                Toast.makeText(getBaseContext(), t.getMessage(), Toast.LENGTH_LONG).show();
                mBusyDialog.dismiss();
                uiContent.showExitDialog(TrainingProductDetailsCommonActivity.this, "Warning", "Please check network connection");
            }
        });
    }


    public void onFailed() {
        Toast.makeText(getBaseContext(), "Error", Toast.LENGTH_LONG).show();
    }
}