package com.radisoftplus.activities;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.radisoftplus.R;

import static com.radisoftplus.utils.SaveLocalStorage.saveToSharedPreferences;

public class MainMenuActivity extends AppCompatActivity {

    private static final String TAG = "MainMenuActivity";
    private Context mContext;

    @BindView(R.id.ll_back)
    LinearLayout back;
    @BindView(R.id.btnLogout)
    LinearLayout btn_logout;
    @BindView(R.id.btnChemistShop)
    LinearLayout btnChemistShop;
    @BindView(R.id.txv_doctor_name)
    TextView txv_doctor_name;
    @BindView(R.id.btn_order)
    LinearLayout btn_order;
    @BindView(R.id.main_menu)
    LinearLayout main_menu;

    @BindView(R.id.btn_back1)
    LinearLayout btn_back1;

    @BindView(R.id.order_submenu)
    LinearLayout order_submenu;

    @BindView(R.id.btn_new_order)
    LinearLayout btn_new_order;
    @BindView(R.id.btn_draft_order)
    LinearLayout btn_draft_order;
    @BindView(R.id.btn_sent_report)
    LinearLayout btn_sent_report;

    @BindView(R.id.btn_oder_report)
    LinearLayout btn_oder_report;

    @BindView(R.id.btn_notification)
    LinearLayout btn_notification;

    @BindView(R.id.btn_contact)
    LinearLayout btn_contact;

    @BindView(R.id.btn_dcr)
    LinearLayout btn_dcr;

    @BindView(R.id.dcr_submenu)
    LinearLayout dcr_submenu;

    @BindView(R.id.btn_daily_call)
    LinearLayout btn_daily_call;
    @BindView(R.id.btn_dcr_back)
    LinearLayout btn_dcr_back;
    @BindView(R.id.btn_dcr_draft)
    LinearLayout btn_dcr_draft;
    @BindView(R.id.btn_dcr_sent_report)
    LinearLayout btn_dcr_sent_report;

    public static final String SHARED_PREFS = "login_information";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);
        mContext = this;
        ButterKnife.bind(this);

        SharedPreferences sh = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        txv_doctor_name.setText(sh.getString("user_name", ""));


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        btn_daily_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveToSharedPreferences("dcr-call-date",null, mContext);
                saveToSharedPreferences("dcr-sessions",null, mContext);
                saveToSharedPreferences("dcr-chamber-type",null, mContext);
                saveToSharedPreferences("dcr-call-type",null, mContext);
                saveToSharedPreferences("dcr-dr-child-id",null, mContext);
                saveToSharedPreferences("dcr-remarks",null, mContext);
                saveToSharedPreferences("find-doctor-name", null, mContext);
                saveToSharedPreferences("find-dr-child-id", null, mContext);
                saveToSharedPreferences("br-list",null, mContext);

                Intent intent = new Intent(mContext, DcrScreenOneActivity.class);
                startActivity(intent);
            }
        });

        btn_dcr_draft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, DcrDraftActivity.class);
                startActivity(intent);
            }
        });


        btn_dcr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                main_menu.setVisibility(View.GONE);
                dcr_submenu.setVisibility(View.VISIBLE);
            }
        });
        
        btn_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                new SweetAlertDialog(mContext, SweetAlertDialog.WARNING_TYPE)
//                        .setTitleText("Under construction ")
//                        .setConfirmText("Close").show();
                main_menu.setVisibility(View.GONE);
                order_submenu.setVisibility(View.VISIBLE);
            }
        });

        btn_notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new SweetAlertDialog(mContext, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("Under construction ")
                        .setConfirmText("Close").show();
            }
        });

        btn_contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new SweetAlertDialog(mContext, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("Under construction ")
                        .setConfirmText("Close").show();
            }
        });


        btn_back1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                main_menu.setVisibility(View.VISIBLE);
                order_submenu.setVisibility(View.GONE);
            }
        });

        btn_dcr_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                main_menu.setVisibility(View.VISIBLE);
                dcr_submenu.setVisibility(View.GONE);
            }
        });

        btnChemistShop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainMenuActivity.this, GetCustomerLocationActivity.class));
            }
        });

        btn_new_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sh = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
                saveToSharedPreferences("find-word_area-work_area",sh.getString("work_area_t", ""), mContext);
                saveToSharedPreferences("find-word_area-address",sh.getString("address", ""), mContext);
                saveToSharedPreferences("find-customer-partner",null, mContext);
                saveToSharedPreferences("find-customer-customer_name",null, mContext);
                saveToSharedPreferences("order-delivery_date",null, mContext);
                saveToSharedPreferences("order-payment_mode",null, mContext);
                saveToSharedPreferences("order-emp_id_cr_team",null, mContext);
                saveToSharedPreferences("order-list",null, mContext);

                Intent intent = new Intent(MainMenuActivity.this,OrderActivity.class);
                intent.putExtra("is_draft", false);
                startActivity(intent);
            }
        });
        btn_draft_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainMenuActivity.this,OrderDraftActivity.class));
            }
        });

        btn_sent_report.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainMenuActivity.this,SentOrderActivity.class));
            }
        });

        btn_oder_report.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainMenuActivity.this,OrderReportActivity.class));
            }
        });

        btn_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences preferences =getSharedPreferences(SHARED_PREFS,Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.clear();
                editor.apply();
                finish();

                Intent intent = new Intent(mContext, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }
}
