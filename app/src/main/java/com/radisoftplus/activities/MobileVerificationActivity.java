package com.radisoftplus.activities;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.github.ybq.android.spinkit.sprite.Sprite;
import com.github.ybq.android.spinkit.style.DoubleBounce;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.gson.JsonObject;
import com.radisoftplus.R;
import com.radisoftplus.models.ApiLoginModel;
import com.radisoftplus.utils.ApiService;
import com.radisoftplus.utils.BusyDialog;
import com.radisoftplus.utils.Netinfo;
import com.radisoftplus.utils.RetroClient;

import java.util.concurrent.TimeUnit;

public class MobileVerificationActivity extends AppCompatActivity {

    private static final String TAG = "MobileVerificationActivity";
    private Context mContext;

    @BindView(R.id.et_verify_code)
    EditText et_verify_code;
    @BindView(R.id.btn_verify)
    Button btn_verify;

    private String mobile_number;
    FirebaseAuth mAuth;

    private BusyDialog mBusyDialog;

    public static final String SHARED_PREFS = "login_information";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mobile_verification);
        ButterKnife.bind(this);
        mContext=this;

        mAuth=FirebaseAuth.getInstance();

        Intent intent = getIntent();
        mobile_number = intent.getStringExtra("mobile_number");

        btn_verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               verify();
            }
        });

        if (!checkInternet()) {
            onLoginFailed();
            return;
        }

    }

    public void verify() {
        if (!validate()) {
            onLoginFailed();
            return;
        }

        if (!checkInternet()) {
            onLoginFailed();
            return;
        }
        mBusyDialog = new BusyDialog(mContext, false, "Checking Code...");
        mBusyDialog.show();


        ApiService api = RetroClient.getApiService();
        JsonObject body = new JsonObject();
        body.addProperty("mobile_number", mobile_number);
        body.addProperty("opt",et_verify_code.getText().toString());
        body.addProperty("device_type","android");
        Call<ApiLoginModel> call = api.API_LOGIN_MODEL_CALL(body);
        call.enqueue(new Callback<ApiLoginModel>() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onResponse(Call<ApiLoginModel> call, Response<ApiLoginModel> response) {

                if (response.isSuccessful()) {
                    Boolean success = response.body().getSuccess();
                    if(success){
                        mBusyDialog.dismiss();
                        Intent intent = new Intent(mContext, ResetPasswordActivity.class);
                        intent.putExtra("work_area_t", response.body().getResult().getWorkAreaT().toString());
                        startActivity(intent);
                        finish();
                    }else {
                        onLoginFailed();
                        String message = response.body().getMessage();
                        mBusyDialog.dismiss();
                        new SweetAlertDialog(mContext, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText(message)
                                .setConfirmText("Close").show();
                    }
                } else {
                    onLoginFailed();
                    mBusyDialog.dismiss();
                    new SweetAlertDialog(mContext, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Server error !")
                            .setConfirmText("Close").show();
                }
            }

            @Override
            public void onFailure(Call<ApiLoginModel> call, Throwable t) {
                onLoginFailed();
                mBusyDialog.dismiss();
                new SweetAlertDialog(mContext, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("Please check network connection")
                        .setConfirmText("Close").show();
            }
        });

    }
//    public void verify() {
//
//        if (!validate()) {
//            onLoginFailed();
//            return;
//        }
//
//        btn_verify.setEnabled(false);
//        btn_verify.setBackgroundColor(Color.GRAY);
//
//        PhoneAuthCredential credential=PhoneAuthProvider.getCredential(otpid,et_verify_code.getText().toString());
//        signInWithPhoneAuthCredential(credential);
//    }

    public boolean validate() {
        boolean valid = true;

        String verifyCode = et_verify_code.getText().toString();

        if (verifyCode.isEmpty()) {
            new SweetAlertDialog(mContext, SweetAlertDialog.WARNING_TYPE)
                    .setTitleText("Please enter verification code !")
                    .setConfirmText("Close").show();
            valid = false;
        }

        return valid;
    }

    public void onLoginFailed() {
        btn_verify.setEnabled(true);
        btn_verify.setBackgroundColor(getResources().getColor(R.color.green));
    }

    public boolean checkInternet(){
        boolean internet = true;
        if (!Netinfo.isOnline(mContext)) {
            new SweetAlertDialog(mContext, SweetAlertDialog.WARNING_TYPE)
                    .setTitleText("Please check the internet connection !")
                    .setConfirmText("Close").show();
            onLoginFailed();
            internet = false;
        }
        return internet;
    }
}
