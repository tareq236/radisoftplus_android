package com.radisoftplus.activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.radisoftplus.R;
import com.radisoftplus.adapter.SearchChemistAdapter;
import com.radisoftplus.adapter.SearchProductAdapter;
import com.radisoftplus.models.ApiFindMaterialModel;
import com.radisoftplus.models.OrderListModel;
import com.radisoftplus.utils.ApiService;
import com.radisoftplus.utils.RetroClient;
import com.radisoftplus.utils.UIContent;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.radisoftplus.utils.SaveLocalStorage.getValueFromSharedPreferences;

public class SearchProductActivity extends AppCompatActivity {

    private static final String TAG = "SearchProductActivity";
    private Context mContext;
    UIContent uiContent;

    @BindView(R.id.ll_back)
    LinearLayout _back;

    @BindView(R.id.et_product)
    EditText et_product;

    List<ApiFindMaterialModel.Result> rList = new ArrayList<>();
    RecyclerView rcvList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_product);
        ButterKnife.bind(this);
        mContext = this;
        uiContent = new UIContent(SearchProductActivity.this);

        rcvList = findViewById(R.id.rcv_list);

        _back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SearchProductActivity.this, OrderActivity.class);
                startActivity(intent);
                finish();
            }
        });

        et_product.requestFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(et_product, InputMethodManager.SHOW_IMPLICIT);

        et_product.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {}

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length() != 0){
                    loadDataFromApi(s.toString());
                }else{
                    rList.clear();
                    rcvList.setLayoutManager(new LinearLayoutManager(SearchProductActivity.this));
                    SearchProductAdapter reAdapter = new SearchProductAdapter(rList,SearchProductActivity.this);
                    rcvList.setAdapter(reAdapter);
                    rcvList.setNestedScrollingEnabled(true);
                    rcvList.setHasFixedSize(true);
                }
            }
        });
    }

    public void loadDataFromApi(String s){
        SharedPreferences sh = getSharedPreferences("login_information", MODE_PRIVATE);

        ApiService api = RetroClient.getApiService();
        JsonObject body = new JsonObject();
        body.addProperty("material_name", s);
        Call<ApiFindMaterialModel> call = api.API_FIND_MATERIAL_MODEL_CALL(body);
        call.enqueue(new Callback<ApiFindMaterialModel>() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onResponse(Call<ApiFindMaterialModel> call, Response<ApiFindMaterialModel> response) {

                if (response.isSuccessful()) {
                    Boolean success = response.body().getSuccess();
                    if(success){

                        rList = response.body().getResult();
                        if(rList.size()>0) {
                            rcvList.setLayoutManager(new LinearLayoutManager(SearchProductActivity.this));
                            SearchProductAdapter reAdapter = new SearchProductAdapter(rList,SearchProductActivity.this);
                            rcvList.setAdapter(reAdapter);
                            rcvList.setNestedScrollingEnabled(true);
                            rcvList.setHasFixedSize(true);
                        }
                    }else{
                        onFailed();
                        uiContent.showExitDialog(SearchProductActivity.this,"Error",response.body().getMessage());
                    }
                } else {
                    onFailed();
                    uiContent.showExitDialog(SearchProductActivity.this, "Error", "Server Error !");
                }
            }

            @Override
            public void onFailure(Call<ApiFindMaterialModel> call, Throwable t) {
                Toast.makeText(getBaseContext(), t.getMessage(), Toast.LENGTH_LONG).show();
                uiContent.showExitDialog(SearchProductActivity.this, "Warning", "Please check network connection");
            }
        });
    }

    public void onFailed() {
        Toast.makeText(getBaseContext(), "Error", Toast.LENGTH_LONG).show();
    }


}