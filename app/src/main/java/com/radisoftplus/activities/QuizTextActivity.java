package com.radisoftplus.activities;

import static com.radisoftplus.utils.SaveLocalStorage.getValueFromSharedPreferences;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.card.MaterialCardView;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.radisoftplus.R;
import com.radisoftplus.models.ApiAnswareModel;
import com.radisoftplus.models.ApiQuestionModel;
import com.radisoftplus.utils.ApiService;
import com.radisoftplus.utils.RetroClient;
import com.radisoftplus.utils.UIContent;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class QuizTextActivity extends AppCompatActivity {

    private static final String TAG = "QuizTextActivity";
    private Context mContext;
    UIContent uiContent;

    @BindView(R.id.back)
    LinearLayout _back;

    @BindView(R.id.ll_congratulation)
    LinearLayout ll_congratulation;
    @BindView(R.id.ll_contain)
    LinearLayout ll_contain;

    @BindView(R.id.ll_question_1)
    LinearLayout ll_question_c_q1;

    @BindView(R.id.ll_question_q1)
    LinearLayout ll_question_q1;
    @BindView(R.id.txt_question_q1)
    TextView txt_question_q1;
    @BindView(R.id.option_1_q1)
    CheckBox option_1_q1;
    @BindView(R.id.option_2_q1)
    CheckBox option_2_q1;
    @BindView(R.id.option_3_q1)
    CheckBox option_3_q1;
    @BindView(R.id.option_4_q1)
    CheckBox option_4_q1;
    @BindView(R.id.option_5_q1)
    CheckBox option_5_q1;
    @BindView(R.id.option_6_q1)
    CheckBox option_6_q1;

    @BindView(R.id.btn_prev)
    Button btn_prev;
    @BindView(R.id.btn_submit)
    Button btn_submit;
    @BindView(R.id.btn_next)
    Button btn_next;

    @BindView(R.id.btn_cc_prev)
    MaterialCardView btn_cc_prev;
    @BindView(R.id.btn_cc_submit)
    MaterialCardView btn_cc_submit;
    @BindView(R.id.btn_cc_next)
    MaterialCardView btn_cc_next;

    @BindView(R.id.ll_contener)
    LinearLayout ll_contener;

    @BindView(R.id.edit_text_q1)
    TextInputEditText edit_text_q1;

    List<ApiQuestionModel.Results> results = new ArrayList<>();
    private Integer result_length;
    private Integer current_position;
    private String option_ans_q1 = "";
    private String option_ans_q2 = "";
    private String option_ans_q3 = "";
    private String option_ans_q4 = "";
    private String option_ans_q5 = "";
    private String option_ans_q6 = "";

    private int[][] intArrayQuestion;
//    private String[] stringArrayQuestion;
    private String[] stringArrayQuestion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_text);
        ButterKnife.bind(this);
        mContext = this;
        uiContent = new UIContent(QuizTextActivity.this);

        _back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(QuizTextActivity.this, MainMenuActivity.class);
                startActivity(intent);
                finish();
            }
        });
        btn_prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                option_1_q1.setChecked(false);
//                option_2_q1.setChecked(false);
//                option_3_q1.setChecked(false);
//                option_4_q1.setChecked(false);
//                option_5_q1.setChecked(false);
//                option_6_q1.setChecked(false);
                stringArrayQuestion[current_position] = edit_text_q1.getText().toString();
                edit_text_q1.setText("");
                current_position = current_position - 1;
                setQuestion(current_position);
            }
        });
        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                option_1_q1.setChecked(false);
//                option_2_q1.setChecked(false);
//                option_3_q1.setChecked(false);
//                option_4_q1.setChecked(false);
//                option_5_q1.setChecked(false);
//                option_6_q1.setChecked(false);
                stringArrayQuestion[current_position] = edit_text_q1.getText().toString();
                edit_text_q1.setText("");
                current_position = current_position + 1;
                setQuestion(current_position);
            }
        });

        option_1_q1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                intArrayQuestion[current_position][0] = 1;
            }
        });
        option_2_q1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                intArrayQuestion[current_position][1] = 2;
            }
        });
        option_3_q1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                intArrayQuestion[current_position][2] = 3;
            }
        });
        option_4_q1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                intArrayQuestion[current_position][3] = 4;
            }
        });
        option_5_q1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                intArrayQuestion[current_position][4] = 5;
            }
        });
        option_6_q1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                intArrayQuestion[current_position][5] = 6;
            }
        });

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stringArrayQuestion[current_position] = edit_text_q1.getText().toString();
                save();

            }
        });

        loadData();
    }

    public void loadData(){

        final ProgressDialog progressDialog = new ProgressDialog(QuizTextActivity.this, R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        ApiService api = RetroClient.getApiService();

        JsonObject body = new JsonObject();
        String user_id = getValueFromSharedPreferences("id", QuizTextActivity.this);
        body.addProperty("user_id", user_id);
        String quizs_id = getValueFromSharedPreferences("quizs_id", mContext);
        body.addProperty("quizs_id", quizs_id);

        Call<ApiQuestionModel> call = api.API_QUIZ_QUESTION_MODEL_CALL(body);
        call.enqueue(new Callback<ApiQuestionModel>() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onResponse(Call<ApiQuestionModel> call, Response<ApiQuestionModel> response) {

                if (response.isSuccessful()) {
                    Boolean success = response.body().getSuccess();
                    if(success){

                        ll_contener.setVisibility(View.VISIBLE);

                        results = response.body().getResults();
                        if (results.size() != 0){

//                            boolean already = false;
//                            for(int cq=0;cq<results.size();cq++){
//                                if(results.get(cq).getIsAnsAlrady() == 1){
//                                    already = true;
//                                }
//                            }
//                            if(already){
//                                String ans = response.body().getAns();
//                                onAlreadyAns();
//                            }else{
//                                intArrayQuestion = new int[results.size()][6];
//                                result_length = results.size();
//                                current_position=0;
//                                setQuestion(0);
//                            }

                            stringArrayQuestion = new String[results.size()];
                            intArrayQuestion = new int[results.size()][6];
                            result_length = results.size();
                            current_position=0;
                            setQuestion(0);

                        }

                        progressDialog.dismiss();
                    }else {
                        String message = response.body().getMessage();
                        btn_submit.setVisibility(View.GONE);
                        progressDialog.dismiss();
                        uiContent.showExitDialog(QuizTextActivity.this,"",message);
                    }
                } else {
                    progressDialog.dismiss();
                    uiContent.showExitDialog(QuizTextActivity.this, "Error", "Server error !");
                }
            }

            @Override
            public void onFailure(Call<ApiQuestionModel> call, Throwable t) {
                progressDialog.dismiss();
                uiContent.showExitDialog(QuizTextActivity.this, "Warning", "Please check network connection");
            }
        });



    }

    public void setQuestion(int position) {
        if(position == 0){
            btn_prev.setVisibility(View.INVISIBLE);
            btn_cc_prev.setVisibility(View.INVISIBLE);
            if(result_length-1 != 0){
                btn_submit.setVisibility(View.INVISIBLE);
                btn_cc_submit.setVisibility(View.INVISIBLE);
            }
            btn_next.setVisibility(View.VISIBLE);
            btn_cc_next.setVisibility(View.VISIBLE);
            if(result_length == 1){
                btn_next.setVisibility(View.INVISIBLE);
                btn_cc_next.setVisibility(View.INVISIBLE);
            }
        }else if(position < result_length-1){
            btn_prev.setVisibility(View.VISIBLE);
            btn_cc_prev.setVisibility(View.VISIBLE);
            btn_next.setVisibility(View.VISIBLE);
            btn_cc_next.setVisibility(View.VISIBLE);
            btn_submit.setVisibility(View.INVISIBLE);
            btn_cc_submit.setVisibility(View.INVISIBLE);
        }else if(position == result_length-1){
            btn_prev.setVisibility(View.VISIBLE);
            btn_cc_prev.setVisibility(View.VISIBLE);
            btn_next.setVisibility(View.INVISIBLE);
            btn_cc_next.setVisibility(View.INVISIBLE);
            btn_submit.setVisibility(View.VISIBLE);
            btn_cc_submit.setVisibility(View.VISIBLE);
        }


        txt_question_q1.setText("Q."+(position+1)+"/"+result_length+" "+results.get(position).getTextQuestion());
        if(stringArrayQuestion[current_position] != null){
            edit_text_q1.setText(stringArrayQuestion[position]);
        }
//        List<ApiQuestionModel.Option> options_q1 = new ArrayList<>();
//        options_q1 = results.get(position).getOptions();
//        for(int i=0;i<options_q1.size();i++){
//            if(i==0){
//                if(options_q1.get(i).getOption().equals("")) {
//                    option_1_q1.setVisibility(View.GONE);
//                }else{
//                    option_1_q1.setVisibility(View.VISIBLE);
//                    option_1_q1.setText(options_q1.get(i).getOption());
//                }
//            }
//            if(i==1){
//                if(options_q1.get(i).getOption().equals("")) {
//                    option_2_q1.setVisibility(View.GONE);
//                }else{
//                    option_2_q1.setVisibility(View.VISIBLE);
//                    option_2_q1.setText(options_q1.get(i).getOption());
//                }
//            }
//            if(i==2){
//                if(options_q1.get(i).getOption().equals("")) {
//                    option_3_q1.setVisibility(View.GONE);
//                }else{
//                    option_3_q1.setVisibility(View.VISIBLE);
//                    option_3_q1.setText(options_q1.get(i).getOption());
//                }
//            }
//            if(i==3){
//                if(options_q1.get(i).getOption().equals("")) {
//                    option_4_q1.setVisibility(View.GONE);
//                }else{
//                    option_4_q1.setVisibility(View.VISIBLE);
//                    option_4_q1.setText(options_q1.get(i).getOption());
//                }
//            }
//            if(i==4){
//                if(options_q1.get(i).getOption().equals("")) {
//                    option_5_q1.setVisibility(View.GONE);
//                }else{
//                    option_5_q1.setVisibility(View.VISIBLE);
//                    option_5_q1.setText(options_q1.get(i).getOption());
//                }
//            }
//            if(i==5){
//                if(options_q1.get(i).getOption().equals("")) {
//                    option_6_q1.setVisibility(View.GONE);
//                }else{
//                    option_6_q1.setVisibility(View.VISIBLE);
//                    option_6_q1.setText(options_q1.get(i).getOption());
//                }
//            }
//        }
    }

    public void save(){

        final ProgressDialog progressDialog = new ProgressDialog(QuizTextActivity.this, R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        ApiService api = RetroClient.getApiService();

        JsonObject body = new JsonObject();
        String user_id = getValueFromSharedPreferences("id", QuizTextActivity.this);
        String quiz_dr_child_id   = getValueFromSharedPreferences("quiz_dr_child_id", QuizTextActivity.this);
        String quiz_doctor_name    = getValueFromSharedPreferences("quiz_doctor_name", QuizTextActivity.this);
        String quiz_doctor_cell_phone   = getValueFromSharedPreferences("quiz_doctor_cell_phone", QuizTextActivity.this);
        String quiz_doctor_speciality   = getValueFromSharedPreferences("quiz_doctor_speciality", QuizTextActivity.this);

        JsonArray jsonArray = new JsonArray();
        for(int i=0;i<stringArrayQuestion.length;i++){
            JsonObject jo = new JsonObject();
            jo.addProperty("user_id", user_id);
            jo.addProperty("dr_child_id", quiz_dr_child_id);
            jo.addProperty("doctor_name", quiz_doctor_name);
            jo.addProperty("doctor_cell_phone", quiz_doctor_cell_phone);
            jo.addProperty("doctor_speciality", quiz_doctor_speciality);
            jo.addProperty("question_id", results.get(i).getId().toString());
            jo.addProperty("text_ans", stringArrayQuestion[i]);
            jsonArray.add(jo);
        }
        body.add("ans_list", jsonArray);


        Call<ApiQuestionModel> call = api.API_QUIZ_ANSWARE_MODEL_CALL(body);
        call.enqueue(new Callback<ApiQuestionModel>() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onResponse(Call<ApiQuestionModel> call, Response<ApiQuestionModel> response) {

                if (response.isSuccessful()) {
                    Boolean success = response.body().getSuccess();
                    if(success){
//                        String ans = response.body().getAns();

                        progressDialog.dismiss();
                        onAlreadyAns();
                    }else {
                        String message = response.body().getMessage();

                        progressDialog.dismiss();
                        uiContent.showExitDialog(QuizTextActivity.this,"Error",message);
                    }
                } else {

                    progressDialog.dismiss();
                    uiContent.showExitDialog(QuizTextActivity.this, "Error", "Server error !");
                }
            }

            @Override
            public void onFailure(Call<ApiQuestionModel> call, Throwable t) {

                progressDialog.dismiss();
                uiContent.showExitDialog(QuizTextActivity.this, "Warning", "Please check network connection");
            }
        });

    }

    public void onAlreadyAns() {
        ll_contain.setVisibility(View.GONE);
        ll_congratulation.setVisibility(View.VISIBLE);
    }
}