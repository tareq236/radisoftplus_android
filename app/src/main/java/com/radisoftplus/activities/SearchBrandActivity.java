package com.radisoftplus.activities;

import static com.radisoftplus.utils.SaveLocalStorage.saveToSharedPreferences;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.radisoftplus.R;
import com.radisoftplus.adapter.SearchBrandAdapter;
import com.radisoftplus.models.ApiFindMaterialModel;
import com.radisoftplus.utils.ApiService;
import com.radisoftplus.utils.RetroClient;
import com.radisoftplus.utils.UIContent;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SearchBrandActivity extends AppCompatActivity {

    private static final String TAG = "SearchBrandActivity";
    private Context mContext;
    UIContent uiContent;

    @BindView(R.id.ll_back)
    LinearLayout _back;

    @BindView(R.id.et_product)
    EditText et_product;

    List<ApiFindMaterialModel.Result> rList = new ArrayList<>();
    RecyclerView rcvList;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_brand);
        ButterKnife.bind(this);
        mContext = this;
        uiContent = new UIContent(SearchBrandActivity.this);

        rcvList = findViewById(R.id.rcv_list);

        _back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SearchBrandActivity.this, DcrScreenTwoActivity.class);
                startActivity(intent);
                finish();
            }
        });

        et_product.requestFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(et_product, InputMethodManager.SHOW_IMPLICIT);

        et_product.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {}

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length() != 0){
                    loadDataFromApi(s.toString());
                }else{
                    rList.clear();
                    rcvList.setLayoutManager(new LinearLayoutManager(SearchBrandActivity.this));
                    SearchBrandAdapter reAdapter = new SearchBrandAdapter(rList,SearchBrandActivity.this);
                    rcvList.setAdapter(reAdapter);
                    rcvList.setNestedScrollingEnabled(true);
                    rcvList.setHasFixedSize(true);
                }
            }
        });
    }

    public void loadDataFromApi(String s){
        SharedPreferences sh = getSharedPreferences("login_information", MODE_PRIVATE);

        ApiService api = RetroClient.getApiService();
        JsonObject body = new JsonObject();
        body.addProperty("material_name", s);
        Call<ApiFindMaterialModel> call = api.API_FIND_BRAND_MODEL_CALL(body);
        call.enqueue(new Callback<ApiFindMaterialModel>() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onResponse(Call<ApiFindMaterialModel> call, Response<ApiFindMaterialModel> response) {

                if (response.isSuccessful()) {
                    Boolean success = response.body().getSuccess();
                    if(success){

                        rList = response.body().getResult();
                        if(rList == null){
                            Toast.makeText(mContext, "Brand noy found", Toast.LENGTH_SHORT).show();
                            rcvList.setAdapter(null);
                        }else{
                            if(rList.size()>0) {
                                saveToSharedPreferences("dcr-brand-from-screen","brand", mContext);
                                rcvList.setLayoutManager(new LinearLayoutManager(SearchBrandActivity.this));
                                SearchBrandAdapter reAdapter = new SearchBrandAdapter(rList,SearchBrandActivity.this);
                                rcvList.setAdapter(reAdapter);
                                rcvList.setNestedScrollingEnabled(true);
                                rcvList.setHasFixedSize(true);
                            }
                        }

                    }else{
                        onFailed();
                        uiContent.showExitDialog(SearchBrandActivity.this,"Error",response.body().getMessage());
                    }
                } else {
                    onFailed();
                    uiContent.showExitDialog(SearchBrandActivity.this, "Error", "Server Error !");
                }
            }

            @Override
            public void onFailure(Call<ApiFindMaterialModel> call, Throwable t) {
                Toast.makeText(getBaseContext(), t.getMessage(), Toast.LENGTH_LONG).show();
                uiContent.showExitDialog(SearchBrandActivity.this, "Warning", "Please check network connection");
            }
        });
    }

    public void onFailed() {
        Toast.makeText(getBaseContext(), "Error", Toast.LENGTH_LONG).show();
    }


}
