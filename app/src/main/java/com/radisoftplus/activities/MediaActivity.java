package com.radisoftplus.activities;

import static com.radisoftplus.utils.SaveLocalStorage.getValueFromSharedPreferences;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.radisoftplus.R;
import com.radisoftplus.adapter.MediaAdapter;
import com.radisoftplus.models.ApiMediaModel;
import com.radisoftplus.utils.UIContent;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MediaActivity extends AppCompatActivity {

    private static final String TAG = "MediaPdfActivity";
    private Context mContext;
    UIContent uiContent;

    @BindView(R.id.menu)
    LinearLayout _menu;

    @BindView(R.id.ll_back)
    LinearLayout _back;

    @BindView(R.id.txt_title)
    TextView txt_title;

    RecyclerView rcvList;

    String media_activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_media);
        ButterKnife.bind(this);
        mContext = this;
        uiContent = new UIContent(MediaActivity.this);

        rcvList = findViewById(R.id.rcv_list);

        String from = getValueFromSharedPreferences("from_media_temp", MediaActivity.this);
        media_activity = getValueFromSharedPreferences("media_activity_media_temp", MediaActivity.this);
        String media_type = getValueFromSharedPreferences("media_type_activity_media_temp", MediaActivity.this);

        _menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MediaActivity.this, MainMenuActivity.class);
                startActivity(intent);
            }
        });
        _back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MediaActivity.this, TrainingProductDetailsActivity.class);
                startActivity(intent);
                finish();
            }
        });

        String items = getValueFromSharedPreferences("product_details", MediaActivity.this);
        Gson gson = new Gson();
        ApiMediaModel.Results[] items_obj= gson.fromJson(items, ApiMediaModel.Results[].class);
        List<ApiMediaModel.Results> rListTemp = new ArrayList<>();
        rListTemp = Arrays.asList(items_obj);

        List<ApiMediaModel.Results> rList = new ArrayList<>();
        for(int i = 0; i<rListTemp.size(); i++){
            if(rListTemp.get(i).getFile_type().equals(media_type)){
                if(rListTemp.get(i).getItem_type().equals(media_activity)){
                    rList.add(rListTemp.get(i));
                }
            }
        }

        String title  = getValueFromSharedPreferences("media_type_title", MediaActivity.this);
        txt_title.setText(title);

//        if(media_type.equals("pdf")){
//            txt_title.setText("PDF");
//        }else if(media_type.equals("image")){
//            txt_title.setText("Image");
//        }else if(media_type.equals("video")){
//            txt_title.setText("Video");
//        }

        String url = "http://order.radisoftplus.com/api/v1/order_app/other_function/training_media/all_files/";
//        if(media_activity.equals("training")){
//            url = "http://116.68.200.97:56008/training_media/thumbnail/";
//        }
//        if(media_activity.equals("training1")){
//            url = "http://116.68.200.97:56008/training_media/thumbnail/";
//        }
//        if(media_activity.equals("training2")){
//            url = "http://116.68.200.97:56008/training_media/thumbnail/";
//        }

        if(rList.size()>0) {
            GridLayoutManager gridLayoutManager = new GridLayoutManager(MediaActivity.this,2);
            rcvList.setLayoutManager(gridLayoutManager);
            MediaAdapter reAdapter = new MediaAdapter(rList, url, MediaActivity.this);
            rcvList.setAdapter(reAdapter);
            rcvList.setNestedScrollingEnabled(true);
            rcvList.setHasFixedSize(true);
        }


    }
}