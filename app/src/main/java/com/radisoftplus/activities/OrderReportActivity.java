package com.radisoftplus.activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.radisoftplus.R;
import com.radisoftplus.adapter.OrderReportAdapter;
import com.radisoftplus.models.ApiOrderReportModel;
import com.radisoftplus.utils.ApiService;
import com.radisoftplus.utils.BusyDialog;
import com.radisoftplus.utils.RetroClient;
import com.radisoftplus.utils.UIContent;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static com.radisoftplus.utils.SaveLocalStorage.getValueFromSharedPreferences;
import static com.radisoftplus.utils.SaveLocalStorage.saveToSharedPreferences;

public class OrderReportActivity extends AppCompatActivity {

    private Context mContext;
    UIContent uiContent;
    @BindView(R.id.tv_delivery_date)
    TextView tv_delivery_date;
    @BindView(R.id.ll_back)
    LinearLayout _back;
    @BindView(R.id.totalOrder)
    TextView totalOrder;
    @BindView(R.id.totalTP)
    TextView totalTP;

    DatePickerDialog datePickerDialog;
    SharedPreferences sh;
    private BusyDialog mBusyDialog;

    List<ApiOrderReportModel.Result> rList = new ArrayList<>();
    RecyclerView rcvList;
    @BindView(R.id.ll_logout)
    LinearLayout ll_logout;
    public static final String SHARED_PREFS = "login_information";

    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_report);
        ButterKnife.bind(this);
        mContext = this;
        uiContent = new UIContent(mContext);
        sh = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        String workArea = sh.getString("work_area_t", "");
        rcvList = findViewById(R.id.rcv_list);



        ll_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences preferences =getSharedPreferences(SHARED_PREFS,Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.clear();
                editor.apply();
                finish();

                Intent intent = new Intent(mContext, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });

        _back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String back_where = getValueFromSharedPreferences("back_where", OrderReportActivity.this);
                if(back_where.equals("radiant")){
                    Intent intent = new Intent(OrderReportActivity.this, HomeRadiantActivity.class);
                    startActivity(intent);
                    finish();
                }else if(back_where.equals("tareq")) {
                    Intent intent = new Intent(OrderReportActivity.this, HomeActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        });

        tv_delivery_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar cldr = Calendar.getInstance();
                int day = cldr.get(Calendar.DAY_OF_MONTH);
                int month = cldr.get(Calendar.MONTH);
                int year = cldr.get(Calendar.YEAR);
                datePickerDialog = new DatePickerDialog(OrderReportActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                saveToSharedPreferences("order-delivery_date",(monthOfYear + 1) + "/" + dayOfMonth + "/" + year, mContext);
                                tv_delivery_date.setText((monthOfYear + 1) + "/" + dayOfMonth + "/" + year);
                                loadDataFromApi((  year+ "-" +(monthOfYear + 1) + "-" + dayOfMonth ));
                            }
                        }, year, month, day);

                datePickerDialog.show();
            }
        });
    }

    public void loadDataFromApi(String orderDate){
        mBusyDialog = new BusyDialog(mContext, false, "");
        mBusyDialog.show();
        SharedPreferences sh = getSharedPreferences("login_information", MODE_PRIVATE);
        ApiService api = RetroClient.getApiService();
        JsonObject body = new JsonObject();
        body.addProperty("work_area_t", sh.getString("work_area_t", ""));
        body.addProperty("order_date", orderDate);

        Call<ApiOrderReportModel> call = api.API_ORDER_SUMMARY(body);
        call.enqueue(new Callback<ApiOrderReportModel>() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onResponse(Call<ApiOrderReportModel> call, Response<ApiOrderReportModel> response) {

                if (response.isSuccessful()) {
                    Boolean success = response.body().getSuccess();
                    if(success){

                        rList = response.body().getResult();
                        if(rList.size()>0) {
                            rcvList.setLayoutManager(new LinearLayoutManager(OrderReportActivity.this));
                            OrderReportAdapter reAdapter = new OrderReportAdapter(rList, OrderReportActivity.this);
                            rcvList.setAdapter(reAdapter);
                            rcvList.setNestedScrollingEnabled(true);
                            rcvList.setHasFixedSize(true);
                        }
                        mBusyDialog.dismiss();
                    }else{
                        mBusyDialog.dismiss();
                        onFailed();
                        uiContent.showExitDialog(OrderReportActivity.this,"Error",response.body().getMessage());
                    }
                } else {
                    mBusyDialog.dismiss();
                    onFailed();
                    uiContent.showExitDialog(OrderReportActivity.this, "Error", "Server Error !");
                }
            }

            @Override
            public void onFailure(Call<ApiOrderReportModel> call, Throwable t) {
                mBusyDialog.dismiss();
                Toast.makeText(getBaseContext(), t.getMessage(), Toast.LENGTH_LONG).show();
                uiContent.showExitDialog(OrderReportActivity.this, "Warning", "Please check network connection");
            }
        });
    }

    public void onFailed() {
        Toast.makeText(getBaseContext(), "Error", Toast.LENGTH_LONG).show();
    }

    public void totalCal(String totatTp,String Order){
        totalOrder.setText(Order);
        totalTP.setText(totatTp);
    }
}
