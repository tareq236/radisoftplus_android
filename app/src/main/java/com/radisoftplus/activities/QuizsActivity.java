package com.radisoftplus.activities;

import static com.radisoftplus.utils.SaveLocalStorage.getValueFromSharedPreferences;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.JsonObject;
import com.radisoftplus.R;
import com.radisoftplus.adapter.QuizsAdapter;
import com.radisoftplus.models.ApiQuizsModel;
import com.radisoftplus.utils.ApiService;
import com.radisoftplus.utils.RetroClient;
import com.radisoftplus.utils.UIContent;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class QuizsActivity extends AppCompatActivity {

    private static final String TAG = "QuizsActivity";
    private Context mContext;
    UIContent uiContent;

    @BindView(R.id.menu)
    LinearLayout _menu;

    @BindView(R.id.back)
    LinearLayout _back;

    @BindView(R.id.txt_title)
    TextView txt_title;

    List<ApiQuizsModel.Results> results = new ArrayList<>();
    RecyclerView.LayoutManager layoutManager;
    RecyclerView rcvDataList;

    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quizs);
        ButterKnife.bind(this);
        mContext = this;
        uiContent = new UIContent(QuizsActivity.this);

        rcvDataList = findViewById(R.id.rcv_list);

        _menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(QuizsActivity.this, MainMenuActivity.class);
                startActivity(intent);
                finish();
            }
        });
        _back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(QuizsActivity.this, MainMenuActivity.class);
                startActivity(intent);
                finish();
            }
        });

        loadCommunicationApi();
    }

    public void loadCommunicationApi(){

        final ProgressDialog progressDialog = new ProgressDialog(mContext, R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Data loading...");
        progressDialog.show();

        SharedPreferences sh = getSharedPreferences("login_information", MODE_PRIVATE);
        String workArea = sh.getString("work_area_t", "");
        String month = getValueFromSharedPreferences("Promotional_item_month_name", QuizsActivity.this);


        JsonObject body = new JsonObject();
        body.addProperty("user_id", workArea);
        body.addProperty("month", month);

        ApiService api = RetroClient.getApiService();
        Call<ApiQuizsModel> call = api.API_QUIZS_MODEL_CALL(body);
        call.enqueue(new Callback<ApiQuizsModel>() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onResponse(Call<ApiQuizsModel> call, Response<ApiQuizsModel> response) {

                if (response.isSuccessful()) {
                    Boolean success = response.body().getSuccess();
                    if(success){

                        results = response.body().getResults();
                        if(results.size()>0){
                            rcvDataList.setLayoutManager(new LinearLayoutManager(mContext));
                            QuizsAdapter reListAdapter = new QuizsAdapter(results,mContext);
                            rcvDataList.setAdapter(reListAdapter);
                            rcvDataList.setHasFixedSize(true);
                            layoutManager = new LinearLayoutManager(mContext);
                        }

                        progressDialog.dismiss();
                    }else {
                        onFailed();
                        progressDialog.dismiss();
                        uiContent.showExitDialog(mContext,"Error",response.body().getMessage());
                    }
                } else {
                    onFailed();
                    progressDialog.dismiss();
                    uiContent.showExitDialog(mContext, "Error", "Server Error !");
                }
            }

            @Override
            public void onFailure(Call<ApiQuizsModel> call, Throwable t) {
                Toast.makeText(getBaseContext(), t.getMessage(), Toast.LENGTH_LONG).show();
                progressDialog.dismiss();
                uiContent.showExitDialog(mContext, "Warning", "Please check network connection");
            }
        });
    }

    public void onFailed() {
        Toast.makeText(getBaseContext(), "Error", Toast.LENGTH_LONG).show();
    }

//    @OnClick(R.id.bt_logo)
//    void homeButton(View view) {
//        Intent intent = new Intent(mContext, HomeActivity.class);
//        startActivity(intent);
//    }
}