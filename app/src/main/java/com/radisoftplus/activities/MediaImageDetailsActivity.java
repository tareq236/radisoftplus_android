package com.radisoftplus.activities;

import static com.radisoftplus.utils.SaveLocalStorage.getValueFromSharedPreferences;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import com.radisoftplus.R;
import com.radisoftplus.utils.UIContent;
import com.squareup.picasso.Picasso;
import butterknife.BindView;
import butterknife.ButterKnife;

public class MediaImageDetailsActivity extends AppCompatActivity {

    private static final String TAG = "MediaImageDetailsActivity";
    private Context mContext;
    UIContent uiContent;

//    @BindView(R.id.menu)
//    LinearLayout _menu;

    @BindView(R.id.ll_back)
    LinearLayout _back;

    @BindView(R.id.txt_title)
    TextView txt_title;

    @BindView(R.id.imgView)
    ImageView imgView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_media_image_details);
        ButterKnife.bind(this);
        mContext = this;
        uiContent = new UIContent(MediaImageDetailsActivity.this);

        String media_type   = getValueFromSharedPreferences("media_type_activity_media_temp", MediaImageDetailsActivity.this);
        String media_url    = getValueFromSharedPreferences("media_url_temp", MediaImageDetailsActivity.this);
        String media_path   = getValueFromSharedPreferences("media_path_temp", MediaImageDetailsActivity.this);

//        _menu.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(MediaImageDetailsActivity.this, MainMenuActivity.class);
//                startActivity(intent);
//            }
//        });
        _back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MediaImageDetailsActivity.this, MediaListActivity.class);
                startActivity(intent);
                finish();
            }
        });

        if(media_type.equals("image")){
            Picasso.get()
                    .load(media_url+media_path)
                    .resize(1000, 1000)
                    .centerInside()
                    .into(imgView);
        }


    }
}