package com.radisoftplus.activities;

import static com.radisoftplus.utils.SaveLocalStorage.getValueFromSharedPreferences;
import static com.radisoftplus.utils.SaveLocalStorage.saveToSharedPreferences;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.button.MaterialButton;
import com.google.gson.Gson;
import com.radisoftplus.R;
import com.radisoftplus.adapter.PreviewDcrListAdapter;
import com.radisoftplus.models.DraftDcrModel;
import com.radisoftplus.models.SaveDcrModel;
import com.radisoftplus.models.ApiSaveDcrModel;
import com.radisoftplus.models.BrListModel;
import com.radisoftplus.models.DraftOrderModel;
import com.radisoftplus.utils.ApiService;
import com.radisoftplus.utils.BusyDialog;
import com.radisoftplus.utils.DBHelper;
import com.radisoftplus.utils.RetroClient;
import com.radisoftplus.utils.UIContent;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PreviewDcrActivity extends AppCompatActivity {

    private Context mContext;
    UIContent uiContent;

    @BindView(R.id.ll_back)
    LinearLayout _back;

    @BindView(R.id.tv_employee_name)
    TextView tv_employee_name;

    @BindView(R.id.tv_call_date)
    TextView tv_call_date;

    @BindView(R.id.tv_session_type)
    TextView tv_session_type;

    @BindView(R.id.tv_chamber_type)
    TextView tv_chamber_type;

    @BindView(R.id.tv_doctor_name)
    TextView tv_doctor_name;

    @BindView(R.id.tv_call_type)
    TextView tv_call_type;

    @BindView(R.id.btn_send)
    MaterialButton btn_send;

    @BindView(R.id.btn_draft)
    MaterialButton btn_draft;

    private String draft_id, workArea, call_date, session_type, chamber_type, doctor_name, dr_child_id, call_type, remarks;

    RecyclerView.LayoutManager layoutManager;
    RecyclerView rcvList;
    List<BrListModel> bList = new ArrayList<>();

    private BusyDialog mBusyDialog;
    private SQLiteDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview_dcr);
        ButterKnife.bind(this);
        mContext = this;
        uiContent = new UIContent(mContext);

        DBHelper dbHelper = new DBHelper(mContext); // context is your Activity or Application context
        db = dbHelper.getWritableDatabase(); // Opens the database for reading and writing

        rcvList = findViewById(R.id.rcv_list);

        _back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveDataFromApi();
            }
        });

        btn_draft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveDraft();
            }
        });

        draft_id = getValueFromSharedPreferences("dcr-draft-id", PreviewDcrActivity.this);
        call_date = getValueFromSharedPreferences("dcr-call-date", PreviewDcrActivity.this);
        session_type = getValueFromSharedPreferences("dcr-sessions", PreviewDcrActivity.this);
        chamber_type = getValueFromSharedPreferences("dcr-chamber-type", PreviewDcrActivity.this);
        call_type = getValueFromSharedPreferences("dcr-call-type", PreviewDcrActivity.this);
        dr_child_id = getValueFromSharedPreferences("dcr-dr-child-id", PreviewDcrActivity.this);
        doctor_name = getValueFromSharedPreferences("find-doctor-name", PreviewDcrActivity.this);
        remarks = getValueFromSharedPreferences("dcr-remarks", PreviewDcrActivity.this);

        SharedPreferences sh = getSharedPreferences("login_information", MODE_PRIVATE);
        tv_employee_name.setText(sh.getString("name", "") + " (" + sh.getString("work_area_t", "") + ")");
        tv_call_date.setText(call_date);
        tv_session_type.setText(session_type);
        tv_chamber_type.setText(chamber_type);
        tv_doctor_name.setText(doctor_name + "(" + dr_child_id + ")");
        tv_call_type.setText(call_type);

        String br_list = getValueFromSharedPreferences("br-list", PreviewDcrActivity.this);
        if(br_list != null){
            doWithAllData(br_list);
        }
        if(br_list != null){
            rcvList.setLayoutManager(new LinearLayoutManager(PreviewDcrActivity.this));
            PreviewDcrListAdapter medicineListAdapter = new PreviewDcrListAdapter(bList ,PreviewDcrActivity.this);
            rcvList.setAdapter(medicineListAdapter);
            rcvList.setHasFixedSize(true);
            layoutManager = new LinearLayoutManager(PreviewDcrActivity.this);
            Drawable dividerDrawable = ContextCompat.getDrawable(PreviewDcrActivity.this, R.drawable.recycler_divider_color);
        }

    }

    public void saveDraft(){
        SharedPreferences sh = getSharedPreferences("login_information", MODE_PRIVATE);

        String[] callDateArray = call_date.split("/", 3);
        String callDate = callDateArray[2]+"-"+callDateArray[0]+"-"+callDateArray[1];
        String br_list = getValueFromSharedPreferences("br-list", PreviewDcrActivity.this);

        ContentValues values = new ContentValues();
        values.put("work_area", sh.getString("work_area_t", ""));
        values.put("call_date", callDate);
        values.put("sessions", session_type);
        values.put("chamber_type", chamber_type);
        values.put("doctor_name", doctor_name);
        values.put("dr_child_id", dr_child_id);
        values.put("call_type", call_type);
        values.put("remarks", remarks);
        values.put("json_details", br_list);

        Cursor cursor = db.query(
                "draft_dcr",
                null,  // Projection: null means all columns
                "call_date = ? AND dr_child_id = ?",
                new String[]{callDate, dr_child_id},
                null,  // Group by
                null,  // Having
                null   // Order by
        );

        if (cursor.moveToFirst()) {
            int rowId = cursor.getInt(cursor.getColumnIndex("id"));
            String whereClause = "id = ?";
            String[] whereArgs = {String.valueOf(rowId)};
            int updatedRows = db.update("draft_dcr", values, whereClause, whereArgs);
        } else {
            long newRowId = db.insert("draft_dcr", null, values);
        }

        db.close();

        Intent intent = new Intent(mContext, DcrDraftActivity.class);
        mContext.startActivity(intent);

//        new SweetAlertDialog(mContext, SweetAlertDialog.SUCCESS_TYPE)
//                .setTitleText("Draft save successfully")
//                .setConfirmText("Close").show();
    }

    public void saveDataFromApi(){
        mBusyDialog = new BusyDialog(mContext, false, "Saving...");
        mBusyDialog.show();

        SharedPreferences sh = getSharedPreferences("login_information", MODE_PRIVATE);
        String[] callDateArray = call_date.split("/", 3);
        String callDate = callDateArray[2]+"-"+callDateArray[0]+"-"+callDateArray[1];

        Integer  sessionsNum = 1;
        if(session_type == "Morning"){ sessionsNum = 1; }else{ sessionsNum = 2;}

        Integer locationNum = 1;
        if(chamber_type == "Private"){ locationNum = 1; }
        if(chamber_type == "Hospital"){ locationNum = 2; }
        if(chamber_type == "Hospital Outdoor"){ locationNum = 3; }
        if(chamber_type == "Clinic Indoor"){ locationNum = 4; }
        if(chamber_type == "Clinic Outdoor"){ locationNum = 5; }

        String callTypeNum = "1";
        if(call_type == "Push"){callTypeNum = "1";}else{callTypeNum = "2";}

        List<SaveDcrModel> saveDcrModel = new ArrayList<>();
        SaveDcrModel saveDcrModelSingle = new SaveDcrModel();
        saveDcrModelSingle.setWork_area_t(sh.getString("work_area_t", ""));
        saveDcrModelSingle.setCall_date(callDate);
        saveDcrModelSingle.setSessions(sessionsNum);
        saveDcrModelSingle.setLocation(locationNum);
        saveDcrModelSingle.setCall_type(callTypeNum);
        saveDcrModelSingle.setRemarks(remarks);
        saveDcrModelSingle.setDraft(0);
        List<SaveDcrModel.CallDetail> callDetail = new ArrayList<>();
        for(int i=0;i<bList.size();i++){
            SaveDcrModel.CallDetail callDetailSingle = new SaveDcrModel.CallDetail();
            callDetailSingle.setProduct_id(bList.get(i).getBrand_name());
            callDetailSingle.setGadget_id(bList.get(i).getBr_code());
            callDetailSingle.setGd_qty(bList.get(i).getQty());
            callDetail.add(callDetailSingle);
        }
        saveDcrModelSingle.setCall_detail(callDetail);
        saveDcrModel.add(saveDcrModelSingle);

        ApiService api = RetroClient.getApiService();
        Call<ApiSaveDcrModel> call = api.API_SAVE_DCR_MODEL_CALL(saveDcrModel);
        call.enqueue(new Callback<ApiSaveDcrModel>() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onResponse(Call<ApiSaveDcrModel> call, Response<ApiSaveDcrModel> response) {

                if (response.isSuccessful()) {
                    Boolean success = response.body().getSuccess();
                    if(success){
                        mBusyDialog.dismiss();
                        if(draft_id != null){
                            String tableName = "draft_dcr";
                            String selection = "id = ?";
                            String[] selectionArgs = {draft_id};
                            db.delete(tableName, selection, selectionArgs);
                            saveToSharedPreferences("dcr-draft-id",null, mContext);
                        }
                        Toast.makeText(getBaseContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();
                        String back_where = getValueFromSharedPreferences("back_where", PreviewDcrActivity.this);
                        if(back_where.equals("radiant")){
                            Intent intent = new Intent(PreviewDcrActivity.this, HomeRadiantActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            finish();
                        }else if(back_where.equals("tareq")) {
                            Intent intent = new Intent(PreviewDcrActivity.this, HomeActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            finish();
                        }
                    }else{
                        mBusyDialog.dismiss();
                        onFailed();
                        uiContent.showExitDialog(PreviewDcrActivity.this,"Error",response.body().getMessage());
                    }
                } else {
                    mBusyDialog.dismiss();
                    onFailed();
                    uiContent.showExitDialog(PreviewDcrActivity.this, "Error", "Server Error !");
                }
            }

            @Override
            public void onFailure(Call<ApiSaveDcrModel> call, Throwable t) {
                mBusyDialog.dismiss();
                Toast.makeText(getBaseContext(), t.getMessage(), Toast.LENGTH_LONG).show();
                uiContent.showExitDialog(PreviewDcrActivity.this, "Warning", "Please check network connection");
            }
        });
    }
    public void doWithAllData(String response){
        try {
            JSONArray mainObject = new JSONArray(response);
            for (int i = 0; i < mainObject.length(); i++) {
                JSONObject object = mainObject.getJSONObject(i);
                BrListModel brListModel = new BrListModel();
                brListModel.setBrand_name(object.getString("brand_name"));
                brListModel.setBr_name(object.getString("br_name"));
                brListModel.setBr_code(object.getString("br_code"));
                brListModel.setQty(object.getInt("qty"));
                bList.add(brListModel);
            }
        } catch (Exception e) {
            Log.w("exception-verify data: ", e.getMessage());
        }
    }

    public void onFailed() {
        Toast.makeText(getBaseContext(), "Error", Toast.LENGTH_LONG).show();
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        db.close(); // Close the database when you're done with it
    }

    public void onBackPressed() {
        Intent intent = new Intent(PreviewDcrActivity.this, DcrScreenTwoActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

}
