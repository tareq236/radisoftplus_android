package com.radisoftplus.activities;

import static com.radisoftplus.utils.SaveLocalStorage.getValueFromSharedPreferences;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.radisoftplus.R;
import com.radisoftplus.adapter.GadgetUtilizationAdapter;
import com.radisoftplus.adapter.SentDcrListAdapter;
import com.radisoftplus.models.ApiDcrListModel;
import com.radisoftplus.models.ApiGadgetUtilizationModel;
import com.radisoftplus.utils.ApiService;
import com.radisoftplus.utils.RetroClient;
import com.radisoftplus.utils.UIContent;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GadgetUtilizationActivity extends AppCompatActivity {
    private Context mContext;
    UIContent uiContent;

    @BindView(R.id.ll_back)
    LinearLayout _back;

    RecyclerView rcvList;
    List<ApiGadgetUtilizationModel.Result> rList = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gadget_utilization);
        ButterKnife.bind(this);
        mContext = this;
        uiContent = new UIContent(mContext);

        rcvList = findViewById(R.id.rcv_list);
        _back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String back_where = getValueFromSharedPreferences("back_where", GadgetUtilizationActivity.this);
                if(back_where.equals("radiant")){
                    Intent intent = new Intent(GadgetUtilizationActivity.this, HomeRadiantActivity.class);
                    startActivity(intent);
                    finish();
                }else if(back_where.equals("tareq")) {
                    Intent intent = new Intent(GadgetUtilizationActivity.this, HomeActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        });

        loadDataFromApi();
    }

    public void loadDataFromApi(){
        SharedPreferences sh = getSharedPreferences("login_information", MODE_PRIVATE);
        ApiService api = RetroClient.getApiService();
        JsonObject body = new JsonObject();
        body.addProperty("work_area_t", sh.getString("work_area_t", ""));
        Call<ApiGadgetUtilizationModel> call = api.API_GADGET_UTILIZATION_MODEL_CALL(body);
        call.enqueue(new Callback<ApiGadgetUtilizationModel>() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onResponse(Call<ApiGadgetUtilizationModel> call, Response<ApiGadgetUtilizationModel> response) {

                if (response.isSuccessful()) {
                    Boolean success = response.body().getSuccess();
                    if(success){
                        rList = response.body().getResult();
                        if(rList.size()>0) {
                            rcvList.setLayoutManager(new LinearLayoutManager(GadgetUtilizationActivity.this));
                            GadgetUtilizationAdapter reAdapter = new GadgetUtilizationAdapter(rList,GadgetUtilizationActivity.this);
                            rcvList.setAdapter(reAdapter);
                            rcvList.setNestedScrollingEnabled(true);
                            rcvList.setHasFixedSize(true);
                        }
                    }else{
                        onFailed();
                        uiContent.showExitDialog(GadgetUtilizationActivity.this,"Error",response.body().getMessage());
                    }
                } else {
                    onFailed();
                    uiContent.showExitDialog(GadgetUtilizationActivity.this, "Error", "Server Error !");
                }
            }

            @Override
            public void onFailure(Call<ApiGadgetUtilizationModel> call, Throwable t) {
                Toast.makeText(getBaseContext(), t.getMessage(), Toast.LENGTH_LONG).show();
                uiContent.showExitDialog(GadgetUtilizationActivity.this, "Warning", "Please check network connection");
            }
        });
    }

    public void onFailed() {
        Toast.makeText(getBaseContext(), "Error", Toast.LENGTH_LONG).show();
    }
}
