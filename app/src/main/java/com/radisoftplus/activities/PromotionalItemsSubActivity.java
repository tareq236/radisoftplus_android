package com.radisoftplus.activities;

import static com.radisoftplus.utils.SaveLocalStorage.getValueFromSharedPreferences;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.radisoftplus.R;
import com.radisoftplus.adapter.TrainingProductListAdapter;
import com.radisoftplus.models.ApiCheckBrandCommonModel;
import com.radisoftplus.models.ApiMonthList;
import com.radisoftplus.models.ApiTrainingProductList;
import com.radisoftplus.utils.ApiService;
import com.radisoftplus.utils.BusyDialog;
import com.radisoftplus.utils.RetroClient;
import com.radisoftplus.utils.UIContent;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PromotionalItemsSubActivity extends AppCompatActivity {

    private static final String TAG = "PromotionalItemsSubActivity";
    private Context mContext;
    UIContent uiContent;

    @BindView(R.id.menu)
    LinearLayout _menu;

    @BindView(R.id.ll_back)
    LinearLayout _back;

    @BindView(R.id.ll_brands)
    LinearLayout _ll_brands;

    @BindView(R.id.ll_common)
    LinearLayout _ll_common;

    @BindView(R.id.txt_title)
    TextView txt_title;

    @BindView(R.id.img_new_brand)
    ImageView img_new_brand;

    @BindView(R.id.img_new_common)
    ImageView img_new_common;

    String item_name;

    List<ApiMonthList.Results> rList = new ArrayList<>();
    RecyclerView rcvList;
    private BusyDialog mBusyDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_promotional_items_sub);
        ButterKnife.bind(this);
        mContext = this;
        uiContent = new UIContent(PromotionalItemsSubActivity.this);

        _menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PromotionalItemsSubActivity.this, MainMenuActivity.class);
                startActivity(intent);
            }
        });
        _back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PromotionalItemsSubActivity.this, PromotionalItemsActivity.class);
                startActivity(intent);
                finish();
            }
        });

        _ll_brands.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PromotionalItemsSubActivity.this, PromotionalItemsBrandsActivity.class);
                startActivity(intent);
                finish();
            }
        });

        _ll_common.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PromotionalItemsSubActivity.this, TrainingProductDetailsCommonActivity.class);
                startActivity(intent);
                finish();
            }
        });

        item_name = getValueFromSharedPreferences("Promotional_item_month_name", PromotionalItemsSubActivity.this);
        txt_title.setText(item_name);

        loadActivityFromApi();
    }


    public void loadActivityFromApi(){
        mBusyDialog = new BusyDialog(mContext, false, "");
        mBusyDialog.show();
//        final ProgressDialog progressDialog = new ProgressDialog(TrainingProductListActivity.this, R.style.AppTheme_Dark_Dialog);
//        progressDialog.setIndeterminate(true);
//        progressDialog.setMessage("Data loading...");
//        progressDialog.show();

        SharedPreferences sh = getSharedPreferences("login_information", MODE_PRIVATE);
        String team = sh.getString("group_name", "");
        String month = getValueFromSharedPreferences("Promotional_item_month_name", PromotionalItemsSubActivity.this);

        ApiService api = RetroClient.getApiService();
        Call<ApiCheckBrandCommonModel> call = api.API_CHECK_BRAND_COMMON_MODEL_CALL(team,month);
        call.enqueue(new Callback<ApiCheckBrandCommonModel>() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onResponse(Call<ApiCheckBrandCommonModel> call, Response<ApiCheckBrandCommonModel> response) {

                if (response.isSuccessful()) {
                    Boolean success = response.body().getSuccess();
                    if(success){

                        if(response.body().getResults().get(0).getName().equals("Brand")){
                            if(response.body().getResults().get(0).getIs_new().equals("1")){
                                img_new_brand.setVisibility(View.VISIBLE);
                            }else{
                                img_new_brand.setVisibility(View.GONE);
                            }
                        }
                        if(response.body().getResults().get(1).getName().equals("Common")){
                            if(response.body().getResults().get(1).getIs_new().equals("1")){
                                img_new_common.setVisibility(View.VISIBLE);
                            }else{
                                img_new_common.setVisibility(View.GONE);
                            }
                        }

                        mBusyDialog.dismiss();
                    }else{
                        onFailed();
                        mBusyDialog.dismiss();
                        uiContent.showExitDialog(PromotionalItemsSubActivity.this,"Error",response.body().getMessage());
                    }
                } else {
                    onFailed();
                    mBusyDialog.dismiss();
                    uiContent.showExitDialog(PromotionalItemsSubActivity.this, "Error", "Server Error !");
                }
            }

            @Override
            public void onFailure(Call<ApiCheckBrandCommonModel> call, Throwable t) {
                Toast.makeText(getBaseContext(), t.getMessage(), Toast.LENGTH_LONG).show();
                mBusyDialog.dismiss();
                uiContent.showExitDialog(PromotionalItemsSubActivity.this, "Warning", "Please check network connection");
            }
        });
    }

    public void onFailed() {
        Toast.makeText(getBaseContext(), "Error", Toast.LENGTH_LONG).show();
    }
}