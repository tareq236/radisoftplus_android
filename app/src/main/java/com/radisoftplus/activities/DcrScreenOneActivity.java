package com.radisoftplus.activities;

import static com.radisoftplus.utils.SaveLocalStorage.getValueFromSharedPreferences;
import static com.radisoftplus.utils.SaveLocalStorage.saveToSharedPreferences;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.material.button.MaterialButton;
import com.radisoftplus.R;
import com.radisoftplus.utils.UIContent;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class DcrScreenOneActivity extends AppCompatActivity {

    private Context mContext;
    UIContent uiContent;

    @BindView(R.id.ll_back)
    LinearLayout _back;
    @BindView(R.id.btn_next)
    MaterialButton btn_next;

    @BindView(R.id.tv_call_date)
    TextView tv_call_date;
    DatePickerDialog datePickerDialog;

    @BindView(R.id.sp_sessions)
    Spinner sp_sessions;
    List<String> sessionsArray;
   @BindView(R.id.sp_chamber_type)
    Spinner sp_chamber_type;
    List<String> chamberTypeArray;

    @BindView(R.id.sp_call_type)
    Spinner sp_call_type;
    List<String> callTypeArray;

    @BindView(R.id.et_remarks)
    EditText et_remarks;

    @BindView(R.id.tv_doctor)
    TextView tv_doctor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dcr_screen_one);
        ButterKnife.bind(this);
        mContext = this;
        uiContent = new UIContent(mContext);

        sessionsArray = new ArrayList<String>();
        sessionsArray.add("Morning");
        sessionsArray.add("Evening");
        ArrayAdapter<String> sessionsArrayAdapter = new ArrayAdapter(mContext, android.R.layout.simple_spinner_item, sessionsArray);
        sessionsArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_sessions.setAdapter(sessionsArrayAdapter);

        chamberTypeArray = new ArrayList<String>();
        chamberTypeArray.add("Private");
        chamberTypeArray.add("Hospital");
        chamberTypeArray.add("Hospital Outdoor");
        chamberTypeArray.add("Clinic Indoor");
        chamberTypeArray.add("Clinic Outdoor");
        ArrayAdapter<String> chamberTypeArrayAdapter = new ArrayAdapter(mContext, android.R.layout.simple_spinner_item, chamberTypeArray);
        chamberTypeArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_chamber_type.setAdapter(chamberTypeArrayAdapter);

        callTypeArray = new ArrayList<String>();
        callTypeArray.add("Push");
        callTypeArray.add("Reminder");
        ArrayAdapter<String> callTypeArrayAdapter = new ArrayAdapter(mContext, android.R.layout.simple_spinner_item, callTypeArray);
        callTypeArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_call_type.setAdapter(callTypeArrayAdapter);

        String doctor_name = getValueFromSharedPreferences("find-doctor-name", DcrScreenOneActivity.this);
        String dr_child_id = getValueFromSharedPreferences("find-dr-child-id", DcrScreenOneActivity.this);
        if (doctor_name != null){
            tv_doctor.setText(doctor_name+" ("+dr_child_id+")");
        }

        String call_date = getValueFromSharedPreferences("dcr-call-date", DcrScreenOneActivity.this);
        if (call_date != null){
            if (!call_date.isEmpty()){
                tv_call_date.setText(call_date);
            }
        }
        String sessions = getValueFromSharedPreferences("dcr-sessions", DcrScreenOneActivity.this);
        if (sessions != null){
            if (!sessions.isEmpty()){
                if(sessions.equals("Morning")){
                    sp_sessions.setSelection(0);
                }
                if(sessions.equals("Evening")){
                    sp_sessions.setSelection(1);
                }
            }
        }
        String call_type = getValueFromSharedPreferences("dcr-call-type", DcrScreenOneActivity.this);
        if (call_type != null){
            if (!call_type.isEmpty()){
                if(call_type.equals("Push")){
                    sp_call_type.setSelection(0);
                }
                if(call_type.equals("Reminder")){
                    sp_call_type.setSelection(1);
                }
            }
        }
        String chamber_type = getValueFromSharedPreferences("dcr-chamber-type", DcrScreenOneActivity.this);
        if (chamber_type != null){
            if (!chamber_type.isEmpty()){
                if(chamber_type.equals("Private")){
                    sp_chamber_type.setSelection(0);
                }
                if(chamber_type.equals("Hospital")){
                    sp_chamber_type.setSelection(1);
                }
                if(chamber_type.equals("Hospital Outdoor")){
                    sp_chamber_type.setSelection(2);
                }
                if(chamber_type.equals("Clinic Indoor")){
                    sp_chamber_type.setSelection(3);
                }
                if(chamber_type.equals("Clinic Outdoor")){
                    sp_chamber_type.setSelection(4);
                }
            }
        }
        String remarks = getValueFromSharedPreferences("dcr-remarks", DcrScreenOneActivity.this);
        if (remarks != null){
            et_remarks.setText(remarks);
        }

        _back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        tv_call_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar cldr = Calendar.getInstance();
                int day = cldr.get(Calendar.DAY_OF_MONTH);
                int month = cldr.get(Calendar.MONTH);
                int year = cldr.get(Calendar.YEAR);
                datePickerDialog = new DatePickerDialog(DcrScreenOneActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                String date_ = (monthOfYear + 1) + "/" + dayOfMonth + "/" + year;
                                saveToSharedPreferences("dcr-call-date",date_, mContext);
                                tv_call_date.setText(date_);
                            }
                        }, year, month, day);

                long now = System.currentTimeMillis() - 1000;
                datePickerDialog.getDatePicker().setMinDate(now-(1000*60*60*24*3));
                datePickerDialog.getDatePicker().setMaxDate(now);
                datePickerDialog.show();
            }
        });

        tv_doctor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DcrScreenOneActivity.this, SearchDoctorActivity.class);
                intent.putExtra("dcr-from-screen", "DcrScreenOneActivity");
                startActivity(intent);
                finish();
            }
        });

        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String call_date = tv_call_date.getText().toString().trim();
                String sessions = sp_sessions.getSelectedItem().toString();
                String chamber_type = sp_chamber_type.getSelectedItem().toString();
                String call_type = sp_call_type.getSelectedItem().toString();
                String doctor_name = getValueFromSharedPreferences("find-doctor-name", DcrScreenOneActivity.this);
                String dr_child_id = getValueFromSharedPreferences("find-dr-child-id", DcrScreenOneActivity.this);
                String remarks = et_remarks.getText().toString();

                if (call_date.isEmpty()) {
                    new SweetAlertDialog(mContext, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Please select call date")
                            .setConfirmText("Close").show();
                }else if (sessions.isEmpty()) {
                    new SweetAlertDialog(mContext, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Please select sessions")
                            .setConfirmText("Close").show();
                }else if(chamber_type.isEmpty()){
                    new SweetAlertDialog(mContext, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Please select a chamber type")
                            .setConfirmText("Close").show();
                }else if(doctor_name.isEmpty()){
                    new SweetAlertDialog(mContext, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Please select a doctor")
                            .setConfirmText("Close").show();
                }else if(call_type.isEmpty()){
                    new SweetAlertDialog(mContext, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Please select call type")
                            .setConfirmText("Close").show();
                }else{
                    saveToSharedPreferences("dcr-sessions",sessions, mContext);
                    saveToSharedPreferences("dcr-chamber-type",chamber_type, mContext);
                    saveToSharedPreferences("dcr-call-type",call_type, mContext);
                    saveToSharedPreferences("dcr-dr-child-id",dr_child_id, mContext);
                    saveToSharedPreferences("dcr-remarks",remarks, mContext);

                    Intent intent = new Intent(mContext, DcrScreenTwoActivity.class);
                    intent.putExtra("dcr-from-screen", "DcrScreenOneActivity");
                    mContext.startActivity(intent);
                }
            }
        });

    }

    public void onBackPressed() {
        Intent intent = new Intent(DcrScreenOneActivity.this, HomeRadiantActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }
}
