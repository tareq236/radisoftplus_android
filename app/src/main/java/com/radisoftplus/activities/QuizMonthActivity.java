package com.radisoftplus.activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.radisoftplus.R;
import com.radisoftplus.adapter.MonthListAdapter;
import com.radisoftplus.adapter.MonthListQuizAdapter;
import com.radisoftplus.models.ApiMonthList;
import com.radisoftplus.utils.ApiService;
import com.radisoftplus.utils.BusyDialog;
import com.radisoftplus.utils.RetroClient;
import com.radisoftplus.utils.UIContent;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class QuizMonthActivity extends AppCompatActivity {

    private static final String TAG = "QuizMonthActivity";
    private Context mContext;
    UIContent uiContent;

    @BindView(R.id.menu)
    LinearLayout _menu;

    @BindView(R.id.ll_back)
    LinearLayout _back;

    List<ApiMonthList.Results> rList = new ArrayList<>();
    RecyclerView rcvList;
    private BusyDialog mBusyDialog;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_month);
        ButterKnife.bind(this);
        mContext = this;
        uiContent = new UIContent(QuizMonthActivity.this);

        rcvList = findViewById(R.id.rcv_list);

        _menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(QuizMonthActivity.this, MainMenuActivity.class);
                startActivity(intent);
            }
        });
        _back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(QuizMonthActivity.this, MainMenuActivity.class);
                startActivity(intent);
                finish();
            }
        });

        loadActivityFromApi();
        
    }
    public void loadActivityFromApi(){
        mBusyDialog = new BusyDialog(mContext, false, "");
        mBusyDialog.show();

        ApiService api = RetroClient.getApiService();
        Call<ApiMonthList> call = api.API_MONTH_LIST_CALL();
        call.enqueue(new Callback<ApiMonthList>() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onResponse(Call<ApiMonthList> call, Response<ApiMonthList> response) {

                if (response.isSuccessful()) {
                    Boolean success = response.body().getSuccess();
                    if(success){

                        rList = response.body().getResults();
                        if(rList.size()>0) {
                            rcvList.setLayoutManager(new LinearLayoutManager(QuizMonthActivity.this));
                            MonthListQuizAdapter reAdapter = new MonthListQuizAdapter(rList,QuizMonthActivity.this);
                            rcvList.setAdapter(reAdapter);
                            rcvList.setNestedScrollingEnabled(true);
                            rcvList.setHasFixedSize(true);
                        }


                        mBusyDialog.dismiss();
                    }else{
                        onFailed();
                        mBusyDialog.dismiss();
                        uiContent.showExitDialog(QuizMonthActivity.this,"Error",response.body().getMessage());
                    }
                } else {
                    onFailed();
                    mBusyDialog.dismiss();
                    uiContent.showExitDialog(QuizMonthActivity.this, "Error", "Server Error !");
                }
            }

            @Override
            public void onFailure(Call<ApiMonthList> call, Throwable t) {
                Toast.makeText(getBaseContext(), t.getMessage(), Toast.LENGTH_LONG).show();
                mBusyDialog.dismiss();
                uiContent.showExitDialog(QuizMonthActivity.this, "Warning", "Please check network connection");
            }
        });
    }


    public void onFailed() {
        Toast.makeText(getBaseContext(), "Error", Toast.LENGTH_LONG).show();
    }

}