package com.radisoftplus.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.material.button.MaterialButton;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.radisoftplus.R;
import com.radisoftplus.adapter.OrderListAdapter;
import com.radisoftplus.models.OrderListModel;
import com.radisoftplus.models.SalesOrgModel;
import com.radisoftplus.utils.UIContent;

import org.json.JSONArray;
import org.json.JSONObject;
import android.content.SharedPreferences;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Collections;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

import static com.radisoftplus.utils.SaveLocalStorage.getValueFromSharedPreferences;
import static com.radisoftplus.utils.SaveLocalStorage.saveToSharedPreferences;

public class OrderListActivity extends AppCompatActivity{

    private Context mContext;
    UIContent uiContent;

    @BindView(R.id.ll_back)
    LinearLayout _back;

    @BindView(R.id.btn_add_product)
    MaterialButton btn_add_product;

    @BindView(R.id.tv_amount_summary)
    TextView tv_amount_summary;

    @BindView(R.id.btn_preview)
    MaterialButton btn_preview;

    RecyclerView.LayoutManager layoutManager;
    RecyclerView rcvList;
    List<OrderListModel> bList = new ArrayList<>();
    @BindView(R.id.ll_logout)
    LinearLayout ll_logout;
    public static final String SHARED_PREFS = "login_information";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_list);
        ButterKnife.bind(this);
        mContext = this;
        uiContent = new UIContent(mContext);

        rcvList = findViewById(R.id.rcv_list);

        ll_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences preferences =getSharedPreferences(SHARED_PREFS,Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.clear();
                editor.apply();
                finish();

                Intent intent = new Intent(mContext, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });

        _back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        String order_from_screen = getIntent().getStringExtra("order-from-screen");
        String order_list = getValueFromSharedPreferences("order-list", OrderListActivity.this);
        if(order_list != null){
            doWithAllData(order_list);
        }
        if(order_list != null && order_from_screen.equals("OrderActivity")){
            rcvList.setLayoutManager(new LinearLayoutManager(OrderListActivity.this));
            Collections.reverse(bList);
            OrderListAdapter medicineListAdapter = new OrderListAdapter(bList ,OrderListActivity.this);
            rcvList.setAdapter(medicineListAdapter);
            rcvList.setHasFixedSize(true);
            layoutManager = new LinearLayoutManager(OrderListActivity.this);
            Drawable dividerDrawable = ContextCompat.getDrawable(OrderListActivity.this, R.drawable.recycler_divider_color);
        }

        String sales_org = getIntent().getStringExtra("sales_org");
        String matnr = getIntent().getStringExtra("matnr");
        String mrp = getIntent().getStringExtra("mrp");
        String unit_tp = getIntent().getStringExtra("unit_tp");
        String unit_vat = getIntent().getStringExtra("unit_vat");
        String product_name = getIntent().getStringExtra("product_name");
        String product_details = getIntent().getStringExtra("product_details");
        if(matnr != null && product_name != null && product_details != null){
            String customer_sales_org = getValueFromSharedPreferences("customer-sales-org", OrderListActivity.this);
            Gson gson1=new GsonBuilder().create();
            List<SalesOrgModel> salesOrgList= Arrays.asList(gson1.fromJson(customer_sales_org,SalesOrgModel[].class));
            boolean isSalesOrg = false;
            for (int i=0;i<salesOrgList.size();i++){
                if(salesOrgList.get(i).getSales_org().equals(sales_org)){
                    isSalesOrg = true;
                    OrderListModel orderListModel = new OrderListModel();
                    orderListModel.setDel_plant(salesOrgList.get(i).getDelPlant());
                    orderListModel.setMatnr(matnr);
                    orderListModel.setMrp(mrp);
                    orderListModel.setUnit_tp(unit_tp);
                    orderListModel.setUnit_vat(unit_vat);
                    orderListModel.setMaterial_name(product_name);
                    orderListModel.setQty(1);
                    bList.add(orderListModel);
                    GsonBuilder gsonBuilder = new GsonBuilder();
                    Gson gson = gsonBuilder.create();
                    String JSONObjectString = gson.toJson(bList);
                    saveToSharedPreferences("order-list",JSONObjectString, mContext);
                    rcvList.setLayoutManager(new LinearLayoutManager(OrderListActivity.this));
                    Collections.reverse(bList);
                    OrderListAdapter medicineListAdapter = new OrderListAdapter(bList ,OrderListActivity.this);
                    rcvList.setAdapter(medicineListAdapter);
                    rcvList.setHasFixedSize(true);
                    layoutManager = new LinearLayoutManager(OrderListActivity.this);
                    Drawable dividerDrawable = ContextCompat.getDrawable(OrderListActivity.this, R.drawable.recycler_divider_color);
                }
            }
            if(isSalesOrg == false){
                uiContent.showExitDialog(OrderListActivity.this,"Not Found","Del Plant not found");
            }
        }

        btn_add_product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(OrderListActivity.this, SearchProductActivity.class);
                startActivity(intent);
                finish();
            }
        });

        btn_preview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(OrderListActivity.this, PreviewOrderActivity.class);
                startActivity(intent);
            }
        });

        if(bList.size() != 0){
            totalCal();
        }
    }

    public void doWithAllData(String response){
        try {
            JSONArray mainObject = new JSONArray(response);
            for (int i = 0; i < mainObject.length(); i++) {
                JSONObject object = mainObject.getJSONObject(i);
                OrderListModel orderListModel = new OrderListModel();
                orderListModel.setDel_plant(object.getString("del_plant"));
                orderListModel.setMatnr(object.getString("matnr"));
                orderListModel.setMrp(object.getString("mrp"));
                orderListModel.setUnit_tp(object.getString("unit_tp"));
                orderListModel.setUnit_vat(object.getString("unit_vat"));
                orderListModel.setMaterial_name(object.getString("material_name"));
                orderListModel.setQty(object.getInt("qty"));
                bList.add(orderListModel);
            }
        } catch (Exception e) {
            Log.w("exception-verify data: ", e.getMessage());
        }
    }

    public void totalDeleteCal(int position){
//        if (position >= 0 && position < bList.size()) {
//            bList.remove(position);
//        }

        float tp = 0;
        float vat = 0;
        for (int i=0;i<bList.size(); i++){
            tp = tp + (Float.parseFloat(bList.get(i).getUnit_tp()) * bList.get(i).getQty());
            vat = vat + (Float.parseFloat(bList.get(i).getUnit_vat()) * bList.get(i).getQty());
        }
        tv_amount_summary.setText("TP: "+ String.format("%.2f", tp) + " VAT: " + String.format("%.2f", vat) + " TOTAL: " + String.format("%.2f", (tp+vat)));
    }

    public void totalCal(){
        float tp = 0;
        float vat = 0;
        for (int i=0;i<bList.size(); i++){
            tp = tp + (Float.parseFloat(bList.get(i).getUnit_tp()) * bList.get(i).getQty());
            vat = vat + (Float.parseFloat(bList.get(i).getUnit_vat()) * bList.get(i).getQty());
        }
        tv_amount_summary.setText("TP: "+ String.format("%.2f", tp) + " VAT: " + String.format("%.2f", vat) + " TOTAL: " + String.format("%.2f", (tp+vat)));
    }

    public void onBackPressed() {
        Intent intent = new Intent(OrderListActivity.this, OrderActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

}
