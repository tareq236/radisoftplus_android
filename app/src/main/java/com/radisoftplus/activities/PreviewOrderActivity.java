package com.radisoftplus.activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.button.MaterialButton;
import com.google.gson.Gson;
import com.radisoftplus.R;
import com.radisoftplus.adapter.PreviewOrderListAdapter;
import com.radisoftplus.models.ApiSaveOrderModel;
import com.radisoftplus.models.DraftOrderModel;
import com.radisoftplus.models.OrderListModel;
import com.radisoftplus.models.SaveOrderModel;
import com.radisoftplus.utils.ApiService;
import com.radisoftplus.utils.BusyDialog;
import com.radisoftplus.utils.DBHelper;
import com.radisoftplus.utils.RetroClient;
import com.radisoftplus.utils.UIContent;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.radisoftplus.utils.SaveLocalStorage.getValueFromSharedPreferences;
import static com.radisoftplus.utils.SaveLocalStorage.saveToSharedPreferences;

public class PreviewOrderActivity extends AppCompatActivity {

    private Context mContext;
    UIContent uiContent;

    @BindView(R.id.ll_back)
    LinearLayout _back;

    @BindView(R.id.tv_employee_name)
    TextView tv_employee_name;

    @BindView(R.id.tv_delivery_date)
    TextView tv_delivery_date;

    @BindView(R.id.tv_chemist)
    TextView tv_chemist;

    @BindView(R.id.tv_payment_mode)
    TextView tv_payment_mode;

    @BindView(R.id.tv_credit_team)
    TextView tv_credit_team;

    @BindView(R.id.tv_amount_summary)
    TextView tv_amount_summary;

    @BindView(R.id.btn_send)
    MaterialButton btn_send;

    @BindView(R.id.btn_draft)
    MaterialButton btn_draft;

    private String draft_id, workArea, partner, delivery_date, payment_mode, emp_id_cr_team, customer_name, address;

    RecyclerView.LayoutManager layoutManager;
    RecyclerView rcvList;
    List<OrderListModel> bList = new ArrayList<>();

    private BusyDialog mBusyDialog;
    private SQLiteDatabase db;
    @BindView(R.id.ll_logout)
    LinearLayout ll_logout;
    public static final String SHARED_PREFS = "login_information";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview_order);
        ButterKnife.bind(this);
        mContext = this;
        uiContent = new UIContent(mContext);

        DBHelper dbHelper = new DBHelper(mContext); // context is your Activity or Application context
        db = dbHelper.getWritableDatabase(); // Opens the database for reading and writing

        rcvList = findViewById(R.id.rcv_list);



        ll_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences preferences =getSharedPreferences(SHARED_PREFS,Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.clear();
                editor.apply();
                finish();

                Intent intent = new Intent(mContext, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);

            }
        });

        _back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PreviewOrderActivity.this, OrderListActivity.class);
                intent.putExtra("order-from-screen", "OrderActivity");
                startActivity(intent);
                finish();
            }
        });

        draft_id = getValueFromSharedPreferences("draft-order-id", PreviewOrderActivity.this);
        workArea = getValueFromSharedPreferences("find-word_area-work_area", PreviewOrderActivity.this);
        partner = getValueFromSharedPreferences("find-customer-partner", PreviewOrderActivity.this);
        delivery_date = getValueFromSharedPreferences("order-delivery_date", PreviewOrderActivity.this);
        payment_mode = getValueFromSharedPreferences("order-payment_mode", PreviewOrderActivity.this);
        emp_id_cr_team = getValueFromSharedPreferences("order-emp_id_cr_team", PreviewOrderActivity.this);
        address = getValueFromSharedPreferences("find-word_area-address", PreviewOrderActivity.this);
        customer_name = getValueFromSharedPreferences("find-customer-customer_name", PreviewOrderActivity.this);

        tv_employee_name.setText(address + " ("+workArea+")");
        tv_chemist.setText(customer_name + " ("+partner+")");
        tv_delivery_date.setText(delivery_date);
        tv_payment_mode.setText(payment_mode);
        tv_credit_team.setText(emp_id_cr_team);

        String order_list = getValueFromSharedPreferences("order-list", PreviewOrderActivity.this);
        if(order_list != null){
            doWithAllData(order_list);
        }
        if(order_list != null){
            rcvList.setLayoutManager(new LinearLayoutManager(PreviewOrderActivity.this));
            PreviewOrderListAdapter medicineListAdapter = new PreviewOrderListAdapter(bList ,PreviewOrderActivity.this);
            rcvList.setAdapter(medicineListAdapter);
            rcvList.setHasFixedSize(true);
            layoutManager = new LinearLayoutManager(PreviewOrderActivity.this);
            Drawable dividerDrawable = ContextCompat.getDrawable(PreviewOrderActivity.this, R.drawable.recycler_divider_color);

            totalCal();
        }

        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveDataFromApi();
            }
        });

        btn_draft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveDraft();
            }
        });

    }

    public void saveDraft(){
        String[] deliveryDateArray = delivery_date.split("/", 3);
        String deliveryDate = deliveryDateArray[2]+"-"+deliveryDateArray[0]+"-"+deliveryDateArray[1];
        SharedPreferences sh = getSharedPreferences("login_information", MODE_PRIVATE);
        List<DraftOrderModel> draftOrderModel = new ArrayList<>();
        DraftOrderModel draftOrderModel1 = new DraftOrderModel();
        draftOrderModel1.setAddress(address);
        draftOrderModel1.setWork_area_t(sh.getString("work_area_t", ""));
        draftOrderModel1.setRef_work_area_t(workArea);
        draftOrderModel1.setOrder_date(deliveryDate);
        draftOrderModel1.setOrder_type(payment_mode);
        draftOrderModel1.setCustomer_id(partner);
        draftOrderModel1.setCustomer_name(customer_name);
        draftOrderModel1.setEmployee_id(emp_id_cr_team);
        draftOrderModel1.setDevice_type("AndroidApplication");
        draftOrderModel1.setOrder_list(bList);
        draftOrderModel.add(draftOrderModel1);
        Gson gson = new Gson();
        String jsonDetails = gson.toJson(draftOrderModel);

        ContentValues values = new ContentValues();
        values.put("order_date", deliveryDate);
        values.put("work_area", sh.getString("work_area_t", ""));
        values.put("address", address);
        values.put("chemist_name", customer_name);
        values.put("chemist_id", partner);
        values.put("payment_mode", payment_mode);
        values.put("json_details", jsonDetails);

        Cursor cursor = db.query(
                "draft_order",
                null,  // Projection: null means all columns
                "order_date = ? AND chemist_id = ?",
                new String[]{deliveryDate, partner},
                null,  // Group by
                null,  // Having
                null   // Order by
        );

        if (cursor.moveToFirst()) {
            int rowId = cursor.getInt(cursor.getColumnIndex("id"));
            String whereClause = "id = ?";
            String[] whereArgs = {String.valueOf(rowId)};
            int updatedRows = db.update("draft_order", values, whereClause, whereArgs);
        } else {
            long newRowId = db.insert("draft_order", null, values);
        }

        db.close();

        Intent intent = new Intent(mContext, OrderDraftActivity.class);
        mContext.startActivity(intent);

//        new SweetAlertDialog(mContext, SweetAlertDialog.SUCCESS_TYPE)
//                .setTitleText("Draft save successfully")
//                .setConfirmText("Close").show();
    }

    public void saveDataFromApi(){
        mBusyDialog = new BusyDialog(mContext, false, "Saving...");
        mBusyDialog.show();

        String[] deliveryDateArray = delivery_date.split("/", 3);
        String deliveryDate = deliveryDateArray[2]+"-"+deliveryDateArray[0]+"-"+deliveryDateArray[1];
        SharedPreferences sh = getSharedPreferences("login_information", MODE_PRIVATE);
        List<SaveOrderModel> saveOrderModel = new ArrayList<>();
        SaveOrderModel saveOrderModel1 = new SaveOrderModel();
        saveOrderModel1.setWork_area_t(sh.getString("work_area_t", ""));
        saveOrderModel1.setRef_work_area_t(workArea);
        saveOrderModel1.setOrder_date(deliveryDate);
        saveOrderModel1.setOrder_type(payment_mode);
        saveOrderModel1.setCustomer_id(partner);
        saveOrderModel1.setEmployee_id(emp_id_cr_team);
        saveOrderModel1.setDevice_type("AndroidApplication");
        List<SaveOrderModel.OrderList> orderLists = new ArrayList<>();

        for(int i=0;i<bList.size();i++){
            SaveOrderModel.OrderList orderList = new SaveOrderModel.OrderList();
            orderList.setDel_plant(bList.get(i).getDel_plant());
            orderList.setProduct_id(bList.get(i).getMatnr());
            orderList.setUnit_tp(bList.get(i).getUnit_tp());
            orderList.setUnit_vat(bList.get(i).getUnit_vat());
            orderList.setMrp(bList.get(i).getMrp());
            orderList.setQty(bList.get(i).getQty());
            orderLists.add(orderList);
        }
        saveOrderModel1.setOrder_list(orderLists);
        saveOrderModel.add(saveOrderModel1);

        ApiService api = RetroClient.getApiService();
        Call<ApiSaveOrderModel> call = api.API_SAVE_ORDER_MODEL_CALL(saveOrderModel);
        call.enqueue(new Callback<ApiSaveOrderModel>() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onResponse(Call<ApiSaveOrderModel> call, Response<ApiSaveOrderModel> response) {

                if (response.isSuccessful()) {
                    Boolean success = response.body().getSuccess();
                    if(success){
                        mBusyDialog.dismiss();

                        if(draft_id != null){
                            String tableName = "draft_order";
                            String selection = "id = ?";
                            String[] selectionArgs = {draft_id};
                            db.delete(tableName, selection, selectionArgs);
                            saveToSharedPreferences("draft-order-id",null, mContext);
                        }

                        Toast.makeText(getBaseContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();
                        String back_where = getValueFromSharedPreferences("back_where", PreviewOrderActivity.this);
                        if(back_where.equals("radiant")){
                            Intent intent = new Intent(PreviewOrderActivity.this, HomeRadiantActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            finish();
                        }else if(back_where.equals("tareq")) {
                            Intent intent = new Intent(PreviewOrderActivity.this, HomeActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            finish();
                        }
                    }else{
                        mBusyDialog.dismiss();
                        onFailed();
                        uiContent.showExitDialog(PreviewOrderActivity.this,"Error",response.body().getMessage());
                    }
                } else {
                    mBusyDialog.dismiss();
                    onFailed();
                    uiContent.showExitDialog(PreviewOrderActivity.this, "Error", "Server Error !");
                }
            }

            @Override
            public void onFailure(Call<ApiSaveOrderModel> call, Throwable t) {
                mBusyDialog.dismiss();
                Toast.makeText(getBaseContext(), t.getMessage(), Toast.LENGTH_LONG).show();
                uiContent.showExitDialog(PreviewOrderActivity.this, "Warning", "Please check network connection");
            }
        });
    }

    public void doWithAllData(String response){
        try {
            JSONArray mainObject = new JSONArray(response);
            for (int i = 0; i < mainObject.length(); i++) {
                JSONObject object = mainObject.getJSONObject(i);
                OrderListModel orderListModel = new OrderListModel();
                orderListModel.setDel_plant(object.getString("del_plant"));
                orderListModel.setMatnr(object.getString("matnr"));
                orderListModel.setMrp(object.getString("mrp"));
                orderListModel.setUnit_tp(object.getString("unit_tp"));
                orderListModel.setUnit_vat(object.getString("unit_vat"));
                orderListModel.setMaterial_name(object.getString("material_name"));
                orderListModel.setQty(object.getInt("qty"));
                bList.add(orderListModel);
            }
        } catch (Exception e) {
            Log.w("exception-verify data: ", e.getMessage());
        }
    }

    public void totalCal(){
        float tp = 0;
        float vat = 0;
        for (int i=0;i<bList.size(); i++){
            tp = tp + (Float.parseFloat(bList.get(i).getUnit_tp()) * bList.get(i).getQty());
            vat = vat + (Float.parseFloat(bList.get(i).getUnit_vat()) * bList.get(i).getQty());
        }
        tv_amount_summary.setText("TP: "+ String.format("%.2f", tp) + " VAT: " + String.format("%.2f", vat) + " TOTAL: " + String.format("%.2f", (tp+vat)));
    }

    public void onFailed() {
        Toast.makeText(getBaseContext(), "Error", Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        db.close(); // Close the database when you're done with it
    }
}
