package com.radisoftplus.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.radisoftplus.R;
import com.radisoftplus.adapter.PreviewOrderListAdapter;
import com.radisoftplus.models.OrderListModel;
import com.radisoftplus.utils.UIContent;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import android.content.SharedPreferences;

import static com.radisoftplus.utils.SaveLocalStorage.getValueFromSharedPreferences;

public class SentOrderDetailsActivity extends AppCompatActivity {

    private Context mContext;
    UIContent uiContent;

    @BindView(R.id.ll_back)
    LinearLayout _back;

    @BindView(R.id.tv_employee_name)
    TextView tv_employee_name;

    @BindView(R.id.tv_delivery_date)
    TextView tv_delivery_date;

    @BindView(R.id.tv_chemist)
    TextView tv_chemist;

    @BindView(R.id.tv_payment_mode)
    TextView tv_payment_mode;

    @BindView(R.id.tv_credit_team)
    TextView tv_credit_team;

    @BindView(R.id.tv_amount_summary)
    TextView tv_amount_summary;

    RecyclerView.LayoutManager layoutManager;
    RecyclerView rcvList;
    List<OrderListModel> bList = new ArrayList<>();
    String sentFrom ="";
    @BindView(R.id.ll_logout)
    LinearLayout ll_logout;
    public static final String SHARED_PREFS = "login_information";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sent_order_details);
        ButterKnife.bind(this);
        mContext = this;
        uiContent = new UIContent(mContext);
        sentFrom = this.getIntent().getStringExtra("sent_from");
        rcvList = findViewById(R.id.rcv_list);



        ll_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences preferences =getSharedPreferences(SHARED_PREFS,Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.clear();
                editor.apply();
                finish();

                Intent intent = new Intent(mContext, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });

        _back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(sentFrom.equals("orderlist")){
                    Intent intent = new Intent(SentOrderDetailsActivity.this, SentOrderActivity.class);
                    startActivity(intent);
                    finish();
                }else if(sentFrom.equals("orderreport")){
                    Intent intent = new Intent(SentOrderDetailsActivity.this, OrderReportActivity.class);
                    startActivity(intent);
                    finish();
                }

            }
        });



        String inputDate = getIntent().getStringExtra("sent-order-order_date");
        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");
        inputFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        SimpleDateFormat outputFormat = new SimpleDateFormat("MM/dd/yyyy");
        try {
            Date date = inputFormat.parse(inputDate);
            String outputDate = outputFormat.format(date);
            tv_delivery_date.setText(outputDate);
        } catch (ParseException e) {
            tv_delivery_date.setText(getIntent().getStringExtra("sent-order-order_date"));
            e.printStackTrace();
        }

        tv_employee_name.setText(getIntent().getStringExtra("sent-order-emp_name")+" "+"("+getIntent().getStringExtra("sent-order-emp_id")+")");
        tv_chemist.setText(getIntent().getStringExtra("sent-order-chemist_name")+" "+"("+getIntent().getStringExtra("sent-order-chemist_id")+")");
        tv_payment_mode.setText(getIntent().getStringExtra("sent-order-payment_mode"));
        tv_credit_team.setText(getIntent().getStringExtra("sent-order-crd_employee_id"));

        String order_list = getValueFromSharedPreferences("sent-order-product-list", SentOrderDetailsActivity.this);
        if (order_list != null) {
            doWithAllData(order_list);
        }
    }

    public void doWithAllData(String response){
        try {
            JSONArray mainObject = new JSONArray(response);
            for (int i = 0; i < mainObject.length(); i++) {
                JSONObject object = mainObject.getJSONObject(i);
                OrderListModel orderListModel = new OrderListModel();
                orderListModel.setMatnr(object.getString("product_id"));
                orderListModel.setMrp(object.getString("mrp"));
                orderListModel.setUnit_tp(object.getString("unit_tp"));
                orderListModel.setUnit_vat(object.getString("unit_vat"));
                JSONObject productObject = new JSONObject(object.getString("product_details"));
                orderListModel.setMaterial_name(productObject.getString("material_name"));
                orderListModel.setQty(object.getInt("qty"));
                bList.add(orderListModel);
            }


            rcvList.setLayoutManager(new LinearLayoutManager(SentOrderDetailsActivity.this));
            PreviewOrderListAdapter medicineListAdapter = new PreviewOrderListAdapter(bList, SentOrderDetailsActivity.this);
            rcvList.setAdapter(medicineListAdapter);
            rcvList.setHasFixedSize(true);
            layoutManager = new LinearLayoutManager(SentOrderDetailsActivity.this);
            Drawable dividerDrawable = ContextCompat.getDrawable(SentOrderDetailsActivity.this, R.drawable.recycler_divider_color);

            totalCal();

        } catch (Exception e) {
            Log.w("exception-verify data: ", e.getMessage());
        }
    }

    public void totalCal(){
        float tp = 0;
        float vat = 0;
        for (int i=0;i<bList.size(); i++){
            tp = tp + (Float.parseFloat(bList.get(i).getUnit_tp()) * bList.get(i).getQty());
            vat = vat + (Float.parseFloat(bList.get(i).getUnit_vat()) * bList.get(i).getQty());
        }
        tv_amount_summary.setText("TP: "+ String.format("%.2f", tp) + " VAT: " + String.format("%.2f", vat) + " TOTAL: " + String.format("%.2f", (tp+vat)));
    }
}
