package com.radisoftplus.activities;



import static com.radisoftplus.utils.SaveLocalStorage.getValueFromSharedPreferences;

import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;


import com.github.barteksc.pdfviewer.PDFView;
import com.radisoftplus.R;
import com.radisoftplus.utils.UIContent;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MediaPdfDetailsActivity extends AppCompatActivity {

    private static final String TAG = "MediaPdfDetailsActivity";
    private Context mContext;
    UIContent uiContent;

    @BindView(R.id.menu)
    LinearLayout _menu;

    @BindView(R.id.ll_back)
    LinearLayout _back;

    @BindView(R.id.txt_title)
    TextView txt_title;

    @BindView(R.id.btn_dl)
    Button btn_dl;

    PDFView pdfView;

    private DownloadManager mgr=null;
    private long lastDownload=-1L;
    private boolean open_file = false;

    String media_type, media_url, media_path;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_media_pdf_details);
        ButterKnife.bind(this);
        mContext = this;
        uiContent = new UIContent(MediaPdfDetailsActivity.this);

        mgr=(DownloadManager)getSystemService(DOWNLOAD_SERVICE);
        registerReceiver(onComplete, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
        registerReceiver(onNotificationClick, new IntentFilter(DownloadManager.ACTION_NOTIFICATION_CLICKED));


        pdfView = findViewById(R.id.pdfView);

        media_type   = getValueFromSharedPreferences("media_type_activity_media_temp", MediaPdfDetailsActivity.this);
        media_url    = getValueFromSharedPreferences("media_url_temp", MediaPdfDetailsActivity.this);
        media_path   = getValueFromSharedPreferences("media_path_temp", MediaPdfDetailsActivity.this);

        _menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MediaPdfDetailsActivity.this, MainMenuActivity.class);
                startActivity(intent);
            }
        });
        _back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MediaPdfDetailsActivity.this, MediaListActivity.class);
                startActivity(intent);
                finish();
            }
        });

        btn_dl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(open_file){
                    startActivity(new Intent(DownloadManager.ACTION_VIEW_DOWNLOADS));
                }else{
                    startDownload();
                }

            }
        });

        if(media_type.equals("pdf")){
            new RetrivePDFfromUrl().execute(media_url+media_path);
        }

    }


    @Override
    public void onDestroy() {
        super.onDestroy();

        unregisterReceiver(onComplete);
        unregisterReceiver(onNotificationClick);
    }

    public void startDownload() {
        Uri uri=Uri.parse(media_url+media_path);

        Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
                .mkdirs();

        lastDownload=
                mgr.enqueue(new DownloadManager.Request(uri)
                        .setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI |
                                DownloadManager.Request.NETWORK_MOBILE)
                        .setAllowedOverRoaming(false)
                        .setTitle(media_path)
                        .setDescription("Something useful. No, really.")
                        .setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, media_path));

        btn_dl.setEnabled(false);
        btn_dl.setText("Downloading...");
    }

    BroadcastReceiver onComplete=new BroadcastReceiver() {
        public void onReceive(Context ctxt, Intent intent) {
            btn_dl.setEnabled(true);
            btn_dl.setText("Open File");
            open_file = true;
            Toast.makeText(ctxt, "Download Complete", Toast.LENGTH_LONG).show();
        }
    };

    BroadcastReceiver onNotificationClick=new BroadcastReceiver() {
        public void onReceive(Context ctxt, Intent intent) {
            Toast.makeText(ctxt, "Ummmm...hi!", Toast.LENGTH_LONG).show();
        }
    };

    class RetrivePDFfromUrl extends AsyncTask<String, Void, InputStream> {
        private ProgressDialog progressDialog = new ProgressDialog(MediaPdfDetailsActivity.this, R.style.AppTheme_Dark_Dialog);

        @Override
        protected void onPreExecute() {
            progressDialog.setMessage("Loading Pdf...");
            progressDialog.show();
        }

        @Override
        protected InputStream doInBackground(String... strings) {
            InputStream inputStream = null;
            try {
                URL url = new URL(strings[0]);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                if (urlConnection.getResponseCode() == 200) {
                    inputStream = new BufferedInputStream(urlConnection.getInputStream());
                }
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
            return inputStream;
        }

        @Override
        protected void onPostExecute(InputStream inputStream) {
            pdfView.fromStream(inputStream).load();
            progressDialog.dismiss();
        }
    }
}