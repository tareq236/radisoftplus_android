package com.radisoftplus.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import com.radisoftplus.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeRadiantActivity extends AppCompatActivity {
    private static final String TAG = "HomeRadiantActivity";
    private Context mContext;
    @BindView(R.id.ll_logout)
    LinearLayout ll_logout;

    @BindView(R.id.ll_order)
    LinearLayout ll_order;

    @BindView(R.id.ll_dcr)
    LinearLayout ll_dcr;

    public static final String SHARED_PREFS = "login_information";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_radiant);
        ButterKnife.bind(this);
        mContext=this;

        View darkOverlay = findViewById(R.id.darkOverlay);
        darkOverlay.setVisibility(View.GONE);

        ll_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences preferences =getSharedPreferences(SHARED_PREFS,Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.clear();
                editor.apply();
                finish();

                Intent intent = new Intent(mContext, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });

        ll_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                darkOverlay.setVisibility(View.VISIBLE);
                OrderOptionsDialogFragment dialog = new OrderOptionsDialogFragment();
                dialog.show(getSupportFragmentManager(), "OrderOptionsDialogFragment");
            }
        });

        ll_dcr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                darkOverlay.setVisibility(View.VISIBLE);
                DcrOptionsDialogFragment dialog = new DcrOptionsDialogFragment();
                dialog.show(getSupportFragmentManager(), "DcrOptionsDialogFragment");
            }
        });
    }

    public void hideDarkOverlay() {
        View darkOverlay = findViewById(R.id.darkOverlay);
        darkOverlay.setVisibility(View.GONE);
    }


}
