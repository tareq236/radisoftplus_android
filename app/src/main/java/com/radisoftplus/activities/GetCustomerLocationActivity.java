package com.radisoftplus.activities;

import static com.radisoftplus.utils.SaveLocalStorage.getValueFromSharedPreferences;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.button.MaterialButton;
import com.google.gson.JsonObject;
import com.radisoftplus.R;
import com.radisoftplus.adapter.SearchChemistAdapter;
import com.radisoftplus.models.ApiFindCustomerModel;
import com.radisoftplus.models.ApiSaveCustomerLocationModel;
import com.radisoftplus.utils.ApiService;
import com.radisoftplus.utils.BusyDialog;
import com.radisoftplus.utils.RetroClient;
import com.radisoftplus.utils.UIContent;

import java.util.ArrayList;

import androidx.recyclerview.widget.LinearLayoutManager;
import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GetCustomerLocationActivity extends AppCompatActivity implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    UIContent uiContent;
    private Context mContext;

    @BindView(R.id.tv_chemist)
    TextView tv_chemist;
    @BindView(R.id.ll_back)
    LinearLayout _back;
    private GoogleMap mMap;
    double latitude = 0.0;
    double longitude = 0.0;
    @BindView(R.id.tvLattitude)
    TextView tvLattitude;
    @BindView(R.id.tvLongitude)
    TextView tvLongitude;


    @BindView(R.id.btnGetLocation)
    MaterialButton btnGetLocation;

    @BindView(R.id.btnSave)
    MaterialButton btnSave;

    private BusyDialog mBusyDialog;

    private final int MULTIPLE_PERMISSIONS = 103;
    String[] permissions = new String[]{
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA,
            Manifest.permission.ACCESS_FINE_LOCATION
    };


    ArrayList<LatLng> MarkerPoints;
    GoogleApiClient mGoogleApiClient;
    Location mLastLocation;
    Marker mCurrLocationMarker;
    LocationRequest mLocationRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_customer_location);
        ButterKnife.bind(this);

        uiContent = new UIContent(GetCustomerLocationActivity.this);
        mContext=this;

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        if(checkLocationPermission()){
            mBusyDialog = new BusyDialog(mContext, false, "Location Tracking...");
            mBusyDialog.show();
        }

        _back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(GetCustomerLocationActivity.this, HomeActivity.class);
                startActivity(intent);
                finish();
            }
        });

        btnGetLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(tv_chemist.getText().toString().equals("")){
                    new SweetAlertDialog(GetCustomerLocationActivity.this, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Please select chemist")
                            .setConfirmText("Close").show();
                }else {
                    tvLattitude.setVisibility(View.VISIBLE);
                    tvLongitude.setVisibility(View.VISIBLE);
                    btnSave.setVisibility(View.VISIBLE);
                }

            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(tv_chemist.getText().toString().equals("")){
                    new SweetAlertDialog(GetCustomerLocationActivity.this, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Please select chemist")
                            .setConfirmText("Close").show();
                }else {
                    saveData();
                }
            }
        });

        tv_chemist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(GetCustomerLocationActivity.this, SearchChemistActivity.class);
                intent.putExtra("order-from-screen", "TrackingActivity");
                startActivity(intent);
            }
        });

        String partner = getValueFromSharedPreferences("find-customer-partner", GetCustomerLocationActivity.this);
        String customer_name = getValueFromSharedPreferences("find-customer-customer_name", GetCustomerLocationActivity.this);
        if (partner != null || customer_name != null) {
            tv_chemist.setText(partner + " (" + customer_name + ")");
        }


    }


    public void saveData(){
        SharedPreferences sh = getSharedPreferences("login_information", MODE_PRIVATE);
        String partner = getValueFromSharedPreferences("find-customer-partner", GetCustomerLocationActivity.this);

        ApiService api = RetroClient.getApiService();
        JsonObject body = new JsonObject();
        body.addProperty("work_area_t", sh.getString("work_area_t", ""));
        body.addProperty("customer_id", partner);
        body.addProperty("latitude", latitude);
        body.addProperty("longitude", longitude);
        Call<ApiSaveCustomerLocationModel> call = api.API_SAVE_CUSTOMER_LOCATION_MODEL_CALL(body);
        call.enqueue(new Callback<ApiSaveCustomerLocationModel>() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onResponse(Call<ApiSaveCustomerLocationModel> call, Response<ApiSaveCustomerLocationModel> response) {

                if (response.isSuccessful()) {
                    Boolean success = response.body().getSuccess();
                    if(success){
                        Toast.makeText(GetCustomerLocationActivity.this,"Customer Location Save Successfully",Toast.LENGTH_LONG).show();
                        startActivity(new Intent(GetCustomerLocationActivity.this,HomeActivity.class));
                    }else{
                        onFailed();
                        uiContent.showExitDialog(GetCustomerLocationActivity.this,"Error",response.body().getMessage());
                    }
                } else {
                    onFailed();
                    uiContent.showExitDialog(GetCustomerLocationActivity.this, "Error", "Server Error !");
                }
            }

            @Override
            public void onFailure(Call<ApiSaveCustomerLocationModel> call, Throwable t) {
                Toast.makeText(getBaseContext(), t.getMessage(), Toast.LENGTH_LONG).show();
                uiContent.showExitDialog(GetCustomerLocationActivity.this, "Warning", "Please check network connection");
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                mMap.setMyLocationEnabled(true);
            }
        } else {
            buildGoogleApiClient();
            mMap.setMyLocationEnabled(true);
        }

    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @SuppressLint("RestrictedApi")
    @Override
    public void onConnected(Bundle bundle) {

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(Location location) {

        mLastLocation = location;
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }

        //Place current location marker
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        tvLattitude.setText(String.valueOf("Lattitude: "+location.getLatitude()));
        tvLongitude.setText(String.valueOf("Longitude: "+location.getLongitude()));

        tvLattitude.setText(String.valueOf("Lattitude: "+location.getLatitude()));
        tvLongitude.setText(String.valueOf("Longitude: "+location.getLongitude()));

        latitude = location.getLatitude();
        longitude = location.getLongitude();

        markerOptions.title("Current Position" + location.getLatitude() + " " + location.getLongitude());
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA));
        mCurrLocationMarker = mMap.addMarker(markerOptions);
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
//        mMap.animateCamera(CameraUpdateFactory.zoomTo(18));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng,15.0f));
        //stop location updates
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }

        mBusyDialog.dismiss();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    public boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }

    @SuppressLint("MissingSuperCall")
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        if (mGoogleApiClient == null) {
                            buildGoogleApiClient();
                        }
                        mMap.setMyLocationEnabled(true);
                    }

                } else {
                    Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show();
                }
                return;
            }
        }
    }

    public void onFailed() {
        Toast.makeText(getBaseContext(), "Error", Toast.LENGTH_LONG).show();
    }

}