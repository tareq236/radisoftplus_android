package com.radisoftplus.activities;

import static com.radisoftplus.utils.SaveLocalStorage.getValueFromSharedPreferences;
import static com.radisoftplus.utils.SaveLocalStorage.saveToSharedPreferences;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.material.button.MaterialButton;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.radisoftplus.R;
import com.radisoftplus.adapter.BrListAdapter;
import com.radisoftplus.adapter.OrderListAdapter;
import com.radisoftplus.models.BrListModel;
import com.radisoftplus.models.OrderListModel;
import com.radisoftplus.utils.UIContent;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DcrScreenTwoActivity extends AppCompatActivity {
    private Context mContext;
    UIContent uiContent;

    @BindView(R.id.ll_back)
    LinearLayout _back;

    @BindView(R.id.tv_brand)
    TextView tv_brand;

    @BindView(R.id.tv_br)
    TextView tv_br;
    @BindView(R.id.btn_add_item)
    MaterialButton btn_add_item;

    @BindView(R.id.btn_preview)
    MaterialButton btn_preview;

    RecyclerView.LayoutManager layoutManager;
    RecyclerView rcvList;
    List<BrListModel> bList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dcr_screen_two);
        ButterKnife.bind(this);
        mContext = this;
        uiContent = new UIContent(mContext);

        rcvList = findViewById(R.id.rcv_list);

        String select_brand_name = getValueFromSharedPreferences("dcr-select-brand-name", DcrScreenTwoActivity.this);
        if (select_brand_name != null){
            tv_brand.setText(select_brand_name);
        }
        String select_br_name = getValueFromSharedPreferences("dcr-select-br-name", DcrScreenTwoActivity.this);
        String select_br_code = getValueFromSharedPreferences("dcr-select-br-code", DcrScreenTwoActivity.this);
        if (select_brand_name != null){
            tv_br.setText(select_br_name);
        }

        String br_list = getValueFromSharedPreferences("br-list", DcrScreenTwoActivity.this);
        if(br_list != null){
            doWithAllData(br_list);
            rcvList.setLayoutManager(new LinearLayoutManager(DcrScreenTwoActivity.this));
            BrListAdapter listAdapter = new BrListAdapter(bList ,DcrScreenTwoActivity.this);
            rcvList.setAdapter(listAdapter);
            rcvList.setHasFixedSize(true);
            layoutManager = new LinearLayoutManager(DcrScreenTwoActivity.this);
            Drawable dividerDrawable = ContextCompat.getDrawable(DcrScreenTwoActivity.this, R.drawable.recycler_divider_color);
        }

        _back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        tv_brand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DcrScreenTwoActivity.this, SearchBrandActivity.class);
                startActivity(intent);
                finish();
            }
        });
        tv_br.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DcrScreenTwoActivity.this, SearchBrActivity.class);
                startActivity(intent);
                finish();
            }
        });

        btn_add_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                String br_list = getValueFromSharedPreferences("br-list", DcrScreenTwoActivity.this);
//                if(br_list != null){
//                    doWithAllData(br_list);
//                }

                String select_brand_name = getValueFromSharedPreferences("dcr-select-brand-name", DcrScreenTwoActivity.this);
                String select_br_name = getValueFromSharedPreferences("dcr-select-br-name", DcrScreenTwoActivity.this);
                String select_br_code = getValueFromSharedPreferences("dcr-select-br-code", DcrScreenTwoActivity.this);
                BrListModel brListModel = new BrListModel();
                brListModel.setBrand_name(select_brand_name);
                brListModel.setBr_name(select_br_name);
                brListModel.setBr_code(select_br_code);
                brListModel.setQty(1);
                bList.add(brListModel);

                GsonBuilder gsonBuilder = new GsonBuilder();
                Gson gson = gsonBuilder.create();
                String JSONObjectString = gson.toJson(bList);
                saveToSharedPreferences("br-list",JSONObjectString, mContext);

                rcvList.setLayoutManager(new LinearLayoutManager(DcrScreenTwoActivity.this));
                BrListAdapter listAdapter = new BrListAdapter(bList ,DcrScreenTwoActivity.this);
                rcvList.setAdapter(listAdapter);
                rcvList.setHasFixedSize(true);
                layoutManager = new LinearLayoutManager(DcrScreenTwoActivity.this);
                Drawable dividerDrawable = ContextCompat.getDrawable(DcrScreenTwoActivity.this, R.drawable.recycler_divider_color);

                saveToSharedPreferences("dcr-select-brand-name", "", mContext);
                saveToSharedPreferences("dcr-select-br-name", "", mContext);
                saveToSharedPreferences("dcr-select-br-code", "", mContext);
                tv_brand.setText("");
                tv_br.setText("");
            }
        });

        btn_preview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DcrScreenTwoActivity.this, PreviewDcrActivity.class);
                startActivity(intent);
            }
        });
    }

    public void doWithAllData(String response){
        try {
            JSONArray mainObject = new JSONArray(response);
            for (int i = 0; i < mainObject.length(); i++) {
                JSONObject object = mainObject.getJSONObject(i);
                BrListModel brListModel = new BrListModel();
                brListModel.setBrand_name(object.getString("brand_name"));
                brListModel.setBr_name(object.getString("br_name"));
                brListModel.setBr_code(object.getString("br_code"));
                brListModel.setQty(object.getInt("qty"));
                bList.add(brListModel);
            }
        } catch (Exception e) {
            Log.w("exception-verify data: ", e.getMessage());
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(DcrScreenTwoActivity.this, DcrScreenOneActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }
}
