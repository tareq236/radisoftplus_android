package com.radisoftplus.activities;

import static com.radisoftplus.utils.SaveLocalStorage.getValueFromSharedPreferences;
import static com.radisoftplus.utils.SaveLocalStorage.saveToSharedPreferences;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.radisoftplus.R;
import com.radisoftplus.adapter.SearchBrAdapter;
import com.radisoftplus.adapter.SearchBrandAdapter;
import com.radisoftplus.models.ApiFindMaterialModel;
import com.radisoftplus.utils.ApiService;
import com.radisoftplus.utils.RetroClient;
import com.radisoftplus.utils.UIContent;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchBrActivity extends AppCompatActivity {

    private static final String TAG = "SearchBrActivity";
    private Context mContext;
    UIContent uiContent;

    @BindView(R.id.ll_back)
    LinearLayout _back;

    @BindView(R.id.llNotAvailable)
    LinearLayout llNotAvailable;

    List<ApiFindMaterialModel.Result> rList = new ArrayList<>();
    RecyclerView rcvList;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_br);
        ButterKnife.bind(this);
        mContext = this;
        uiContent = new UIContent(SearchBrActivity.this);

        rcvList = findViewById(R.id.rcv_list);

        _back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        loadDataFromApi();
    }

    public void loadDataFromApi(){
        SharedPreferences sh = getSharedPreferences("login_information", MODE_PRIVATE);

        ApiService api = RetroClient.getApiService();
        JsonObject body = new JsonObject();
        String product_code = getValueFromSharedPreferences("dcr-select-brand-name", SearchBrActivity.this);
        body.addProperty("product_code", product_code);
        body.addProperty("work_area_t", sh.getString("work_area_t", ""));
        Call<ApiFindMaterialModel> call = api.API_FIND_PMA_MODEL_CALL(body);
        call.enqueue(new Callback<ApiFindMaterialModel>() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onResponse(Call<ApiFindMaterialModel> call, Response<ApiFindMaterialModel> response) {

                if (response.isSuccessful()) {
                    Boolean success = response.body().getSuccess();
                    if(success){
                        rList = response.body().getResult();
                        if(rList != null){
                            if(rList.size()>0) {
                                llNotAvailable.setVisibility(View.GONE);
                                rcvList.setVisibility(View.VISIBLE);
                                rcvList.setLayoutManager(new LinearLayoutManager(SearchBrActivity.this));
                                SearchBrAdapter reAdapter = new SearchBrAdapter(rList,SearchBrActivity.this);
                                rcvList.setAdapter(reAdapter);
                                rcvList.setNestedScrollingEnabled(true);
                                rcvList.setHasFixedSize(true);
                            }else{
                                llNotAvailable.setVisibility(View.VISIBLE);
                                rcvList.setVisibility(View.GONE);
                            }
                        }else{
                            llNotAvailable.setVisibility(View.VISIBLE);
                            rcvList.setVisibility(View.GONE);
                        }
                    }else{
                        llNotAvailable.setVisibility(View.VISIBLE);
                        rcvList.setVisibility(View.GONE);
                        onFailed();
                        uiContent.showExitDialog(SearchBrActivity.this,"Error",response.body().getMessage());
                    }
                } else {
                    llNotAvailable.setVisibility(View.VISIBLE);
                    rcvList.setVisibility(View.GONE);
                    onFailed();
                    uiContent.showExitDialog(SearchBrActivity.this, "Error", "Server Error !");
                }
            }

            @Override
            public void onFailure(Call<ApiFindMaterialModel> call, Throwable t) {
                Toast.makeText(getBaseContext(), t.getMessage(), Toast.LENGTH_LONG).show();
                uiContent.showExitDialog(SearchBrActivity.this, "Warning", "Please check network connection");
            }
        });
    }

    public void onFailed() {
        Toast.makeText(getBaseContext(), "Error", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(SearchBrActivity.this, DcrScreenTwoActivity.class);
        startActivity(intent);
        finish();
    }
}
