package com.radisoftplus.activities;

import static com.radisoftplus.utils.SaveLocalStorage.saveToSharedPreferences;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;

import com.radisoftplus.R;
import com.radisoftplus.utils.UIContent;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class QuizEnterDoctorActivity extends AppCompatActivity {

    private static final String TAG = "QuizEnterDoctorActivity";
    private Context mContext;
    UIContent uiContent;

    @BindView(R.id.menu)
    LinearLayout _menu;

    @BindView(R.id.back)
    LinearLayout _back;

    @BindView(R.id.btn_start_quiz)
    Button btn_start_quiz;

    @BindView(R.id.et_doctor_name)
    EditText et_doctor_name;

    @BindView(R.id.et_doctor_id)
    EditText et_doctor_id;

    @BindView(R.id.et_contact_no)
    EditText et_contact_no;

    String[] specialty_list = new String[] {"DERMATOLOGIST",
            "ORTHOPEDICS",
            "ENT",
            "MEDICINE",
            "GYNECOLOGY",
            "CHEST MED. & SURG",
            "GP",
            "GASTROENTEROLOGY",
            "NEURO-MEDICINE",
            "OPHTHALMOLOGY",
            "NEPHROLOGY",
            "UROLOGY",
            "DIABETICS",
            "NEURO-SURGERY",
            "PSYCHIATRY",
            "CARDIOLOGY",
            "ENDOCRINOLOGY",
            "OTHERS",
            "RMP",
            "HEPATOLOGY",
            "RHEUMATOLOGY",
            "SURGERY",
            "CARDIAC-SURGERY",
            "NEONETOLOGY",
            "ANESTHESIOLOGY",
            "DENTAL",
            "ONCOLOGY",
            "BURN & PLASTIC SURG",
            "INTERNEE & DOD",
            "PAEDIATRIC",
            "HEMATOLOGY"};
    MaterialBetterSpinner materialBetterSpinner ;
    String specialty;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_enter_doctor);
        ButterKnife.bind(this);
        mContext = this;
        uiContent = new UIContent(QuizEnterDoctorActivity.this);

        materialBetterSpinner = findViewById(R.id.input_specialty_spinner);
        materialBetterSpinner = (MaterialBetterSpinner)findViewById(R.id.input_specialty_spinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(QuizEnterDoctorActivity.this, android.R.layout.simple_dropdown_item_1line, specialty_list);
        materialBetterSpinner.setAdapter(adapter);
        materialBetterSpinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                String spinner_value= adapterView.getItemAtPosition(position).toString();
                specialty = spinner_value;
            }
        });

        _menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(QuizEnterDoctorActivity.this, MainMenuActivity.class);
                startActivity(intent);
            }
        });
        _back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(QuizEnterDoctorActivity.this, HomeActivity.class);
                startActivity(intent);
                finish();
            }
        });

        btn_start_quiz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(et_doctor_name.getText().toString().equals("")){
                    uiContent.showExitDialog(QuizEnterDoctorActivity.this, "Validation", "Please enter doctor name !");
                }else
                if(et_doctor_id.getText().toString().equals("")){
                    uiContent.showExitDialog(QuizEnterDoctorActivity.this, "Validation", "Please enter doctor id !");
                }else
                if(et_contact_no.getText().toString().equals("")){
                    uiContent.showExitDialog(QuizEnterDoctorActivity.this, "Validation", "Please enter doctor contact no !");
                }else
                if(specialty.equals("")){
                    uiContent.showExitDialog(QuizEnterDoctorActivity.this, "Validation", "Please enter doctor specialty !");
                }else{
                    saveToSharedPreferences("quiz_dr_child_id",et_doctor_id.getText().toString(), mContext);
                    saveToSharedPreferences("quiz_doctor_name",et_doctor_name.getText().toString(), mContext);
                    saveToSharedPreferences("quiz_doctor_cell_phone",et_contact_no.getText().toString(), mContext);
                    saveToSharedPreferences("quiz_doctor_speciality",specialty.toString(), mContext);

                    Intent intent = new Intent(QuizEnterDoctorActivity.this, QuizStartActivity.class);
                    startActivity(intent);
                }

            }
        });
    }

//    @OnClick(R.id.bt_logo)
//    void homeButton(View view) {
//        Intent intent = new Intent(mContext, HomeActivity.class);
//        startActivity(intent);
//    }

}