package com.radisoftplus.activities;

import static com.radisoftplus.utils.SaveLocalStorage.getValueFromSharedPreferences;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.radisoftplus.R;
import com.radisoftplus.adapter.DraftOrderListAdapter;
import com.radisoftplus.models.DraftOrderTableModel;
import com.radisoftplus.utils.DBHelper;
import com.radisoftplus.utils.UIContent;
import android.content.SharedPreferences;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class OrderDraftActivity extends AppCompatActivity {
    private Context mContext;
    UIContent uiContent;

    @BindView(R.id.ll_back)
    LinearLayout _back;

    RecyclerView rcvList;

    private SQLiteDatabase db;
    @BindView(R.id.ll_logout)
    LinearLayout ll_logout;
    public static final String SHARED_PREFS = "login_information";
    List<DraftOrderTableModel> draftOrderTableModel = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_draft);
        ButterKnife.bind(this);
        mContext = this;
        uiContent = new UIContent(mContext);
        
        DBHelper dbHelper = new DBHelper(mContext); // context is your Activity or Application context
        db = dbHelper.getWritableDatabase(); // Opens the database for reading and writing

        // Perform the query
        Cursor cursor = db.query(
                "draft_order",     // The table name
                null,                   // The columns to retrieve (null for all)
                null,               // The selection (null for all rows)
                null,               // Selection arguments
                null,               // Group by
                null,               // Having
                "id DESC"           // The sort order
        );

        // Iterate through the results
        while (cursor.moveToNext()) {
            DraftOrderTableModel draftOrderTableModelSingle = new DraftOrderTableModel();

            draftOrderTableModelSingle.setId(cursor.getInt(cursor.getColumnIndex("id")));
            draftOrderTableModelSingle.setOrder_date(cursor.getString(cursor.getColumnIndex("order_date")));
            draftOrderTableModelSingle.setWork_area(cursor.getString(cursor.getColumnIndex("work_area")));
            draftOrderTableModelSingle.setAddress(cursor.getString(cursor.getColumnIndex("address")));
            draftOrderTableModelSingle.setChemist_name(cursor.getString(cursor.getColumnIndex("chemist_name")));
            draftOrderTableModelSingle.setChemist_id(cursor.getString(cursor.getColumnIndex("chemist_id")));
            draftOrderTableModelSingle.setPayment_mode(cursor.getString(cursor.getColumnIndex("payment_mode")));
            draftOrderTableModelSingle.setJson_details(cursor.getString(cursor.getColumnIndex("json_details")));
            draftOrderTableModelSingle.setCreated_at(cursor.getString(cursor.getColumnIndex("created_at")));

            draftOrderTableModel.add(draftOrderTableModelSingle);
        }
        cursor.close();

        rcvList = findViewById(R.id.rcv_list);

        if(draftOrderTableModel.size()>0) {
            rcvList.setLayoutManager(new LinearLayoutManager(OrderDraftActivity.this));
            DraftOrderListAdapter reAdapter = new DraftOrderListAdapter(draftOrderTableModel,OrderDraftActivity.this);
            rcvList.setAdapter(reAdapter);
            rcvList.setNestedScrollingEnabled(true);
            rcvList.setHasFixedSize(true);
        }



        ll_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences preferences =getSharedPreferences(SHARED_PREFS,Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.clear();
                editor.apply();
                finish();

                Intent intent = new Intent(mContext, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });

        _back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    public void deleteItem(int id){
        // Define the table name and the condition for deletion
        String tableName = "draft_order";
        String selection = "id = ?";
        String[] selectionArgs = {String.valueOf(id)};

        // Perform the deletion
        int deletedRows = db.delete(tableName, selection, selectionArgs);

        if (deletedRows > 0) {
            new SweetAlertDialog(mContext, SweetAlertDialog.SUCCESS_TYPE)
                .setTitleText("Delete was successful")
                .setConfirmText("Close").show();
        } else {
            new SweetAlertDialog(mContext, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Not Deleted")
                    .setConfirmText("Close").show();
        }
    }


    @Override
    public void onBackPressed() {
        String back_where = getValueFromSharedPreferences("back_where", OrderDraftActivity.this);

        Intent intent = null;

        if (back_where.equals("radiant")) {
            intent = new Intent(OrderDraftActivity.this, HomeRadiantActivity.class);
        } else if (back_where.equals("tareq")) {
            intent = new Intent(OrderDraftActivity.this, HomeActivity.class);
        }

        if (intent != null) {
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        }
    }
}
