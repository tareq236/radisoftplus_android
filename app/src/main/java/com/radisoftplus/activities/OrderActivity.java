package com.radisoftplus.activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import com.google.android.material.button.MaterialButton;
import com.radisoftplus.R;
import com.radisoftplus.utils.UIContent;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.radisoftplus.utils.SaveLocalStorage.getValueFromSharedPreferences;
import static com.radisoftplus.utils.SaveLocalStorage.saveToSharedPreferences;
import android.content.SharedPreferences;

public class OrderActivity extends AppCompatActivity{

    private Context mContext;
    UIContent uiContent;

    @BindView(R.id.ll_back)
    LinearLayout _back;

    @BindView(R.id.tv_work_area)
    TextView tv_work_area;

    @BindView(R.id.tv_delivery_date)
    TextView tv_delivery_date;

    @BindView(R.id.tv_chemist)
    TextView tv_chemist;

    @BindView(R.id.sp_payment_mode)
    Spinner sp_payment_mode;

    @BindView(R.id.et_emp_id_cr_team)
    EditText et_emp_id_cr_team;

    @BindView(R.id.btn_next)
    MaterialButton btn_next;

    DatePickerDialog datePickerDialog;
    List<String> paymentModeArray;
    @BindView(R.id.ll_logout)
    LinearLayout ll_logout;
    public static final String SHARED_PREFS = "login_information";

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);
        ButterKnife.bind(this);
        mContext = this;
        uiContent = new UIContent(mContext);


        ll_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences preferences =getSharedPreferences(SHARED_PREFS,Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.clear();
                editor.apply();
                finish();

                Intent intent = new Intent(mContext, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });

        _back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        paymentModeArray = new ArrayList<String>();
        paymentModeArray.add("Cash");
        paymentModeArray.add("Credit");
        paymentModeArray.add("Shared");
        ArrayAdapter<String> paymentArrayAdapter = new ArrayAdapter(mContext, android.R.layout.simple_spinner_item, paymentModeArray);
        paymentArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_payment_mode.setAdapter(paymentArrayAdapter);

        String workArea = getValueFromSharedPreferences("find-word_area-work_area", OrderActivity.this);
        String address = getValueFromSharedPreferences("find-word_area-address", OrderActivity.this);
        tv_work_area.setText(workArea+" ("+address+")");
        String partner = getValueFromSharedPreferences("find-customer-partner", OrderActivity.this);
        String customer_name = getValueFromSharedPreferences("find-customer-customer_name", OrderActivity.this);
        if (partner != null || customer_name != null){
            tv_chemist.setText(partner+" ("+customer_name+")");
        }
        String delivery_date = getValueFromSharedPreferences("order-delivery_date", OrderActivity.this);
        if (delivery_date != null){
            tv_delivery_date.setText(delivery_date);
            tv_delivery_date.setEnabled(false);
        }
        String payment_mode = getValueFromSharedPreferences("order-payment_mode", OrderActivity.this);
        if(payment_mode != null){
            if (payment_mode.equals("Cash")){
                sp_payment_mode.setSelection(0);
            }else if (payment_mode.equals("Credit")){
                sp_payment_mode.setSelection(1);
            }else if (payment_mode.equals("Shared")){
                sp_payment_mode.setSelection(2);
            }
        }


        String emp_id_cr_team = getValueFromSharedPreferences("order-emp_id_cr_team", OrderActivity.this);
        if (emp_id_cr_team != null){
            et_emp_id_cr_team.setText(emp_id_cr_team);
        }

        tv_work_area.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(OrderActivity.this, SearchWorkAreaActivity.class);
                startActivity(intent);
            }
        });

        tv_chemist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(OrderActivity.this, SearchChemistActivity.class);
                intent.putExtra("order-from-screen", "OrderActivity");
                startActivity(intent);
                finish();
            }
        });

        tv_delivery_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar cldr = Calendar.getInstance();
                int day = cldr.get(Calendar.DAY_OF_MONTH);
                int month = cldr.get(Calendar.MONTH);
                int year = cldr.get(Calendar.YEAR);
                datePickerDialog = new DatePickerDialog(OrderActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                saveToSharedPreferences("order-delivery_date",(monthOfYear + 1) + "/" + dayOfMonth + "/" + year, mContext);
                                tv_delivery_date.setText((monthOfYear + 1) + "/" + dayOfMonth + "/" + year);
                            }
                        }, year, month, day);

                long now = System.currentTimeMillis() - 1000;
                datePickerDialog.getDatePicker().setMinDate(now);
                datePickerDialog.getDatePicker().setMaxDate(now+(1000*60*60*24*7));
                datePickerDialog.show();
            }
        });

        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String payment_mode = sp_payment_mode.getSelectedItem().toString();
                String work_area_t = getValueFromSharedPreferences("find-word_area-work_area", OrderActivity.this);
                String delivery_date = tv_delivery_date.getText().toString().trim();
                String customer_partner = getValueFromSharedPreferences("find-customer-partner", OrderActivity.this);
                String emp_id_cr_team = et_emp_id_cr_team.getText().toString();

                if(work_area_t.isEmpty()){
                    new SweetAlertDialog(mContext, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Please select a work area id")
                            .setConfirmText("Close").show();
                }else if (delivery_date.isEmpty()) {
                    new SweetAlertDialog(mContext, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Please select delivery date")
                            .setConfirmText("Close").show();
                }else if (customer_partner == null) {
                    new SweetAlertDialog(mContext, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Please select chemist name")
                            .setConfirmText("Close").show();
                }else if(payment_mode.isEmpty()){
                    new SweetAlertDialog(mContext, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Please select a payment mode")
                            .setConfirmText("Close").show();
                }else{
                    saveToSharedPreferences("order-payment_mode",payment_mode, mContext);
                    saveToSharedPreferences("order-emp_id_cr_team",emp_id_cr_team, mContext);

                    Intent intent = new Intent(mContext, OrderListActivity.class);
                    intent.putExtra("order-from-screen", "OrderActivity");
                    mContext.startActivity(intent);
                }

//                SharedPreferencesHelper.setWorkAreaId(mContext,workareaId);
//                SharedPreferencesHelper.setDeliveryDate(mContext,deliveryDate);
//                SharedPreferencesHelper.setChemistName(mContext,chemistName);
//                SharedPreferencesHelper.setPaymentMode(mContext,paymentMode);
//                SharedPreferencesHelper.setEmployeeId(mContext,employeeId);
//                    startActivity(new Intent(OrderActivity.this, AddProductActivity.class));
            }
        });


    }

    public void onBackPressed() {
        Intent intent = new Intent(OrderActivity.this, HomeRadiantActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

}
