package com.radisoftplus.activities;

import static com.radisoftplus.utils.SaveLocalStorage.getValueFromSharedPreferences;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.radisoftplus.R;
import com.radisoftplus.adapter.DraftDcrListAdapter;
import com.radisoftplus.adapter.DraftOrderListAdapter;
import com.radisoftplus.models.DraftDcrModel;
import com.radisoftplus.models.DraftOrderTableModel;
import com.radisoftplus.utils.DBHelper;
import com.radisoftplus.utils.UIContent;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class DcrDraftActivity extends AppCompatActivity {

    private Context mContext;
    UIContent uiContent;

    @BindView(R.id.ll_back)
    LinearLayout _back;

    RecyclerView rcvList;

    private SQLiteDatabase db;

    List<DraftDcrModel> draftDcrTableModel = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dcr_draft);
        ButterKnife.bind(this);
        mContext = this;
        uiContent = new UIContent(mContext);

        DBHelper dbHelper = new DBHelper(mContext); // context is your Activity or Application context
        db = dbHelper.getWritableDatabase(); // Opens the database for reading and writing

        // Perform the query
        Cursor cursor = db.query(
                "draft_dcr",     // The table name
                null,                   // The columns to retrieve (null for all)
                null,               // The selection (null for all rows)
                null,               // Selection arguments
                null,               // Group by
                null,               // Having
                "id DESC"           // The sort order
        );

        // Iterate through the results
        while (cursor.moveToNext()) {
            DraftDcrModel draftDcrTableModelSingle = new DraftDcrModel();
            draftDcrTableModelSingle.setId(cursor.getInt(cursor.getColumnIndex("id")));
            draftDcrTableModelSingle.setCall_date(cursor.getString(cursor.getColumnIndex("call_date")));
            draftDcrTableModelSingle.setSessions(cursor.getString(cursor.getColumnIndex("sessions")));
            draftDcrTableModelSingle.setChamber_type(cursor.getString(cursor.getColumnIndex("chamber_type")));
            draftDcrTableModelSingle.setDoctor_name(cursor.getString(cursor.getColumnIndex("doctor_name")));
            draftDcrTableModelSingle.setDr_child_id(cursor.getString(cursor.getColumnIndex("dr_child_id")));
            draftDcrTableModelSingle.setCall_type(cursor.getString(cursor.getColumnIndex("call_type")));
            draftDcrTableModelSingle.setRemarks(cursor.getString(cursor.getColumnIndex("remarks")));
            draftDcrTableModelSingle.setJson_details(cursor.getString(cursor.getColumnIndex("json_details")));
            draftDcrTableModelSingle.setCreated_at(cursor.getString(cursor.getColumnIndex("created_at")));
            draftDcrTableModel.add(draftDcrTableModelSingle);
        }
        cursor.close();

        rcvList = findViewById(R.id.rcv_list);

        if(draftDcrTableModel.size()>0) {
            rcvList.setLayoutManager(new LinearLayoutManager(DcrDraftActivity.this));
            DraftDcrListAdapter reAdapter = new DraftDcrListAdapter(draftDcrTableModel,DcrDraftActivity.this);
            rcvList.setAdapter(reAdapter);
            rcvList.setNestedScrollingEnabled(true);
            rcvList.setHasFixedSize(true);
        }

        _back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    public void deleteItem(int id){
        // Define the table name and the condition for deletion
        String tableName = "draft_dcr";
        String selection = "id = ?";
        String[] selectionArgs = {String.valueOf(id)};

        // Perform the deletion
        int deletedRows = db.delete(tableName, selection, selectionArgs);

        if (deletedRows > 0) {
            new SweetAlertDialog(mContext, SweetAlertDialog.SUCCESS_TYPE)
                    .setTitleText("Delete was successful")
                    .setConfirmText("Close").show();
        } else {
            new SweetAlertDialog(mContext, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Not Deleted")
                    .setConfirmText("Close").show();
        }
    }

    @Override
    public void onBackPressed() {
        String back_where = getValueFromSharedPreferences("back_where", DcrDraftActivity.this);
        if(back_where.equals("radiant")){
            Intent intent = new Intent(DcrDraftActivity.this, HomeRadiantActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        }else if(back_where.equals("tareq")) {
            Intent intent = new Intent(DcrDraftActivity.this, HomeActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        }
    }
}


