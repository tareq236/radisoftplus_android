package com.radisoftplus.activities;

import static com.radisoftplus.utils.SaveLocalStorage.getValueFromSharedPreferences;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.radisoftplus.R;
import com.radisoftplus.adapter.SentOrderListAdapter;
import com.radisoftplus.models.ApiOrderListModel;
import com.radisoftplus.utils.ApiService;
import com.radisoftplus.utils.RetroClient;
import com.radisoftplus.utils.UIContent;

import java.util.ArrayList;
import java.util.List;

public class SentOrderActivity extends AppCompatActivity {

    private static final String TAG = "SentDcrActivity";
    private Context mContext;
    UIContent uiContent;

    @BindView(R.id.ll_back)
    LinearLayout _back;

    List<ApiOrderListModel.Result> rList = new ArrayList<>();
    RecyclerView rcvList;
    @BindView(R.id.ll_logout)
    LinearLayout ll_logout;
    public static final String SHARED_PREFS = "login_information";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sent_order);
        ButterKnife.bind(this);
        mContext = this;
        uiContent = new UIContent(SentOrderActivity.this);

        rcvList = findViewById(R.id.rcv_list);



        ll_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences preferences =getSharedPreferences(SHARED_PREFS,Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.clear();
                editor.apply();
                finish();

                Intent intent = new Intent(mContext, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });

        _back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String back_where = getValueFromSharedPreferences("back_where", SentOrderActivity.this);
                if(back_where.equals("radiant")){
                    Intent intent = new Intent(SentOrderActivity.this, HomeRadiantActivity.class);
                    startActivity(intent);
                    finish();
                }else if(back_where.equals("tareq")) {
                    Intent intent = new Intent(SentOrderActivity.this, HomeActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        });

        loadDataFromApi();
    }

    public void loadDataFromApi(){
        SharedPreferences sh = getSharedPreferences("login_information", MODE_PRIVATE);
        ApiService api = RetroClient.getApiService();
        JsonObject body = new JsonObject();
        body.addProperty("work_area_t", sh.getString("work_area_t", ""));
        body.addProperty("offset", 0);
        body.addProperty("limit", 20);
        Call<ApiOrderListModel> call = api.API_ORDER_LIST_MODEL_CALL(body);
        call.enqueue(new Callback<ApiOrderListModel>() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onResponse(Call<ApiOrderListModel> call, Response<ApiOrderListModel> response) {

                if (response.isSuccessful()) {
                    Boolean success = response.body().getSuccess();
                    if(success){

                        rList = response.body().getResult();
                        if(rList.size()>0) {
                            rcvList.setLayoutManager(new LinearLayoutManager(SentOrderActivity.this));
                            SentOrderListAdapter reAdapter = new SentOrderListAdapter(rList,SentOrderActivity.this);
                            rcvList.setAdapter(reAdapter);
                            rcvList.setNestedScrollingEnabled(true);
                            rcvList.setHasFixedSize(true);
                        }
                    }else{
                        onFailed();
                        uiContent.showExitDialog(SentOrderActivity.this,"Error",response.body().getMessage());
                    }
                } else {
                    onFailed();
                    uiContent.showExitDialog(SentOrderActivity.this, "Error", "Server Error !");
                }
            }

            @Override
            public void onFailure(Call<ApiOrderListModel> call, Throwable t) {
                Toast.makeText(getBaseContext(), t.getMessage(), Toast.LENGTH_LONG).show();
                uiContent.showExitDialog(SentOrderActivity.this, "Warning", "Please check network connection");
            }
        });
    }

    public void onFailed() {
        Toast.makeText(getBaseContext(), "Error", Toast.LENGTH_LONG).show();
    }

}
