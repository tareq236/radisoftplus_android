package com.radisoftplus.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.radisoftplus.R;
import com.radisoftplus.models.ApiTodayVisitPlanModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class VisitPlanDetailsActivity extends AppCompatActivity {

    private static final String TAG = "VisitPlanDetailsActivity";
    private Context mContext;

    @BindView(R.id.ll_back)
    LinearLayout back;

    @BindView(R.id.txv_doctor_name)
    TextView txv_doctor_name;
    @BindView(R.id.txv_speciality)
    TextView txv_speciality;
    @BindView(R.id.txv_category)
    TextView txv_category;
    @BindView(R.id.txv_brand1)
    TextView txv_brand1;
    @BindView(R.id.txv_brand2)
    TextView txv_brand2;
    @BindView(R.id.txv_brand3)
    TextView txv_brand3;
    @BindView(R.id.txv_brand4)
    TextView txv_brand4;
    @BindView(R.id.txv_gadget_list)
    TextView txv_gadget_list;

    @BindView(R.id.ckb_brand1)
    CheckBox ckb_brand1;
    @BindView(R.id.ckb_brand2)
    CheckBox ckb_brand2;
    @BindView(R.id.ckb_brand3)
    CheckBox ckb_brand3;
    @BindView(R.id.ckb_brand4)
    CheckBox ckb_brand4;

    @BindView(R.id.ckb_gadget1)
    CheckBox ckb_gadget1;
    @BindView(R.id.ckb_gadget2)
    CheckBox ckb_gadget2;
    @BindView(R.id.ckb_gadget3)
    CheckBox ckb_gadget3;
    @BindView(R.id.ckb_gadget4)
    CheckBox ckb_gadget4;
    @BindView(R.id.card_view_g1)
    CardView card_view_g1;
    @BindView(R.id.card_view_g2)
    CardView card_view_g2;
    @BindView(R.id.card_view_g3)
    CardView card_view_g3;
    @BindView(R.id.card_view_g4)
    CardView card_view_g4;

    @BindView(R.id.btn_finish_visit)
    Button btn_finish_visit;
    @BindView(R.id.btn_cancel_visit)
    Button btn_cancel_visit;

    private String doctor_name;
    private String speciality_description;
    private String category;
    private List<ApiTodayVisitPlanModel.MonthlyVisitPlanList> monthlyVisitPlanList = new ArrayList<>();
    private String monthlyVisitPlanListString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visit_plan_details);
        mContext = this;
        ButterKnife.bind(this);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        doctor_name = getIntent().getStringExtra("doctor_name");
        speciality_description = getIntent().getStringExtra("speciality_description");
        category = getIntent().getStringExtra("category");
        monthlyVisitPlanListString = getIntent().getStringExtra("monthly_visit_plan_list");
        try {
            JSONArray jsonarray = new JSONArray(monthlyVisitPlanListString);
            String gadgetListText = "";
            for(int i=0;i<jsonarray.length();i++){
                JSONObject obj=jsonarray.getJSONObject(i);
                if(i == 0){
                    txv_brand1.setText(obj.getString("brand_name"));
                    ckb_brand1.setText(obj.getString("brand_name"));
                    ckb_brand1.setChecked(true);
                    String gadget_name = obj.getString("gadget_name");
                    if(!gadget_name.equals("")){
                        gadgetListText = gadgetListText + ", " + obj.getString("gadget_name");
                        ckb_gadget1.setText(obj.getString("gadget_name"));
                        ckb_gadget1.setChecked(true);
                        card_view_g1.setVisibility(View.VISIBLE);
                    }
                }
                if(i == 1){
                    txv_brand2.setText(obj.getString("brand_name"));
                    ckb_brand2.setText(obj.getString("brand_name"));
                    ckb_brand2.setChecked(true);
                    String gadget_name = obj.getString("gadget_name");
                    if(!gadget_name.equals("")){
                        gadgetListText = gadgetListText + ", " + obj.getString("gadget_name");
                        ckb_gadget2.setText(obj.getString("gadget_name"));
                        ckb_gadget2.setChecked(true);
                        card_view_g2.setVisibility(View.VISIBLE);
                    }
                }
                if(i == 2){
                    txv_brand3.setText(obj.getString("brand_name"));
                    ckb_brand3.setText(obj.getString("brand_name"));
                    ckb_brand3.setChecked(true);
                    String gadget_name = obj.getString("gadget_name");
                    if(!gadget_name.equals("")){
                        gadgetListText = gadgetListText + ", " + obj.getString("gadget_name");
                        ckb_gadget3.setText(obj.getString("gadget_name"));
                        ckb_gadget3.setChecked(true);
                        card_view_g3.setVisibility(View.VISIBLE);
                    }
                }
                if(i == 3){
                    txv_brand4.setText(obj.getString("brand_name"));
                    ckb_brand4.setText(obj.getString("brand_name"));
                    ckb_brand4.setChecked(true);
                    String gadget_name = obj.getString("gadget_name");
                    if(!gadget_name.equals("")){
                        gadgetListText = gadgetListText + ", " + obj.getString("gadget_name");
                        ckb_gadget4.setText(obj.getString("gadget_name"));
                        ckb_gadget4.setChecked(true);
                        card_view_g4.setVisibility(View.VISIBLE);
                    }
                }
            }
            txv_gadget_list.setText(gadgetListText);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        txv_doctor_name.setText(String.valueOf(doctor_name).trim());
        txv_speciality.setText(String.valueOf("Speciality: " + speciality_description));
        txv_category.setText(String.valueOf("Category: " + category));

        btn_finish_visit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        btn_cancel_visit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, CancelVisitActivity.class);
                startActivity(intent);
            }
        });

    }
}