package com.radisoftplus.activities;

import static com.radisoftplus.utils.SaveLocalStorage.getValueFromSharedPreferences;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.radisoftplus.R;
import com.radisoftplus.adapter.BrListAdapter;
import com.radisoftplus.adapter.BrSentDetailsListAdapter;
import com.radisoftplus.adapter.PreviewOrderListAdapter;
import com.radisoftplus.models.ApiDcrListModel;
import com.radisoftplus.models.BrListModel;
import com.radisoftplus.models.OrderListModel;
import com.radisoftplus.models.SalesOrgModel;
import com.radisoftplus.utils.UIContent;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SentDcrDetailsActivity extends AppCompatActivity {

    private Context mContext;
    UIContent uiContent;
    @BindView(R.id.ll_back)
    LinearLayout _back;
    @BindView(R.id.tv_employee_name)
    TextView tv_employee_name;
    @BindView(R.id.tv_call_date)
    TextView tv_call_date;
    @BindView(R.id.tv_session_type)
    TextView tv_session_type;
    @BindView(R.id.tv_chamber_type)
    TextView tv_chamber_type;
    @BindView(R.id.tv_call_type)
    TextView tv_call_type;
    @BindView(R.id.tv_remarks)
    TextView tv_remarks;
    @BindView(R.id.tv_doctor_name)
    TextView tv_doctor_name;

    RecyclerView.LayoutManager layoutManager;
    RecyclerView rcvList;
    String sentFrom ="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sent_dcr_details);
        ButterKnife.bind(this);
        mContext = this;
        uiContent = new UIContent(mContext);
        sentFrom = this.getIntent().getStringExtra("sent_from");
        rcvList = findViewById(R.id.rcv_list);

        _back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SentDcrDetailsActivity.this, SentDcrActivity.class);
                startActivity(intent);
                finish();
            }
        });

        String details = getValueFromSharedPreferences("sent-dcr-details", SentDcrDetailsActivity.this);
        if (details != null) {
            Gson gson1=new GsonBuilder().create();
            List<ApiDcrListModel.Result> salesOrgList= Arrays.asList(gson1.fromJson(details,ApiDcrListModel.Result.class));
            tv_employee_name.setText(salesOrgList.get(0).getUserDetails().getName() + "("+salesOrgList.get(0).getWorkAreaT()+")");


            String inputDate = salesOrgList.get(0).getCallDate();
            SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");
            inputFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            SimpleDateFormat outputFormat = new SimpleDateFormat("MM/dd/yyyy");
            try {
                Date date = inputFormat.parse(inputDate);
                String outputDate = outputFormat.format(date);
                tv_call_date.setText(outputDate);
            } catch (ParseException e) {
                tv_call_date.setText(salesOrgList.get(0).getCallDate());
                e.printStackTrace();
            }


            String sessions = "";
            if(salesOrgList.get(0).getSessions() == 1){ sessions = "Morning"; }else{ sessions = "Evening";}
            tv_session_type.setText(sessions);
            String location = "";
            if(salesOrgList.get(0).getLocation() == 1){ location = "Private"; }
            if(salesOrgList.get(0).getLocation() == 2){ location = "Hospital"; }
            if(salesOrgList.get(0).getLocation() == 3){ location = "Hospital Outdoor"; }
            if(salesOrgList.get(0).getLocation() == 4){ location = "Clinic Indoor"; }
            if(salesOrgList.get(0).getLocation() == 5){ location = "Clinic Outdoor"; }
            tv_chamber_type.setText(location);
            String callType = "";
            if(salesOrgList.get(0).getLocation() == 1){callType = "Push";}else{callType = "Reminder";}
            tv_call_type.setText(callType);
            tv_remarks.setText(salesOrgList.get(0).getRemarks());
            tv_doctor_name.setText(salesOrgList.get(0).getCallDoctorDetails().getDoctorDetails().getDoctorName1() +" "+ salesOrgList.get(0).getCallDoctorDetails().getDoctorDetails().getDoctorName2() + "("+salesOrgList.get(0).getDoctorId()+")");

            List<BrListModel> brList = new ArrayList<>();
            for (int i = 0; i < salesOrgList.get(0).getCallDetail().size(); i++) {
                BrListModel brListModel = new BrListModel();
                brListModel.setBrand_name(salesOrgList.get(0).getCallDetail().get(i).getProductId());
                brListModel.setBr_code(salesOrgList.get(0).getCallDetail().get(i).getGadgetID());
                brListModel.setBr_name(salesOrgList.get(0).getCallDetail().get(i).getGadgetDetails().getPiName());
                brListModel.setQty(salesOrgList.get(0).getCallDetail().get(i).getGdQty());
                brList.add(brListModel);
            }

            rcvList.setLayoutManager(new LinearLayoutManager(SentDcrDetailsActivity.this));
            BrSentDetailsListAdapter medicineListAdapter = new BrSentDetailsListAdapter(brList, SentDcrDetailsActivity.this);
            rcvList.setAdapter(medicineListAdapter);
            rcvList.setHasFixedSize(true);
            layoutManager = new LinearLayoutManager(SentDcrDetailsActivity.this);
            Drawable dividerDrawable = ContextCompat.getDrawable(SentDcrDetailsActivity.this, R.drawable.recycler_divider_color);


        }
    }


}
