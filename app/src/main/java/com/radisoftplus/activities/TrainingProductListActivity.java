package com.radisoftplus.activities;

import static com.radisoftplus.utils.SaveLocalStorage.getValueFromSharedPreferences;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.radisoftplus.R;
import com.radisoftplus.adapter.TrainingProductListAdapter;
import com.radisoftplus.models.ApiTrainingProductList;
import com.radisoftplus.utils.ApiService;
import com.radisoftplus.utils.BusyDialog;
import com.radisoftplus.utils.RetroClient;
import com.radisoftplus.utils.UIContent;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TrainingProductListActivity extends AppCompatActivity {

    private static final String TAG = "TrainingProductListActivity";
    private Context mContext;
    UIContent uiContent;

    @BindView(R.id.menu)
    LinearLayout _menu;

    @BindView(R.id.ll_back)
    LinearLayout _back;

    List<ApiTrainingProductList.Results> rList = new ArrayList<>();
    RecyclerView rcvList;
    private BusyDialog mBusyDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_training_product_list);
        ButterKnife.bind(this);
        mContext = this;
        uiContent = new UIContent(TrainingProductListActivity.this);

        rcvList = findViewById(R.id.rcv_list);

        _menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TrainingProductListActivity.this, MainMenuActivity.class);
                startActivity(intent);
            }
        });
        _back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TrainingProductListActivity.this, MainMenuActivity.class);
                startActivity(intent);
                finish();
            }
        });

        loadActivityFromApi();

    }

    public void loadActivityFromApi(){
        mBusyDialog = new BusyDialog(mContext, false, "");
        mBusyDialog.show();
//        final ProgressDialog progressDialog = new ProgressDialog(TrainingProductListActivity.this, R.style.AppTheme_Dark_Dialog);
//        progressDialog.setIndeterminate(true);
//        progressDialog.setMessage("Data loading...");
//        progressDialog.show();

        String month = getValueFromSharedPreferences("Promotional_item_month_name", TrainingProductListActivity.this);

        ApiService api = RetroClient.getApiService();
        Call<ApiTrainingProductList> call = api.API_TRAINING_PRODUCT_LIST_CALL("",month);
        call.enqueue(new Callback<ApiTrainingProductList>() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onResponse(Call<ApiTrainingProductList> call, Response<ApiTrainingProductList> response) {

                if (response.isSuccessful()) {
                    Boolean success = response.body().getSuccess();
                    if(success){

                        rList = response.body().getResults();
                        if(rList.size()>0) {
                            rcvList.setLayoutManager(new LinearLayoutManager(TrainingProductListActivity.this));
                            TrainingProductListAdapter reAdapter = new TrainingProductListAdapter(rList,TrainingProductListActivity.this);
                            rcvList.setAdapter(reAdapter);
                            rcvList.setNestedScrollingEnabled(true);
                            rcvList.setHasFixedSize(true);
                        }


                        mBusyDialog.dismiss();
                    }else{
                        onFailed();
                        mBusyDialog.dismiss();
                        uiContent.showExitDialog(TrainingProductListActivity.this,"Error",response.body().getMessage());
                    }
                } else {
                    onFailed();
                    mBusyDialog.dismiss();
                    uiContent.showExitDialog(TrainingProductListActivity.this, "Error", "Server Error !");
                }
            }

            @Override
            public void onFailure(Call<ApiTrainingProductList> call, Throwable t) {
                Toast.makeText(getBaseContext(), t.getMessage(), Toast.LENGTH_LONG).show();
                mBusyDialog.dismiss();
                uiContent.showExitDialog(TrainingProductListActivity.this, "Warning", "Please check network connection");
            }
        });
    }


    public void onFailed() {
        Toast.makeText(getBaseContext(), "Error", Toast.LENGTH_LONG).show();
    }



}