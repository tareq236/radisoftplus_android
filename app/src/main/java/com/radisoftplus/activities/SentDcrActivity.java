package com.radisoftplus.activities;

import static com.radisoftplus.utils.SaveLocalStorage.getValueFromSharedPreferences;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.radisoftplus.R;
import com.radisoftplus.adapter.SentDcrListAdapter;
import com.radisoftplus.adapter.SentOrderListAdapter;
import com.radisoftplus.models.ApiDcrListModel;
import com.radisoftplus.models.ApiOrderListModel;
import com.radisoftplus.utils.ApiService;
import com.radisoftplus.utils.RetroClient;
import com.radisoftplus.utils.UIContent;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SentDcrActivity extends AppCompatActivity {

    private static final String TAG = "SentDcrActivity";
    private Context mContext;
    UIContent uiContent;

    @BindView(R.id.ll_back)
    LinearLayout _back;

    List<ApiDcrListModel.Result> rList = new ArrayList<>();
    RecyclerView rcvList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sent_dcr);
        ButterKnife.bind(this);
        mContext = this;
        uiContent = new UIContent(SentDcrActivity.this);

        rcvList = findViewById(R.id.rcv_list);
        _back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String back_where = getValueFromSharedPreferences("back_where", SentDcrActivity.this);
                if(back_where.equals("radiant")){
                    Intent intent = new Intent(SentDcrActivity.this, HomeRadiantActivity.class);
                    startActivity(intent);
                    finish();
                }else if(back_where.equals("tareq")) {
                    Intent intent = new Intent(SentDcrActivity.this, HomeActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        });

        loadDataFromApi();
    }

    public void loadDataFromApi(){
        SharedPreferences sh = getSharedPreferences("login_information", MODE_PRIVATE);
        ApiService api = RetroClient.getApiService();
        JsonObject body = new JsonObject();
        body.addProperty("work_area_t", sh.getString("work_area_t", ""));
        body.addProperty("offset", 0);
        body.addProperty("limit", 20);
        Call<ApiDcrListModel> call = api.API_DCR_LIST_MODEL_CALL(body);
        call.enqueue(new Callback<ApiDcrListModel>() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onResponse(Call<ApiDcrListModel> call, Response<ApiDcrListModel> response) {

                if (response.isSuccessful()) {
                    Boolean success = response.body().getSuccess();
                    if(success){

                        rList = response.body().getResult();
                        if(rList.size()>0) {
                            rcvList.setLayoutManager(new LinearLayoutManager(SentDcrActivity.this));
                            SentDcrListAdapter reAdapter = new SentDcrListAdapter(rList,SentDcrActivity.this);
                            rcvList.setAdapter(reAdapter);
                            rcvList.setNestedScrollingEnabled(true);
                            rcvList.setHasFixedSize(true);
                        }
                    }else{
                        onFailed();
                        uiContent.showExitDialog(SentDcrActivity.this,"Error",response.body().getMessage());
                    }
                } else {
                    onFailed();
                    uiContent.showExitDialog(SentDcrActivity.this, "Error", "Server Error !");
                }
            }

            @Override
            public void onFailure(Call<ApiDcrListModel> call, Throwable t) {
                Toast.makeText(getBaseContext(), t.getMessage(), Toast.LENGTH_LONG).show();
                uiContent.showExitDialog(SentDcrActivity.this, "Warning", "Please check network connection");
            }
        });
    }

    public void onFailed() {
        Toast.makeText(getBaseContext(), "Error", Toast.LENGTH_LONG).show();
    }
}
