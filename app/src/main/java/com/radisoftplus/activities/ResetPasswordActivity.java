package com.radisoftplus.activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.gson.JsonObject;
import com.radisoftplus.R;
import com.radisoftplus.models.ApiLoginModel;
import com.radisoftplus.utils.ApiService;
import com.radisoftplus.utils.BusyDialog;
import com.radisoftplus.utils.Netinfo;
import com.radisoftplus.utils.RetroClient;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ResetPasswordActivity extends AppCompatActivity {
    private static final String TAG = "ResetPasswordActivity";
    private Context mContext;

    @BindView(R.id.et_new_password)
    EditText et_new_password;

    @BindView(R.id.et_confirm_password)
    EditText et_confirm_password;
    @BindView(R.id.btn_confirm)
    Button btn_confirm;

    private BusyDialog mBusyDialog;

    private String workAreaT="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);
        ButterKnife.bind(this);
        mContext=this;

        Intent intent = getIntent();
        workAreaT = intent.getStringExtra("work_area_t");

        if (!checkInternet()) {
            onLoginFailed();
            return;
        }

        btn_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirm();
            }
        });
    }

    public void confirm() {
        Log.d(TAG, "Confirm");

        if (!validate()) {
            onLoginFailed();
            return;
        }

        btn_confirm.setEnabled(false);
        btn_confirm.setBackgroundColor(Color.GRAY);

        if (!checkInternet()) {
            onLoginFailed();
            return;
        }

        mBusyDialog = new BusyDialog(mContext, false, "Reset Password...");
        mBusyDialog.show();

        ApiService api = RetroClient.getApiService();
        JsonObject body = new JsonObject();
        String newPassword = et_new_password.getText().toString();
        body.addProperty("work_area_t", workAreaT);
        body.addProperty("password", newPassword);

        Call<ApiLoginModel> call = api.API_CHANGE_FIRST_PASSWORD_CALL(body);
        call.enqueue(new Callback<ApiLoginModel>() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onResponse(Call<ApiLoginModel> call, Response<ApiLoginModel> response) {

                if (response.isSuccessful()) {
                    Boolean success = response.body().getSuccess();
                    if(success){
                        mBusyDialog.dismiss();
                        Intent intent = new Intent(mContext, LoginActivity.class);
                        startActivity(intent);
                        finish();
                    }else {
                        String message = response.body().getMessage();
                        mBusyDialog.dismiss();
                        onLoginFailed();
                        new SweetAlertDialog(mContext, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText(message)
                                .setConfirmText("Close").show();
                    }
                } else {
                    mBusyDialog.dismiss();
                    onLoginFailed();
                    new SweetAlertDialog(mContext, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Server error !")
                            .setConfirmText("Close").show();
                }
            }

            @Override
            public void onFailure(Call<ApiLoginModel> call, Throwable t) {
                mBusyDialog.dismiss();
                onLoginFailed();
                new SweetAlertDialog(mContext, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("Please check network connection")
                        .setConfirmText("Close").show();
            }
        });

    }

    public boolean validate() {
        boolean valid = true;

        String newPassword = et_new_password.getText().toString();
        String confirmPassword = et_confirm_password.getText().toString();

        if (newPassword.isEmpty()) {
            new SweetAlertDialog(mContext, SweetAlertDialog.WARNING_TYPE)
                    .setTitleText("Please enter new password")
                    .setConfirmText("Close").show();
            valid = false;
        }else if (confirmPassword.isEmpty()){
            new SweetAlertDialog(mContext, SweetAlertDialog.WARNING_TYPE)
                    .setTitleText("Please enter confirm password")
                    .setConfirmText("Close").show();
            valid = false;
        }else if(!newPassword.equals(confirmPassword)){
            new SweetAlertDialog(mContext, SweetAlertDialog.WARNING_TYPE)
                    .setTitleText("Password not match")
                    .setConfirmText("Close").show();
            valid = false;
        }


        return valid;
    }

    public void onLoginFailed() {
        btn_confirm.setEnabled(true);
        btn_confirm.setBackgroundColor(getResources().getColor(R.color.green));
    }

    public boolean checkInternet(){
        boolean internet = true;
        if (!Netinfo.isOnline(mContext)) {
            new SweetAlertDialog(mContext, SweetAlertDialog.WARNING_TYPE)
                    .setTitleText("Please check the internet connection !")
                    .setConfirmText("Close").show();
            onLoginFailed();
            internet = false;
        }
        return internet;
    }
}
