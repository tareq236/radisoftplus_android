package com.radisoftplus.activities;



import static com.radisoftplus.utils.SaveLocalStorage.getValueFromSharedPreferences;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;


import com.radisoftplus.R;
import com.radisoftplus.utils.UIContent;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class MediaVideoDetailsActivity extends AppCompatActivity {

    private static final String TAG = "MediaVideoDetailsActivity";
    private Context mContext;
    UIContent uiContent;

    @BindView(R.id.menu)
    LinearLayout _menu;

    @BindView(R.id.ll_back)
    LinearLayout _back;

    @BindView(R.id.txt_title)
    TextView txt_title;

    @BindView(R.id.btn_download_video)
    Button btn_download_video;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_media_video_details);
        ButterKnife.bind(this);
        mContext = this;
        uiContent = new UIContent(MediaVideoDetailsActivity.this);

        String media_type   = getValueFromSharedPreferences("media_type_activity_media_temp", MediaVideoDetailsActivity.this);
        String media_url    = getValueFromSharedPreferences("media_url_temp", MediaVideoDetailsActivity.this);
        String media_path   = getValueFromSharedPreferences("media_path_temp", MediaVideoDetailsActivity.this);

        _menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MediaVideoDetailsActivity.this, MainMenuActivity.class);
                startActivity(intent);
            }
        });
        _back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MediaVideoDetailsActivity.this, MediaListActivity.class);
                startActivity(intent);
                finish();
            }
        });

        btn_download_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new SweetAlertDialog(mContext, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Permission Not Granted")
                        .setConfirmText("DENY")
                        .show();
            }
        });

        String frameVideo = "<html><body><iframe width='100%' height='315' src='https://drive.google.com/file/d/"+media_path+"/preview' frameborder='0' allowfullscreen></iframe></body></html>";

        WebView myWebView = (WebView) findViewById(R.id.web_view);

//        WebSettings webSettings = myWebView.getSettings();
//        webSettings.setJavaScriptEnabled(true);
//        myWebView.loadData(frameVideo, "text/html", "utf-8");

        CookieManager.getInstance().setAcceptCookie(true);
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            CookieManager.getInstance().setAcceptThirdPartyCookies(myWebView, true);
        }

        WebSettings webSettings = myWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setDomStorageEnabled(true);

        myWebView.setWebViewClient(new WebViewClient());
//        myWebView.setWebChromeClient(new MyWebChromeClient());
        myWebView.loadData(frameVideo, "text/html", "utf-8");


    }
}