package com.radisoftplus.activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.radisoftplus.R;
import com.radisoftplus.models.ApiLoginModel;
import com.radisoftplus.utils.ApiService;
import com.radisoftplus.utils.BusyDialog;
import com.radisoftplus.utils.Netinfo;
import com.radisoftplus.utils.RetroClient;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgetPasswordActivity extends AppCompatActivity {
    private static final String TAG = "ForgetPasswordActivity";
    private Context mContext;

    @BindView(R.id.txt_back_login)
    TextView txt_back_login;

    @BindView(R.id.et_mobile_number)
    EditText et_mobile_number;

    @BindView(R.id.btn_reset)
    Button btn_reset;

    private BusyDialog mBusyDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);
        ButterKnife.bind(this);
        mContext=this;

        txt_back_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });

        btn_reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                send_otp();
            }
        });

    }

    public boolean validate() {
        boolean valid = true;

        String mobileNumber = et_mobile_number.getText().toString();

        if (mobileNumber.isEmpty()) {
            new SweetAlertDialog(mContext, SweetAlertDialog.WARNING_TYPE)
                    .setTitleText("Please enter your mobile number")
                    .setConfirmText("Close").show();
            valid = false;
        }


        return valid;
    }

    public void send_otp() {
        Log.d(TAG, "send_otp");

        if (!validate()) {
            onLoginFailed();
            return;
        }

        btn_reset.setEnabled(false);
        btn_reset.setBackgroundColor(Color.GRAY);

        if (!checkInternet()) {
            onLoginFailed();
            return;
        }

        mBusyDialog = new BusyDialog(mContext, false, "Login...");
        mBusyDialog.show();

        ApiService api = RetroClient.getApiService();
        JsonObject body = new JsonObject();
        String mobileNumber = et_mobile_number.getText().toString();
        if (mobileNumber.length() >= 2 && mobileNumber.substring(0, 2).equals("88")) {
            mobileNumber = mobileNumber.substring(2);
        }
        body.addProperty("mobile_number", "88"+mobileNumber);

        Call<ApiLoginModel> call = api.API_CHECK_MOBILE_CALL(body);
        call.enqueue(new Callback<ApiLoginModel>() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onResponse(Call<ApiLoginModel> call, Response<ApiLoginModel> response) {

                if (response.isSuccessful()) {
                    Boolean success = response.body().getSuccess();
                    if(success){
                        mBusyDialog.dismiss();
                        Intent intent = new Intent(mContext, MobileVerificationActivity.class);
                        intent.putExtra("mobile_number", response.body().getResult().getMobileNumber().toString());
                        startActivity(intent);
                        finish();
                    }else {
                        String message = response.body().getMessage();
                        mBusyDialog.dismiss();
                        onLoginFailed();
                        new SweetAlertDialog(mContext, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText(message)
                                .setConfirmText("Close").show();
                    }
                } else {
                    mBusyDialog.dismiss();
                    onLoginFailed();
                    new SweetAlertDialog(mContext, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Server error !")
                            .setConfirmText("Close").show();
                }
            }

            @Override
            public void onFailure(Call<ApiLoginModel> call, Throwable t) {
                mBusyDialog.dismiss();
                onLoginFailed();
                new SweetAlertDialog(mContext, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("Please check network connection")
                        .setConfirmText("Close").show();
            }
        });

    }

    public void onLoginFailed() {
        btn_reset.setEnabled(true);
        btn_reset.setBackgroundColor(getResources().getColor(R.color.green));
    }

    public boolean checkInternet(){
        boolean internet = true;
        if (!Netinfo.isOnline(mContext)) {
            new SweetAlertDialog(mContext, SweetAlertDialog.WARNING_TYPE)
                    .setTitleText("Please check the internet connection !")
                    .setConfirmText("Close").show();
            onLoginFailed();
            internet = false;
        }
        return internet;
    }
}
