package com.radisoftplus.activities;

import static com.radisoftplus.utils.SaveLocalStorage.saveToSharedPreferences;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.radisoftplus.R;

public class DcrOptionsDialogFragment extends DialogFragment {

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(requireContext()).inflate(R.layout.dcr_options_dialog, null);
        LinearLayout btnNewCall = view.findViewById(R.id.btn_new_call);
        LinearLayout btnDraftCall = view.findViewById(R.id.btn_draft_call);
        LinearLayout btnSentCall = view.findViewById(R.id.btn_sent_call);
        LinearLayout btnGadgetUtilization = view.findViewById(R.id.btn_gadget_utilization);

        // Set click listeners for the buttons to handle the actions
        btnNewCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Handle "New Order" action
                dismiss();
                saveToSharedPreferences("dcr-call-date",null, requireContext());
                saveToSharedPreferences("dcr-sessions",null, requireContext());
                saveToSharedPreferences("dcr-chamber-type",null, requireContext());
                saveToSharedPreferences("dcr-call-type",null, requireContext());
                saveToSharedPreferences("dcr-dr-child-id",null, requireContext());
                saveToSharedPreferences("dcr-remarks",null, requireContext());
                saveToSharedPreferences("find-doctor-name", null, requireContext());
                saveToSharedPreferences("find-dr-child-id", null, requireContext());
                saveToSharedPreferences("br-list",null, requireContext());
                saveToSharedPreferences("back_where","radiant", requireContext());
                Intent intent = new Intent(requireContext(), DcrScreenOneActivity.class);
                startActivity(intent);
//                requireActivity().finish();
            }
        });

        btnDraftCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Handle "Draft Order" action
                dismiss();
                saveToSharedPreferences("back_where","radiant", requireContext());
                Intent intent = new Intent(requireContext(), DcrDraftActivity.class);
                startActivity(intent);
//                requireActivity().finish();
            }
        });

        btnSentCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Handle "Sent Order" action
                dismiss();
                saveToSharedPreferences("back_where","radiant", requireContext());
                Intent intent = new Intent(requireContext(), SentDcrActivity.class);
                startActivity(intent);
//                requireActivity().finish();
            }
        });

        btnGadgetUtilization.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Handle "Order Report" action
                dismiss();
                saveToSharedPreferences("back_where","radiant", requireContext());
                Intent intent = new Intent(requireContext(), GadgetUtilizationActivity.class);
                startActivity(intent);
//                requireActivity().finish();
            }
        });

        Dialog dialog = new Dialog(requireContext());
        dialog.setContentView(view);

        // Set the width of the dialog to 200dp
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams((int) getResources().getDimension(R.dimen.dialog_width), ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setLayout(layoutParams.width, layoutParams.height);

        // Set the dialog to be centered
        dialog.getWindow().setGravity(Gravity.CENTER);

        return dialog;
    }

    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        super.onDismiss(dialog);

        // Hide the darkOverlay when the dialog is dismissed
        ((HomeRadiantActivity) requireActivity()).hideDarkOverlay();
    }
}

