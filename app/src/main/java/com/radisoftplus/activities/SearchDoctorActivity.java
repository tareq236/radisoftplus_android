package com.radisoftplus.activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.cooltechworks.views.shimmer.ShimmerRecyclerView;
import com.google.gson.JsonObject;
import com.radisoftplus.R;
import com.radisoftplus.adapter.SearchChemistAdapter;
import com.radisoftplus.adapter.SearchDoctorAdapter;
import com.radisoftplus.models.ApiFindCustomerModel;
import com.radisoftplus.models.ApiFindDoctorModel;
import com.radisoftplus.utils.ApiService;
import com.radisoftplus.utils.RetroClient;
import com.radisoftplus.utils.UIContent;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchDoctorActivity extends AppCompatActivity {

    private static final String TAG = "SearchDoctorActivity";
    private Context mContext;
    UIContent uiContent;

    @BindView(R.id.ll_back)
    LinearLayout _back;

    @BindView(R.id.et_doctor)
    EditText et_doctor;

    List<ApiFindDoctorModel.Result> rList = new ArrayList<>();
    ShimmerRecyclerView rcvList;
    SearchDoctorAdapter reAdapter;
    LinearLayoutManager linearLayoutManager;

    String dcr_from_screen = "";
    int offset = 0;
    int limit = 50;
    private boolean isLastPage = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_doctor);
        ButterKnife.bind(this);
        mContext = this;
        uiContent = new UIContent(SearchDoctorActivity.this);

        rcvList = findViewById(R.id.rcv_list);
        dcr_from_screen = getIntent().getStringExtra("dcr-from-screen");
        reAdapter = new SearchDoctorAdapter(SearchDoctorActivity.this, dcr_from_screen);
        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rcvList.setLayoutManager(new LinearLayoutManager(SearchDoctorActivity.this));
        rcvList.setNestedScrollingEnabled(true);
        rcvList.setHasFixedSize(true);

        rcvList.addOnScrollListener(new RecyclerView.OnScrollListener()
        {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0)
                {
                    if (!recyclerView.canScrollVertically(1)) {
                        if(!isLastPage){
                            offset = offset + rList.size();
                            loadDataFromApi("");
                        }
                    }
                }
            }
        });

        _back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(dcr_from_screen.equals("DcrScreenOneActivity")){
                    Intent intent = new Intent(SearchDoctorActivity.this, DcrScreenOneActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        });

        et_doctor.requestFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(et_doctor, InputMethodManager.SHOW_IMPLICIT);

        et_doctor.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                loadDataFromApi(s.toString());
            }
        });

        loadDataFromApi("");

    }

    public void loadDataFromApi(String s) {
        rcvList.showShimmerAdapter();
        SharedPreferences sh = getSharedPreferences("login_information", MODE_PRIVATE);
        ApiService api = RetroClient.getApiService();
        JsonObject body = new JsonObject();
        body.addProperty("work_area_t", sh.getString("work_area_t", ""));
        body.addProperty("doctor_name", s);
        if(!s.equals("")) {
            reAdapter.clearData();
            offset = 0;
        }
        body.addProperty("offset", offset);
        body.addProperty("limit", limit);
        Call<ApiFindDoctorModel> call = api.API_FIND_DOCTOR_MODEL_CALL(body);
        call.enqueue(new Callback<ApiFindDoctorModel>() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onResponse(Call<ApiFindDoctorModel> call, Response<ApiFindDoctorModel> response) {

                if (response.isSuccessful()) {
                    Boolean success = response.body().getSuccess();
                    if (success) {
                        rList = response.body().getResult();
                        if(rList != null){
                            if(rList.size()>0){
                                rcvList.hideShimmerAdapter();
                                reAdapter.addData(rList);
                                rcvList.setAdapter(reAdapter);
                                rcvList.scrollToPosition(offset-1);
                                if(rList.size()<limit){
                                    isLastPage = true;
                                }
                            }else {
                                isLastPage = true;
                                rcvList.hideShimmerAdapter();
                                rcvList.scrollToPosition(offset-1);
                            }
                        }else{
                            isLastPage = true;
                            rcvList.hideShimmerAdapter();
                            rcvList.scrollToPosition(offset-1);
                        }

                    } else {
                        rcvList.hideShimmerAdapter();
                        onFailed();
                        uiContent.showExitDialog(SearchDoctorActivity.this, "Error", response.body().getMessage());
                    }
                } else {
                    rcvList.hideShimmerAdapter();
                    onFailed();
                    uiContent.showExitDialog(SearchDoctorActivity.this, "Error", "Server Error !");
                }
            }

            @Override
            public void onFailure(Call<ApiFindDoctorModel> call, Throwable t) {
                rcvList.hideShimmerAdapter();
                Toast.makeText(getBaseContext(), t.getMessage(), Toast.LENGTH_LONG).show();
                uiContent.showExitDialog(SearchDoctorActivity.this, "Warning", "Please check network connection");
            }
        });
    }


    public void onFailed() {
        Toast.makeText(getBaseContext(), "Error", Toast.LENGTH_LONG).show();
    }
}