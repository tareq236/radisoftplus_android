package com.radisoftplus.activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.JsonObject;
import com.radisoftplus.R;
import com.radisoftplus.adapter.VisitPlanListAdapter;
import com.radisoftplus.models.ApiTodayOrderInfoModel;
import com.radisoftplus.models.ApiTodayVisitPlanModel;
import com.radisoftplus.utils.ApiService;
import com.radisoftplus.utils.BusyDialog;
import com.radisoftplus.utils.RetroClient;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static com.radisoftplus.utils.SaveLocalStorage.saveToSharedPreferences;

public class HomeActivity extends AppCompatActivity {
    private static final String TAG = "HomeActivity";
    private Context mContext;

    private BusyDialog mBusyDialog;
    public static final String SHARED_PREFS = "login_information";

    //List<ApiTodayVisitPlanModel.Result> dataList = new ArrayList<>();
    RecyclerView.LayoutManager layoutManager;
    RecyclerView rcvList;

    @BindView(R.id.btn_more)
    LinearLayout btn_more;

    @BindView(R.id.btn_report)
    LinearLayout btn_report;

    @BindView(R.id.btn_doctor)
    LinearLayout btn_doctor;

    @BindView(R.id.totalOrder)
    TextView totalOrderId;

    @BindView(R.id.totalTP)
    TextView totalTPId;

    @BindView(R.id.totalQty)
    TextView totalQtyId;

    @BindView(R.id.totalChemist)
    TextView totalChemistId;
    @BindView(R.id.fab)
    FloatingActionButton btnOrder;

    SharedPreferences sh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        mContext=this;

        sh = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);

        saveToSharedPreferences("find-word_area-work_area",sh.getString("work_area_t", ""), mContext);
        saveToSharedPreferences("find-word_area-address",sh.getString("address", ""), mContext);
        saveToSharedPreferences("find-customer-partner",null, mContext);
        saveToSharedPreferences("find-customer-customer_name",null, mContext);
        saveToSharedPreferences("order-delivery_date",null, mContext);
        saveToSharedPreferences("order-payment_mode",null, mContext);
        saveToSharedPreferences("order-emp_id_cr_team",null, mContext);
        saveToSharedPreferences("order-list",null, mContext);

        rcvList = findViewById(R.id.rcv_list);

        loadData();

        btn_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, MainMenuActivity.class);
                startActivity(intent);
            }
        });

        btnOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                new SweetAlertDialog(mContext, SweetAlertDialog.WARNING_TYPE)
//                        .setTitleText("Under construction ")
//                        .setConfirmText("Close").show();
                Intent intent = new Intent(HomeActivity.this,OrderActivity.class);
                intent.putExtra("is_draft", false);
                startActivity(intent);
            }
        });

        btn_report.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                new SweetAlertDialog(mContext, SweetAlertDialog.WARNING_TYPE)
//                        .setTitleText("Under construction ")
//                        .setConfirmText("Close").show();
                startActivity(new Intent(HomeActivity.this,OrderReportActivity.class));
            }
        });

        btn_doctor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new SweetAlertDialog(mContext, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("Under construction ")
                        .setConfirmText("Close").show();
            }
        });

    }

    public void loadData() {
        Log.d(TAG, "Loading Data");

        mBusyDialog = new BusyDialog(mContext, false, "");
        mBusyDialog.show();

        // API CALL
        ApiService api = RetroClient.getApiService();
        String workArea = sh.getString("work_area_t", "");

        Call<ApiTodayOrderInfoModel> call = api.API_TODAY_ORDER_INFO_MODEL_CALL(workArea);
        call.enqueue(new Callback<ApiTodayOrderInfoModel>() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onResponse(Call<ApiTodayOrderInfoModel> call, Response<ApiTodayOrderInfoModel> response) {

                if (response.isSuccessful()) {
                    Boolean success = response.body().getSuccess();
                    if(success){

                            ApiTodayOrderInfoModel.Result data = response.body().getResult();
                             if(data!=null){
                                 String totalOrder = data.getTotalOrder();
                                 String totalCustomer = data.getTotalCustomer();
                                 String totalTP = data.getTotalTp();
                                 String totalQTY = data.getTotalQty();
                                 if(totalCustomer.equals("")){
                                     totalChemistId.setText(totalCustomer);
                                 }else {
                                     totalChemistId.setText(totalCustomer);
                                 }
                                 if(totalOrder.equals("")){
                                     totalOrderId.setText("0");
                                 }else {
                                     totalOrderId.setText(totalOrder);
                                 }
                                 if(totalTP.equals("")){
                                     totalTPId.setText("0");
                                 }else {
                                     totalTPId.setText(totalTP);
                                 }
                                 if(totalQTY.equals("")){
                                     totalQtyId.setText("0");
                                 }else {
                                     totalQtyId.setText(totalQTY);
                                 }



                             }else {
                                 new SweetAlertDialog(mContext, SweetAlertDialog.ERROR_TYPE)
                                         .setTitleText(response.body().getMessage())
                                         .setConfirmText("Close").show();
                             }
                            mBusyDialog.dismiss();
                        }

                } else {
                    mBusyDialog.dismiss();
                    new SweetAlertDialog(mContext, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Server error !")
                            .setConfirmText("Close").show();
                }
            }

            @Override
            public void onFailure(Call<ApiTodayOrderInfoModel> call, Throwable t) {
                mBusyDialog.dismiss();
                new SweetAlertDialog(mContext, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("Please check network connection")
                        .setConfirmText("Close").show();
            }
        });

    }
}
