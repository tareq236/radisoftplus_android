package com.radisoftplus.activities;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.JsonObject;
import com.radisoftplus.R;
import com.radisoftplus.activities.ForgetPasswordActivity;
import com.radisoftplus.activities.HomeRadiantActivity;
import com.radisoftplus.activities.ResetPasswordActivity;
import com.radisoftplus.models.ApiLoginModel;
import com.radisoftplus.utils.ApiService;
import com.radisoftplus.utils.BusyDialog;
import com.radisoftplus.utils.Netinfo;
import com.radisoftplus.utils.RetroClient;
import android.widget.CompoundButton;

public class LoginActivity extends AppCompatActivity {

    private static final String TAG = "LoginActivity";
    private Context mContext;

    @BindView(R.id.et_work_area)
    EditText et_work_area;
    @BindView(R.id.et_password)
    EditText et_password;
    @BindView(R.id.txt_forget_password)
    TextView txt_forget_password;

    @BindView(R.id.btn_login)
    Button btn_login;

    private String token;

    private BusyDialog mBusyDialog;

    public static final String SHARED_PREFS = "login_information";

    private CheckBox rememberMeCheckBox;
    private TextView txvFaq;
    private Boolean isRemember = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        mContext=this;

        rememberMeCheckBox = findViewById(R.id.chk_remember_me);
        txvFaq = findViewById(R.id.txv_faq);
        rememberMeCheckBox.setChecked(true);

//        et_work_area.setText("11001");
//        et_password.setText("Rs@123");

        if (!checkInternet()) {
            onLoginFailed();
            return;
        }

        FirebaseMessaging.getInstance().getToken().addOnCompleteListener(new OnCompleteListener<String>() {
            @Override
            public void onComplete(@NonNull Task<String> task) {
                if (!task.isSuccessful()) {
                    Log.w(TAG, "Fetching FCM registration token failed", task.getException());
                    return;
                }
                token = task.getResult();
            }
        });

        txvFaq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, WebViewActivity.class);
                startActivity(intent);
            }
        });

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();
            }
        });

        txt_forget_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, ForgetPasswordActivity.class);
                startActivity(intent);
                finish();
            }
        });

        rememberMeCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // Handle the check and uncheck events here
                if (isChecked) {
                    // Checkbox is checked
                    // Do something when the checkbox is checked
                } else {
                    // Checkbox is unchecked
                    // Do something when the checkbox is unchecked
                }
            }
        });

    }

    public void login() {
        Log.d(TAG, "Login");

        if (!validate()) {
            onLoginFailed();
            return;
        }

        btn_login.setEnabled(false);
        btn_login.setBackgroundColor(Color.GRAY);

        if (!checkInternet()) {
            onLoginFailed();
            return;
        }

        mBusyDialog = new BusyDialog(mContext, false, "Login...");
        mBusyDialog.show();

        ApiService api = RetroClient.getApiService();
        JsonObject body = new JsonObject();
        String workAreaT = et_work_area.getText().toString();
        String password = et_password.getText().toString();
        body.addProperty("work_area_t", workAreaT);
        body.addProperty("password", password);
        body.addProperty("token", token);

        Call<ApiLoginModel> call = api.API_LOGIN_CALL(body);
        call.enqueue(new Callback<ApiLoginModel>() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onResponse(Call<ApiLoginModel> call, Response<ApiLoginModel> response) {

                if (response.isSuccessful()) {
                    Boolean success = response.body().getSuccess();
                    if(success){
                        mBusyDialog.dismiss();
                        if (response.body().getResult().getPasswordChange() == 0){
                            Intent intent = new Intent(mContext, ResetPasswordActivity.class);
                            intent.putExtra("work_area_t", response.body().getResult().getWorkAreaT().toString());
                            startActivity(intent);
                            finish();
                        }else if(response.body().getResult().getPasswordChange() == 1){
                            SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
                            SharedPreferences.Editor editPrefs= sharedPreferences.edit();
                            editPrefs.putString("user_name", response.body().getResult().getName());
                            editPrefs.putString("name", response.body().getResult().getName());
                            editPrefs.putString("work_area_t", response.body().getResult().getWorkAreaT());
                            editPrefs.putString("address", response.body().getResult().getAddress());
                            editPrefs.putString("team", response.body().getResult().getGroupName());
                            editPrefs.putString("group_name", response.body().getResult().getGroupName());
                            editPrefs.putString("mobile_number", response.body().getResult().getMobileNumber());
                            editPrefs.putBoolean("isLogin", true);
                            editPrefs.commit();

                            mBusyDialog.dismiss();
                            Intent intent = new Intent(mContext, HomeRadiantActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    }else {
                        String message = response.body().getMessage();
                        mBusyDialog.dismiss();
                        onLoginFailed();
                        new SweetAlertDialog(mContext, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText(message)
                                .setConfirmText("Close").show();
                    }
                } else {
                    mBusyDialog.dismiss();
                    onLoginFailed();
                    new SweetAlertDialog(mContext, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Server error !")
                            .setConfirmText("Close").show();
                }
            }

            @Override
            public void onFailure(Call<ApiLoginModel> call, Throwable t) {
                mBusyDialog.dismiss();
                onLoginFailed();
                new SweetAlertDialog(mContext, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("Please check network connection")
                        .setConfirmText("Close").show();
            }
        });

    }

    public boolean validate() {
        boolean valid = true;

        String workArea = et_work_area.getText().toString();
        String password = et_password.getText().toString();

        if (workArea.isEmpty()) {
            new SweetAlertDialog(mContext, SweetAlertDialog.WARNING_TYPE)
                    .setTitleText("Please enter your work area")
                    .setConfirmText("Close").show();
            valid = false;
        }else if (password.isEmpty()){
            new SweetAlertDialog(mContext, SweetAlertDialog.WARNING_TYPE)
                    .setTitleText("Please enter password")
                    .setConfirmText("Close").show();
            valid = false;
        }


        return valid;
    }

    public void onLoginFailed() {
        btn_login.setEnabled(true);
        btn_login.setBackgroundColor(getResources().getColor(R.color.green));
    }

    public boolean checkInternet(){
        boolean internet = true;
        if (!Netinfo.isOnline(mContext)) {
            new SweetAlertDialog(mContext, SweetAlertDialog.WARNING_TYPE)
                    .setTitleText("Please check the internet connection !")
                    .setConfirmText("Close").show();
            onLoginFailed();
            internet = false;
        }
        return internet;
    }

}
