package com.radisoftplus.activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.radisoftplus.R;
import com.radisoftplus.adapter.SearchProductAdapter;
import com.radisoftplus.adapter.SearchWorkAreaAdapter;
import com.radisoftplus.models.ApiFindWorkAreaModel;
import com.radisoftplus.utils.ApiService;
import com.radisoftplus.utils.RetroClient;
import com.radisoftplus.utils.UIContent;

import java.util.ArrayList;
import java.util.List;

public class SearchWorkAreaActivity extends AppCompatActivity {

    private static final String TAG = "SearchWorkAreaActivity";
    private Context mContext;
    UIContent uiContent;

    @BindView(R.id.ll_back)
    LinearLayout _back;

    @BindView(R.id.et_work_area)
    EditText et_work_area;

    List<ApiFindWorkAreaModel.Result> rList = new ArrayList<>();
    RecyclerView rcvList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_work_area);
        ButterKnife.bind(this);
        mContext = this;
        uiContent = new UIContent(SearchWorkAreaActivity.this);

        rcvList = findViewById(R.id.rcv_list);

        _back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SearchWorkAreaActivity.this, OrderActivity.class);
                startActivity(intent);
                finish();
            }
        });

        et_work_area.requestFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(et_work_area, InputMethodManager.SHOW_IMPLICIT);

        et_work_area.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {}

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length() != 0){
                    loadDataFromApi(s.toString());
                }else{
                    rList.clear();
                    rcvList.setLayoutManager(new LinearLayoutManager(SearchWorkAreaActivity.this));
                    SearchWorkAreaAdapter reAdapter = new SearchWorkAreaAdapter(rList,SearchWorkAreaActivity.this);
                    rcvList.setAdapter(reAdapter);
                    rcvList.setNestedScrollingEnabled(true);
                    rcvList.setHasFixedSize(true);
                }
            }
        });

    }

    public void loadDataFromApi(String s){

        ApiService api = RetroClient.getApiService();
        JsonObject body = new JsonObject();
        body.addProperty("work_area_t", s);
        body.addProperty("offset", "0");
        body.addProperty("limit", "50");
        Call<ApiFindWorkAreaModel> call = api.API_FIND_WORK_AREA_MODEL_CALL(body);
        call.enqueue(new Callback<ApiFindWorkAreaModel>() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onResponse(Call<ApiFindWorkAreaModel> call, Response<ApiFindWorkAreaModel> response) {

                if (response.isSuccessful()) {
                    Boolean success = response.body().getSuccess();
                    if(success){

                        rList = response.body().getResult();
                        if(rList.size()>0) {
                            rcvList.setLayoutManager(new LinearLayoutManager(SearchWorkAreaActivity.this));
                            SearchWorkAreaAdapter reAdapter = new SearchWorkAreaAdapter(rList,SearchWorkAreaActivity.this);
                            rcvList.setAdapter(reAdapter);
                            rcvList.setNestedScrollingEnabled(true);
                            rcvList.setHasFixedSize(true);
                        }
                    }else{
                        onFailed();
                        uiContent.showExitDialog(SearchWorkAreaActivity.this,"Error",response.body().getMessage());
                    }
                } else {
                    onFailed();
                    uiContent.showExitDialog(SearchWorkAreaActivity.this, "Error", "Server Error !");
                }
            }

            @Override
            public void onFailure(Call<ApiFindWorkAreaModel> call, Throwable t) {
                Toast.makeText(getBaseContext(), t.getMessage(), Toast.LENGTH_LONG).show();
                uiContent.showExitDialog(SearchWorkAreaActivity.this, "Warning", "Please check network connection");
            }
        });
    }

    public void onFailed() {
        Toast.makeText(getBaseContext(), "Error", Toast.LENGTH_LONG).show();
    }
}