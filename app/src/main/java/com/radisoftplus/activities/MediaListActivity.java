package com.radisoftplus.activities;



import static com.radisoftplus.utils.SaveLocalStorage.getValueFromSharedPreferences;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.radisoftplus.R;
import com.radisoftplus.adapter.MediaListAdapter;
import com.radisoftplus.models.ApiMediaModel;
import com.radisoftplus.utils.UIContent;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MediaListActivity extends AppCompatActivity {

    private static final String TAG = "MediaListActivity";
    private Context mContext;
    UIContent uiContent;

    @BindView(R.id.menu)
    LinearLayout _menu;

    @BindView(R.id.ll_back)
    LinearLayout _back;

    @BindView(R.id.txt_title)
    TextView txt_title;

    RecyclerView rcvList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_media_list);
        ButterKnife.bind(this);
        mContext = this;
        uiContent = new UIContent(MediaListActivity.this);

        rcvList = findViewById(R.id.rcv_list);

        String media_id = getValueFromSharedPreferences("media_id_temp", MediaListActivity.this);
        String from = getValueFromSharedPreferences("from_media_temp", MediaListActivity.this);
        String media_activity = getValueFromSharedPreferences("media_activity_media_temp", MediaListActivity.this);
        String media_type = getValueFromSharedPreferences("media_type_activity_media_temp", MediaListActivity.this);


        _menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MediaListActivity.this, MainMenuActivity.class);
                startActivity(intent);
            }
        });
        _back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(from.equals("TrainingProductDetailsCommonActivity")){
                    Intent intent = new Intent(MediaListActivity.this, TrainingProductDetailsCommonActivity.class);
                    startActivity(intent);
                    finish();
                }else{
                    // Intent intent = new Intent(MediaListActivity.this, MediaActivity.class);
                    Intent intent = new Intent(MediaListActivity.this, TrainingProductDetailsActivity.class);
                    startActivity(intent);
                    finish();
                }

            }
        });

//        if(media_type.equals("pdf")){
//            txt_title.setText("PDF");
//        }else if(media_type.equals("image")){
//            txt_title.setText("Image");
//        }else if(media_type.equals("video")){
//            txt_title.setText("Video");
//        }
        String title  = getValueFromSharedPreferences("media_type_title", MediaListActivity.this);
        txt_title.setText(title);

        String url = "http://order.radisoftplus.com/api/v1/order_app/other_function/training_media/all_files/";
//        if(media_activity.equals("training") && media_type.equals("pdf")){
//            url = "http://116.68.200.97:56008/training_media/pdf/";
//        }
//        if(media_activity.equals("training") && media_type.equals("image")){
//            url = "http://116.68.200.97:56008/training_media/image/";
//        }
//        if(media_activity.equals("training") && media_type.equals("video")){
//            url = "http://116.68.200.97:56008/training_media/video/";
//        }

        String _items = getValueFromSharedPreferences("product_details", MediaListActivity.this);
        Gson _gson = new Gson();
        ApiMediaModel.Results[] _items_obj= _gson.fromJson(_items, ApiMediaModel.Results[].class);
        List<ApiMediaModel.Results> _rListTemp = new ArrayList<>();
        _rListTemp = Arrays.asList(_items_obj);

        List<ApiMediaModel.Results> _rList = new ArrayList<>();
        for(int i = 0; i<_rListTemp.size(); i++){
            if(_rListTemp.get(i).getFile_type().equals(media_type)){
                if(_rListTemp.get(i).getItem_type().equals(media_activity)){
                    _rList.add(_rListTemp.get(i));
                }
            }
        }


        String items = getValueFromSharedPreferences("product_details", MediaListActivity.this);
        Gson gson = new Gson();
        ApiMediaModel.Results[] items_obj= gson.fromJson(items, ApiMediaModel.Results[].class);
        List<ApiMediaModel.Results> rListTemp = new ArrayList<>();
        rListTemp = Arrays.asList(items_obj);

        if(_rList.size() == 0){

        }else{
            List<ApiMediaModel.MediaList> rList = new ArrayList<>();
            for(int i = 0; i<rListTemp.size(); i++){
//            if(rListTemp.get(i).getId().toString().equals(media_id)){
//                rList = rListTemp.get(i).getMedia_list();
//            }
                if(rListTemp.get(i).getId().toString().equals(_rList.get(0).getId().toString())){
                    rList = rListTemp.get(i).getMedia_list();
                }
            }

            if(rList.size()>0) {
                GridLayoutManager gridLayoutManager = new GridLayoutManager(MediaListActivity.this,2);
                rcvList.setLayoutManager(gridLayoutManager);
                MediaListAdapter reAdapter = new MediaListAdapter(rList, url, MediaListActivity.this);
                rcvList.setAdapter(reAdapter);
                rcvList.setNestedScrollingEnabled(true);
                rcvList.setHasFixedSize(true);
            }
        }
    }
}