package com.radisoftplus.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import com.radisoftplus.R;

public class SplashScreenActivity extends AppCompatActivity {

    public static final String SHARED_PREFS = "login_information";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        SharedPreferences sh = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        Boolean isLogin = sh.getBoolean("isLogin", false);
        Intent intent;
        if(isLogin){
//            intent = new Intent(this, HomeActivity.class);
            intent = new Intent(this, HomeRadiantActivity.class);
        }else{
            intent = new Intent(this, LoginActivity.class);
        }
        startActivity(intent);
        finish();
    }
}
