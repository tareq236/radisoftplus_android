package com.radisoftplus.activities;

import static com.radisoftplus.utils.SaveLocalStorage.getValueFromSharedPreferences;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.squareup.picasso.Picasso;
import com.radisoftplus.R;
import com.radisoftplus.utils.UIContent;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class QuizStartActivity extends AppCompatActivity {

    private static final String TAG = "QuizStartActivity";
    private Context mContext;
    UIContent uiContent;

    @BindView(R.id.menu)
    LinearLayout _menu;

    @BindView(R.id.back)
    LinearLayout _back;

    @BindView(R.id.doctor_ch_id)
    TextView doctor_ch_id;

    @BindView(R.id.doctor_name)
    TextView doctor_name;

    @BindView(R.id.doctor_contact_no)
    TextView doctor_contact_no;

    @BindView(R.id.doctor_specialty)
    TextView doctor_specialty;

    @BindView(R.id.start_quiz)
    LinearLayout start_quiz;

    @BindView(R.id.quiz_image_view)
    ImageView _image_view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_start);
        ButterKnife.bind(this);
        mContext = this;
        uiContent = new UIContent(QuizStartActivity.this);


        _menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(QuizStartActivity.this, MainMenuActivity.class);
                startActivity(intent);
            }
        });
        _back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String qs_from_screen   = getValueFromSharedPreferences("qs_from_screen", mContext);
                if(qs_from_screen.equals("Quiz")){
                    Intent intent = new Intent(QuizStartActivity.this, QuizsActivity.class);
                    startActivity(intent);
                    finish();
                }else{
                    Intent intent = new Intent(QuizStartActivity.this, QuizEnterDoctorActivity.class);
                    startActivity(intent);
                    finish();
                }

            }
        });

        String quiz_dr_child_id   = getValueFromSharedPreferences("quiz_dr_child_id", QuizStartActivity.this);
        String quiz_doctor_name    = getValueFromSharedPreferences("quiz_doctor_name", QuizStartActivity.this);
        String quiz_doctor_cell_phone   = getValueFromSharedPreferences("quiz_doctor_cell_phone", QuizStartActivity.this);
        String quiz_doctor_speciality   = getValueFromSharedPreferences("quiz_doctor_speciality", QuizStartActivity.this);

        doctor_ch_id.setText("Doctor Chamber ID: "+quiz_dr_child_id);
        doctor_name.setText("Doctor Name: "+quiz_doctor_name);
        doctor_contact_no.setText("Contact Number: "+quiz_doctor_cell_phone);
        doctor_specialty.setText("Doctor Speciality: "+quiz_doctor_speciality);

        String quiz_image = getValueFromSharedPreferences("quiz_image", mContext);
        if(quiz_image != null){
            _image_view.setVisibility(View.VISIBLE);
            Picasso.get().load(quiz_image).into(_image_view);
        }


        start_quiz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String quiz_type = getValueFromSharedPreferences("quiz_type", mContext);
                if(quiz_type.equals("text")){
                    Intent intent = new Intent(QuizStartActivity.this, QuizTextActivity.class);
                    startActivity(intent);
                }else{
                    Intent intent = new Intent(QuizStartActivity.this, QuizActivity.class);
                    startActivity(intent);
                }

            }
        });

    }

//    @OnClick(R.id.bt_logo)
//    void homeButton(View view) {
//        Intent intent = new Intent(mContext, HomeActivity.class);
//        startActivity(intent);
//    }

}