package com.radisoftplus.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ApiDcrListModel {
    @SerializedName("error")
    @Expose
    private String error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("result")
    @Expose
    private List<ApiDcrListModel.Result> result = null;
    @SerializedName("success")
    @Expose
    private Boolean success;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Result> getResult() {
        return result;
    }

    public void setResult(List<Result> result) {
        this.result = result;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public class Result {
        @Expose
        @SerializedName("id")
        private int id;

        @SerializedName("work_area_t")
        private String workAreaT;

        @Expose
        @SerializedName("user_details")
        private UserDetails userDetails;

        @SerializedName("call_date")
        private String callDate;

        @Expose
        @SerializedName("sessions")
        private int sessions;

        @Expose
        @SerializedName("location")
        private int location;

        @Expose
        @SerializedName("doctor_id")
        private String doctorId;

        @Expose
        @SerializedName("call_doctor_details")
        private CallDoctorDetails callDoctorDetails;

        @Expose
        @SerializedName("call_type")
        private String callType;

        @Expose
        @SerializedName("remarks")
        private String remarks;

        @Expose
        @SerializedName("call_detail")
        private List<CallDetail> callDetail;

        @Expose
        @SerializedName("status")
        private int status;

        @Expose
        @SerializedName("draft")
        private int draft;

        @SerializedName("CreatedAt")
        private String createdAt;

        @SerializedName("UpdatedAt")
        private String updatedAt;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getWorkAreaT() {
            return workAreaT;
        }

        public void setWorkAreaT(String workAreaT) {
            this.workAreaT = workAreaT;
        }

        public UserDetails getUserDetails() {
            return userDetails;
        }

        public void setUserDetails(UserDetails userDetails) {
            this.userDetails = userDetails;
        }

        public String getCallDate() {
            return callDate;
        }

        public void setCallDate(String callDate) {
            this.callDate = callDate;
        }

        public int getSessions() {
            return sessions;
        }

        public void setSessions(int sessions) {
            this.sessions = sessions;
        }

        public int getLocation() {
            return location;
        }

        public void setLocation(int location) {
            this.location = location;
        }

        public String getDoctorId() {
            return doctorId;
        }

        public void setDoctorId(String doctorId) {
            this.doctorId = doctorId;
        }

        public CallDoctorDetails getCallDoctorDetails() {
            return callDoctorDetails;
        }

        public void setCallDoctorDetails(CallDoctorDetails callDoctorDetails) {
            this.callDoctorDetails = callDoctorDetails;
        }

        public String getCallType() {
            return callType;
        }

        public void setCallType(String callType) {
            this.callType = callType;
        }

        public String getRemarks() {
            return remarks;
        }

        public void setRemarks(String remarks) {
            this.remarks = remarks;
        }

        public List<CallDetail> getCallDetail() {
            return callDetail;
        }

        public void setCallDetail(List<CallDetail> callDetail) {
            this.callDetail = callDetail;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public int getDraft() {
            return draft;
        }

        public void setDraft(int draft) {
            this.draft = draft;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }
    }

    public class UserDetails {
        @Expose
        @SerializedName("id")
        private int id;

        @SerializedName("work_area_t")
        private String workAreaT;

        @Expose
        @SerializedName("rm_code")
        private String rmCode;

        @Expose
        @SerializedName("zm_code")
        private String zmCode;

        @Expose
        @SerializedName("sm_code")
        private String smCode;

        @Expose
        @SerializedName("gm_code")
        private String gmCode;

        @Expose
        @SerializedName("rm_address")
        private String rmAddress;

        @Expose
        @SerializedName("zm_address")
        private String zmAddress;

        @Expose
        @SerializedName("sm_address")
        private String smAddress;

        @Expose
        @SerializedName("gm_address")
        private String gmAddress;

        @Expose
        @SerializedName("sap_user_code")
        private int sapUserCode;

        @Expose
        @SerializedName("sap_next_user_code")
        private int sapNextUserCode;

        @Expose
        @SerializedName("name")
        private String name;

        @Expose
        @SerializedName("address")
        private String address;

        @Expose
        @SerializedName("mobile_number")
        private String mobileNumber;

        @Expose
        @SerializedName("designation_id")
        private int designationId;

        @Expose
        @SerializedName("designation")
        private String designation;

        @Expose
        @SerializedName("group_name")
        private String groupName;

        @Expose
        @SerializedName("opt")
        private String opt;

        @Expose
        @SerializedName("token")
        private String token;

        @SerializedName("device_type")
        private String deviceType;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getWorkAreaT() {
            return workAreaT;
        }

        public void setWorkAreaT(String workAreaT) {
            this.workAreaT = workAreaT;
        }

        public String getRmCode() {
            return rmCode;
        }

        public void setRmCode(String rmCode) {
            this.rmCode = rmCode;
        }

        public String getZmCode() {
            return zmCode;
        }

        public void setZmCode(String zmCode) {
            this.zmCode = zmCode;
        }

        public String getSmCode() {
            return smCode;
        }

        public void setSmCode(String smCode) {
            this.smCode = smCode;
        }

        public String getGmCode() {
            return gmCode;
        }

        public void setGmCode(String gmCode) {
            this.gmCode = gmCode;
        }

        public String getRmAddress() {
            return rmAddress;
        }

        public void setRmAddress(String rmAddress) {
            this.rmAddress = rmAddress;
        }

        public String getZmAddress() {
            return zmAddress;
        }

        public void setZmAddress(String zmAddress) {
            this.zmAddress = zmAddress;
        }

        public String getSmAddress() {
            return smAddress;
        }

        public void setSmAddress(String smAddress) {
            this.smAddress = smAddress;
        }

        public String getGmAddress() {
            return gmAddress;
        }

        public void setGmAddress(String gmAddress) {
            this.gmAddress = gmAddress;
        }

        public int getSapUserCode() {
            return sapUserCode;
        }

        public void setSapUserCode(int sapUserCode) {
            this.sapUserCode = sapUserCode;
        }

        public int getSapNextUserCode() {
            return sapNextUserCode;
        }

        public void setSapNextUserCode(int sapNextUserCode) {
            this.sapNextUserCode = sapNextUserCode;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getMobileNumber() {
            return mobileNumber;
        }

        public void setMobileNumber(String mobileNumber) {
            this.mobileNumber = mobileNumber;
        }

        public int getDesignationId() {
            return designationId;
        }

        public void setDesignationId(int designationId) {
            this.designationId = designationId;
        }

        public String getDesignation() {
            return designation;
        }

        public void setDesignation(String designation) {
            this.designation = designation;
        }

        public String getGroupName() {
            return groupName;
        }

        public void setGroupName(String groupName) {
            this.groupName = groupName;
        }

        public String getOpt() {
            return opt;
        }

        public void setOpt(String opt) {
            this.opt = opt;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public String getDeviceType() {
            return deviceType;
        }

        public void setDeviceType(String deviceType) {
            this.deviceType = deviceType;
        }
    }

    public class CallDetail {
        @Expose
        @SerializedName("id")
        private int id;

        @Expose
        @SerializedName("call_id")
        private String callId;

        @Expose
        @SerializedName("product_id")
        private String productId;

        @Expose
        @SerializedName("product_details")
        private ProductDetails productDetails;

        @SerializedName("gadget_id")
        private String gadgetID;

        @SerializedName("gadget_details")
        private GadgetDetails gadgetDetails;

        @SerializedName("pad_id")
        private String padID;

        @SerializedName("pad_qty")
        private int padQty;

        @SerializedName("sad_id")
        private String sadID;

        @SerializedName("g_gadget_id")
        private String gGadgetID;

        @SerializedName("gd_qty")
        private int gdQty;

        @SerializedName("CreatedAt")
        private String createdAt;

        @SerializedName("UpdatedAt")
        private String updatedAt;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getCallId() {
            return callId;
        }

        public void setCallId(String callId) {
            this.callId = callId;
        }

        public String getProductId() {
            return productId;
        }

        public void setProductId(String productId) {
            this.productId = productId;
        }

        public ProductDetails getProductDetails() {
            return productDetails;
        }

        public void setProductDetails(ProductDetails productDetails) {
            this.productDetails = productDetails;
        }

        public String getGadgetID() {
            return gadgetID;
        }

        public void setGadgetID(String gadgetID) {
            this.gadgetID = gadgetID;
        }

        public GadgetDetails getGadgetDetails() {
            return gadgetDetails;
        }

        public void setGadgetDetails(GadgetDetails gadgetDetails) {
            this.gadgetDetails = gadgetDetails;
        }

        public String getPadID() {
            return padID;
        }

        public void setPadID(String padID) {
            this.padID = padID;
        }

        public int getPadQty() {
            return padQty;
        }

        public void setPadQty(int padQty) {
            this.padQty = padQty;
        }

        public String getSadID() {
            return sadID;
        }

        public void setSadID(String sadID) {
            this.sadID = sadID;
        }

        public String getgGadgetID() {
            return gGadgetID;
        }

        public void setgGadgetID(String gGadgetID) {
            this.gGadgetID = gGadgetID;
        }

        public int getGdQty() {
            return gdQty;
        }

        public void setGdQty(int gdQty) {
            this.gdQty = gdQty;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }
    }

    public class ProductDetails {
        @Expose
        @SerializedName("id")
        private int id;

        @Expose
        @SerializedName("matnr")
        private String matnr;

        @Expose
        @SerializedName("plant")
        private String plant;

        @Expose
        @SerializedName("sales_org")
        private String salesOrg;

        @Expose
        @SerializedName("dis_channel")
        private String disChannel;

        @Expose
        @SerializedName("material_name")
        private String materialName;

        @Expose
        @SerializedName("producer_company")
        private String producerCompany;

        @Expose
        @SerializedName("team1")
        private String team1;

        @Expose
        @SerializedName("unit_tp")
        private String unitTp;

        @Expose
        @SerializedName("unit_vat")
        private String unitVat;

        @Expose
        @SerializedName("mrp")
        private String mrp;

        @Expose
        @SerializedName("brand_name")
        private String brandName;

        @Expose
        @SerializedName("brand_description")
        private String brandDescription;

        @Expose
        @SerializedName("active")
        private String active;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getMatnr() {
            return matnr;
        }

        public void setMatnr(String matnr) {
            this.matnr = matnr;
        }

        public String getPlant() {
            return plant;
        }

        public void setPlant(String plant) {
            this.plant = plant;
        }

        public String getSalesOrg() {
            return salesOrg;
        }

        public void setSalesOrg(String salesOrg) {
            this.salesOrg = salesOrg;
        }

        public String getDisChannel() {
            return disChannel;
        }

        public void setDisChannel(String disChannel) {
            this.disChannel = disChannel;
        }

        public String getMaterialName() {
            return materialName;
        }

        public void setMaterialName(String materialName) {
            this.materialName = materialName;
        }

        public String getProducerCompany() {
            return producerCompany;
        }

        public void setProducerCompany(String producerCompany) {
            this.producerCompany = producerCompany;
        }

        public String getTeam1() {
            return team1;
        }

        public void setTeam1(String team1) {
            this.team1 = team1;
        }

        public String getUnitTp() {
            return unitTp;
        }

        public void setUnitTp(String unitTp) {
            this.unitTp = unitTp;
        }

        public String getUnitVat() {
            return unitVat;
        }

        public void setUnitVat(String unitVat) {
            this.unitVat = unitVat;
        }

        public String getMrp() {
            return mrp;
        }

        public void setMrp(String mrp) {
            this.mrp = mrp;
        }

        public String getBrandName() {
            return brandName;
        }

        public void setBrandName(String brandName) {
            this.brandName = brandName;
        }

        public String getBrandDescription() {
            return brandDescription;
        }

        public void setBrandDescription(String brandDescription) {
            this.brandDescription = brandDescription;
        }

        public String getActive() {
            return active;
        }

        public void setActive(String active) {
            this.active = active;
        }
    }

    public class GadgetDetails {
        @SerializedName("PICode")
        private String piCode;

        @SerializedName("PIName")
        private String piName;

        @SerializedName("CreatedAt")
        private String createdAt;

        @SerializedName("UpdatedAt")
        private String updatedAt;

        public String getPiCode() {
            return piCode;
        }

        public void setPiCode(String piCode) {
            this.piCode = piCode;
        }

        public String getPiName() {
            return piName;
        }

        public void setPiName(String piName) {
            this.piName = piName;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }
    }

    public class CallDoctorDetails {
        @Expose
        @SerializedName("dr_child_id")
        private String drChildId;

        @Expose
        @SerializedName("dr_master_id")
        private String drMasterId;

        @Expose
        @SerializedName("doctor_details")
        private DoctorDetails doctorDetails;

        @Expose
        @SerializedName("speciality_code")
        private String specialityCode;

        @Expose
        @SerializedName("ch_addr1")
        private String chAddr1;

        @Expose
        @SerializedName("ch_addr2")
        private String chAddr2;

        @Expose
        @SerializedName("ch_phone")
        private String chPhone;

        @Expose
        @SerializedName("ch_pabx")
        private String chPabx;

        @Expose
        @SerializedName("ch_email")
        private String chEmail;

        public String getDrChildId() {
            return drChildId;
        }

        public void setDrChildId(String drChildId) {
            this.drChildId = drChildId;
        }

        public String getDrMasterId() {
            return drMasterId;
        }

        public void setDrMasterId(String drMasterId) {
            this.drMasterId = drMasterId;
        }

        public DoctorDetails getDoctorDetails() {
            return doctorDetails;
        }

        public void setDoctorDetails(DoctorDetails doctorDetails) {
            this.doctorDetails = doctorDetails;
        }

        public String getSpecialityCode() {
            return specialityCode;
        }

        public void setSpecialityCode(String specialityCode) {
            this.specialityCode = specialityCode;
        }

        public String getChAddr1() {
            return chAddr1;
        }

        public void setChAddr1(String chAddr1) {
            this.chAddr1 = chAddr1;
        }

        public String getChAddr2() {
            return chAddr2;
        }

        public void setChAddr2(String chAddr2) {
            this.chAddr2 = chAddr2;
        }

        public String getChPhone() {
            return chPhone;
        }

        public void setChPhone(String chPhone) {
            this.chPhone = chPhone;
        }

        public String getChPabx() {
            return chPabx;
        }

        public void setChPabx(String chPabx) {
            this.chPabx = chPabx;
        }

        public String getChEmail() {
            return chEmail;
        }

        public void setChEmail(String chEmail) {
            this.chEmail = chEmail;
        }
    }

    public class DoctorDetails {
        @Expose
        @SerializedName("dr_master_id")
        private String drMasterId;

        @Expose
        @SerializedName("doctor_name1")
        private String doctorName1;

        @SerializedName("doctor_name2")
        private String doctorName2;

        @Expose
        @SerializedName("gender")
        private String gender;

        @SerializedName("cell_phone1")
        private String cellPhone1;

        @SerializedName("cell_phone2")
        private String cellPhone2;

        @SerializedName("email1")
        private String email1;

        @SerializedName("email2")
        private String email2;

        @SerializedName("education")
        private String education;

        @Expose
        @SerializedName("professional_degrees")
        private String professionalDegrees;

        @SerializedName("medical_college")
        private String medicalCollege;

        public String getDrMasterId() {
            return drMasterId;
        }

        public void setDrMasterId(String drMasterId) {
            this.drMasterId = drMasterId;
        }

        public String getDoctorName1() {
            return doctorName1;
        }

        public void setDoctorName1(String doctorName1) {
            this.doctorName1 = doctorName1;
        }

        public String getDoctorName2() {
            return doctorName2;
        }

        public void setDoctorName2(String doctorName2) {
            this.doctorName2 = doctorName2;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getCellPhone1() {
            return cellPhone1;
        }

        public void setCellPhone1(String cellPhone1) {
            this.cellPhone1 = cellPhone1;
        }

        public String getCellPhone2() {
            return cellPhone2;
        }

        public void setCellPhone2(String cellPhone2) {
            this.cellPhone2 = cellPhone2;
        }

        public String getEmail1() {
            return email1;
        }

        public void setEmail1(String email1) {
            this.email1 = email1;
        }

        public String getEmail2() {
            return email2;
        }

        public void setEmail2(String email2) {
            this.email2 = email2;
        }

        public String getEducation() {
            return education;
        }

        public void setEducation(String education) {
            this.education = education;
        }

        public String getProfessionalDegrees() {
            return professionalDegrees;
        }

        public void setProfessionalDegrees(String professionalDegrees) {
            this.professionalDegrees = professionalDegrees;
        }

        public String getMedicalCollege() {
            return medicalCollege;
        }

        public void setMedicalCollege(String medicalCollege) {
            this.medicalCollege = medicalCollege;
        }
    }

}
