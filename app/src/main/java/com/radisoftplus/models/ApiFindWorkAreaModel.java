package com.radisoftplus.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ApiFindWorkAreaModel {
    @SerializedName("error")
    @Expose
    private String error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("result")
    @Expose
    private List<Result> result = null;
    @SerializedName("success")
    @Expose
    private Boolean success;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Result> getResult() {
        return result;
    }

    public void setResult(List<Result> result) {
        this.result = result;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public class Result {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("work_area_t")
        @Expose
        private String workAreaT;
        @SerializedName("rm_code")
        @Expose
        private String rmCode;
        @SerializedName("zm_code")
        @Expose
        private String zmCode;
        @SerializedName("sm_code")
        @Expose
        private String smCode;
        @SerializedName("gm_code")
        @Expose
        private String gmCode;
        @SerializedName("rm_address")
        @Expose
        private String rmAddress;
        @SerializedName("zm_address")
        @Expose
        private String zmAddress;
        @SerializedName("sm_address")
        @Expose
        private String smAddress;
        @SerializedName("gm_address")
        @Expose
        private String gmAddress;
        @SerializedName("sap_user_code")
        @Expose
        private Integer sapUserCode;
        @SerializedName("sap_next_user_code")
        @Expose
        private Integer sapNextUserCode;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("address")
        @Expose
        private String address;
        @SerializedName("mobile_number")
        @Expose
        private String mobileNumber;
        @SerializedName("designation_id")
        @Expose
        private Integer designationId;
        @SerializedName("designation")
        @Expose
        private String designation;
        @SerializedName("group_name")
        @Expose
        private String groupName;
        @SerializedName("opt")
        @Expose
        private String opt;
        @SerializedName("token")
        @Expose
        private String token;
        @SerializedName("device_type")
        @Expose
        private String deviceType;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getWorkAreaT() {
            return workAreaT;
        }

        public void setWorkAreaT(String workAreaT) {
            this.workAreaT = workAreaT;
        }

        public String getRmCode() {
            return rmCode;
        }

        public void setRmCode(String rmCode) {
            this.rmCode = rmCode;
        }

        public String getZmCode() {
            return zmCode;
        }

        public void setZmCode(String zmCode) {
            this.zmCode = zmCode;
        }

        public String getSmCode() {
            return smCode;
        }

        public void setSmCode(String smCode) {
            this.smCode = smCode;
        }

        public String getGmCode() {
            return gmCode;
        }

        public void setGmCode(String gmCode) {
            this.gmCode = gmCode;
        }

        public String getRmAddress() {
            return rmAddress;
        }

        public void setRmAddress(String rmAddress) {
            this.rmAddress = rmAddress;
        }

        public String getZmAddress() {
            return zmAddress;
        }

        public void setZmAddress(String zmAddress) {
            this.zmAddress = zmAddress;
        }

        public String getSmAddress() {
            return smAddress;
        }

        public void setSmAddress(String smAddress) {
            this.smAddress = smAddress;
        }

        public String getGmAddress() {
            return gmAddress;
        }

        public void setGmAddress(String gmAddress) {
            this.gmAddress = gmAddress;
        }

        public Integer getSapUserCode() {
            return sapUserCode;
        }

        public void setSapUserCode(Integer sapUserCode) {
            this.sapUserCode = sapUserCode;
        }

        public Integer getSapNextUserCode() {
            return sapNextUserCode;
        }

        public void setSapNextUserCode(Integer sapNextUserCode) {
            this.sapNextUserCode = sapNextUserCode;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getMobileNumber() {
            return mobileNumber;
        }

        public void setMobileNumber(String mobileNumber) {
            this.mobileNumber = mobileNumber;
        }

        public Integer getDesignationId() {
            return designationId;
        }

        public void setDesignationId(Integer designationId) {
            this.designationId = designationId;
        }

        public String getDesignation() {
            return designation;
        }

        public void setDesignation(String designation) {
            this.designation = designation;
        }

        public String getGroupName() {
            return groupName;
        }

        public void setGroupName(String groupName) {
            this.groupName = groupName;
        }

        public String getOpt() {
            return opt;
        }

        public void setOpt(String opt) {
            this.opt = opt;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public String getDeviceType() {
            return deviceType;
        }

        public void setDeviceType(String deviceType) {
            this.deviceType = deviceType;
        }

        @Override
        public String toString() {
            return  workAreaT + " - "+name;
        }


    }
}
