package com.radisoftplus.models;

import java.util.List;

public class DraftOrderModel {
    String address;
    String work_area_t;
    String ref_work_area_t;
    String order_date;
    String order_type;
    String customer_id;
    String customer_name;
    String employee_id;
    String device_type;
    private List<OrderListModel> order_list = null;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getWork_area_t() {
        return work_area_t;
    }

    public void setWork_area_t(String work_area_t) {
        this.work_area_t = work_area_t;
    }

    public String getRef_work_area_t() {
        return ref_work_area_t;
    }

    public void setRef_work_area_t(String ref_work_area_t) {
        this.ref_work_area_t = ref_work_area_t;
    }

    public String getOrder_date() {
        return order_date;
    }

    public void setOrder_date(String order_date) {
        this.order_date = order_date;
    }

    public String getOrder_type() {
        return order_type;
    }

    public void setOrder_type(String order_type) {
        this.order_type = order_type;
    }

    public String getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(String customer_id) {
        this.customer_id = customer_id;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public String getEmployee_id() {
        return employee_id;
    }

    public void setEmployee_id(String employee_id) {
        this.employee_id = employee_id;
    }

    public String getDevice_type() {
        return device_type;
    }

    public void setDevice_type(String device_type) {
        this.device_type = device_type;
    }

    public List<OrderListModel> getOrder_list() {
        return order_list;
    }

    public void setOrder_list(List<OrderListModel> order_list) {
        this.order_list = order_list;
    }
}
