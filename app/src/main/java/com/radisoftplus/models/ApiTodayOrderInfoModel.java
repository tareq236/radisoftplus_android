package com.radisoftplus.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ApiTodayOrderInfoModel {
    @SerializedName("error")
    @Expose
    private String error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("result")
    @Expose
    private Result result;
    @SerializedName("success")
    @Expose
    private Boolean success;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public class Result {

        @SerializedName("total_order")
        @Expose
        private String totalOrder;
        @SerializedName("total_customer")
        @Expose
        private String totalCustomer;
        @SerializedName("total_tp")
        @Expose
        private String totalTp;
        @SerializedName("total_vat")
        @Expose
        private String totalVat;
        @SerializedName("total_mrp")
        @Expose
        private String totalMrp;
        @SerializedName("total_qty")
        @Expose
        private String totalQty;
        @SerializedName("total_cash")
        @Expose
        private String totalCash;
        @SerializedName("total_cash_amount")
        @Expose
        private String totalCashAmount;
        @SerializedName("total_credit")
        @Expose
        private String totalCredit;
        @SerializedName("total_credit_amount")
        @Expose
        private String totalCreditAmount;
        @SerializedName("total_shared")
        @Expose
        private String totalShared;
        @SerializedName("total_shared_amount")
        @Expose
        private String totalSharedAmount;

        public String getTotalOrder() {
            return totalOrder;
        }

        public void setTotalOrder(String totalOrder) {
            this.totalOrder = totalOrder;
        }

        public String getTotalCustomer() {
            return totalCustomer;
        }

        public void setTotalCustomer(String totalCustomer) {
            this.totalCustomer = totalCustomer;
        }

        public String getTotalTp() {
            return totalTp;
        }

        public void setTotalTp(String totalTp) {
            this.totalTp = totalTp;
        }

        public String getTotalVat() {
            return totalVat;
        }

        public void setTotalVat(String totalVat) {
            this.totalVat = totalVat;
        }

        public String getTotalMrp() {
            return totalMrp;
        }

        public void setTotalMrp(String totalMrp) {
            this.totalMrp = totalMrp;
        }

        public String getTotalQty() {
            return totalQty;
        }

        public void setTotalQty(String totalQty) {
            this.totalQty = totalQty;
        }

        public String getTotalCash() {
            return totalCash;
        }

        public void setTotalCash(String totalCash) {
            this.totalCash = totalCash;
        }

        public String getTotalCashAmount() {
            return totalCashAmount;
        }

        public void setTotalCashAmount(String totalCashAmount) {
            this.totalCashAmount = totalCashAmount;
        }

        public String getTotalCredit() {
            return totalCredit;
        }

        public void setTotalCredit(String totalCredit) {
            this.totalCredit = totalCredit;
        }

        public String getTotalCreditAmount() {
            return totalCreditAmount;
        }

        public void setTotalCreditAmount(String totalCreditAmount) {
            this.totalCreditAmount = totalCreditAmount;
        }

        public String getTotalShared() {
            return totalShared;
        }

        public void setTotalShared(String totalShared) {
            this.totalShared = totalShared;
        }

        public String getTotalSharedAmount() {
            return totalSharedAmount;
        }

        public void setTotalSharedAmount(String totalSharedAmount) {
            this.totalSharedAmount = totalSharedAmount;
        }

    }
}
