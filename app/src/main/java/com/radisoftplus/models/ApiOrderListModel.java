package com.radisoftplus.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ApiOrderListModel {
    @SerializedName("error")
    @Expose
    private String error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("result")
    @Expose
    private List<ApiOrderListModel.Result> result = null;
    @SerializedName("success")
    @Expose
    private Boolean success;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Result> getResult() {
        return result;
    }

    public void setResult(List<Result> result) {
        this.result = result;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public class Result {
        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("work_area_t")
        @Expose
        private String work_area_t;
        @SerializedName("ref_work_area_t")
        @Expose
        private String ref_work_area_t;
        @SerializedName("order_date")
        @Expose
        private String order_date;
        @SerializedName("customer_id")
        @Expose
        private String customer_id;
        @SerializedName("order_type")
        @Expose
        private String order_type;
        @SerializedName("employee_id")
        @Expose
        private String employee_id;
        @SerializedName("ref_user_details")
        @Expose
        private ApiOrderListModel.RefUserDetails ref_user_details = null;
        @SerializedName("customer_details")
        @Expose
        private ApiOrderListModel.CustomerDetails customer_details = null;
        @SerializedName("order_list")
        @Expose
        private List<ApiOrderListModel.OrderList> order_list = null;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getWork_area_t() {
            return work_area_t;
        }

        public void setWork_area_t(String work_area_t) {
            this.work_area_t = work_area_t;
        }

        public String getRef_work_area_t() {
            return ref_work_area_t;
        }

        public void setRef_work_area_t(String ref_work_area_t) {
            this.ref_work_area_t = ref_work_area_t;
        }

        public String getOrder_date() {
            return order_date;
        }

        public void setOrder_date(String order_date) {
            this.order_date = order_date;
        }

        public String getCustomer_id() {
            return customer_id;
        }

        public void setCustomer_id(String customer_id) {
            this.customer_id = customer_id;
        }

        public String getOrder_type() {
            return order_type;
        }

        public void setOrder_type(String order_type) {
            this.order_type = order_type;
        }

        public String getEmployee_id() {
            return employee_id;
        }

        public void setEmployee_id(String employee_id) {
            this.employee_id = employee_id;
        }

        public RefUserDetails getRef_user_details() {
            return ref_user_details;
        }

        public void setRef_user_details(RefUserDetails ref_user_details) {
            this.ref_user_details = ref_user_details;
        }

        public CustomerDetails getCustomer_details() {
            return customer_details;
        }

        public void setCustomer_details(CustomerDetails customer_details) {
            this.customer_details = customer_details;
        }

        public List<OrderList> getOrder_list() {
            return order_list;
        }

        public void setOrder_list(List<OrderList> order_list) {
            this.order_list = order_list;
        }
    }

    public class RefUserDetails {
        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("work_area_t")
        @Expose
        private String work_area_t;
        @SerializedName("rm_code")
        @Expose
        private String rm_code;
        @SerializedName("zm_code")
        @Expose
        private String zm_code;
        @SerializedName("sm_code")
        @Expose
        private String sm_code;
        @SerializedName("gm_code")
        @Expose
        private String gm_code;
        @SerializedName("rm_address")
        @Expose
        private String rm_address;
        @SerializedName("zm_address")
        @Expose
        private String zm_address;
        @SerializedName("sm_address")
        @Expose
        private String sm_address;
        @SerializedName("gm_address")
        @Expose
        private String gm_address;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("address")
        @Expose
        private String address;
        @SerializedName("designation_id")
        @Expose
        private String designation_id;
        @SerializedName("designation")
        @Expose
        private String designation;
        @SerializedName("group_name")
        @Expose
        private String group_name;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getWork_area_t() {
            return work_area_t;
        }

        public void setWork_area_t(String work_area_t) {
            this.work_area_t = work_area_t;
        }

        public String getRm_code() {
            return rm_code;
        }

        public void setRm_code(String rm_code) {
            this.rm_code = rm_code;
        }

        public String getZm_code() {
            return zm_code;
        }

        public void setZm_code(String zm_code) {
            this.zm_code = zm_code;
        }

        public String getSm_code() {
            return sm_code;
        }

        public void setSm_code(String sm_code) {
            this.sm_code = sm_code;
        }

        public String getGm_code() {
            return gm_code;
        }

        public void setGm_code(String gm_code) {
            this.gm_code = gm_code;
        }

        public String getRm_address() {
            return rm_address;
        }

        public void setRm_address(String rm_address) {
            this.rm_address = rm_address;
        }

        public String getZm_address() {
            return zm_address;
        }

        public void setZm_address(String zm_address) {
            this.zm_address = zm_address;
        }

        public String getSm_address() {
            return sm_address;
        }

        public void setSm_address(String sm_address) {
            this.sm_address = sm_address;
        }

        public String getGm_address() {
            return gm_address;
        }

        public void setGm_address(String gm_address) {
            this.gm_address = gm_address;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getDesignation_id() {
            return designation_id;
        }

        public void setDesignation_id(String designation_id) {
            this.designation_id = designation_id;
        }

        public String getDesignation() {
            return designation;
        }

        public void setDesignation(String designation) {
            this.designation = designation;
        }

        public String getGroup_name() {
            return group_name;
        }

        public void setGroup_name(String group_name) {
            this.group_name = group_name;
        }
    }

    public class CustomerDetails {
        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("partner")
        @Expose
        private String partner;
        @SerializedName("customer_sales_org")
        @Expose
        private String customer_sales_org;
        @SerializedName("name1")
        @Expose
        private String name1;
        @SerializedName("name2")
        @Expose
        private String name2;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getPartner() {
            return partner;
        }

        public void setPartner(String partner) {
            this.partner = partner;
        }

        public String getCustomer_sales_org() {
            return customer_sales_org;
        }

        public void setCustomer_sales_org(String customer_sales_org) {
            this.customer_sales_org = customer_sales_org;
        }

        public String getName1() {
            return name1;
        }

        public void setName1(String name1) {
            this.name1 = name1;
        }

        public String getName2() {
            return name2;
        }

        public void setName2(String name2) {
            this.name2 = name2;
        }
    }

    public class OrderList {
        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("order_id")
        @Expose
        private Integer order_id;
        @SerializedName("product_id")
        @Expose
        private String product_id;
        @SerializedName("unit_tp")
        @Expose
        private String unit_tp;
        @SerializedName("unit_vat")
        @Expose
        private String unit_vat;
        @SerializedName("mrp")
        @Expose
        private String mrp;
        @SerializedName("qty")
        @Expose
        private Integer qty;
        @SerializedName("product_details")
        @Expose
        private ApiOrderListModel.ProductDetails product_details = null;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getOrder_id() {
            return order_id;
        }

        public void setOrder_id(Integer order_id) {
            this.order_id = order_id;
        }

        public String getProduct_id() {
            return product_id;
        }

        public void setProduct_id(String product_id) {
            this.product_id = product_id;
        }

        public String getUnit_tp() {
            return unit_tp;
        }

        public void setUnit_tp(String unit_tp) {
            this.unit_tp = unit_tp;
        }

        public String getUnit_vat() {
            return unit_vat;
        }

        public void setUnit_vat(String unit_vat) {
            this.unit_vat = unit_vat;
        }

        public String getMrp() {
            return mrp;
        }

        public void setMrp(String mrp) {
            this.mrp = mrp;
        }

        public Integer getQty() {
            return qty;
        }

        public void setQty(Integer qty) {
            this.qty = qty;
        }

        public ProductDetails getProduct_details() {
            return product_details;
        }

        public void setProduct_details(ProductDetails product_details) {
            this.product_details = product_details;
        }
    }

    public class ProductDetails {
        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("matnr")
        @Expose
        private String matnr;
        @SerializedName("plant")
        @Expose
        private String plant;
        @SerializedName("sales_org")
        @Expose
        private String sales_org;
        @SerializedName("dis_channel")
        @Expose
        private String dis_channel;
        @SerializedName("material_name")
        @Expose
        private String material_name;
        @SerializedName("producer_company")
        @Expose
        private String producer_company;
        @SerializedName("team1")
        @Expose
        private String team1;
        @SerializedName("unit_tp")
        @Expose
        private String unit_tp;
        @SerializedName("unit_vat")
        @Expose
        private String unit_vat;
        @SerializedName("mrp")
        @Expose
        private String mrp;
        @SerializedName("brand_name")
        @Expose
        private String brand_name;
        @SerializedName("brand_description")
        @Expose
        private String brand_description;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getMatnr() {
            return matnr;
        }

        public void setMatnr(String matnr) {
            this.matnr = matnr;
        }

        public String getPlant() {
            return plant;
        }

        public void setPlant(String plant) {
            this.plant = plant;
        }

        public String getSales_org() {
            return sales_org;
        }

        public void setSales_org(String sales_org) {
            this.sales_org = sales_org;
        }

        public String getDis_channel() {
            return dis_channel;
        }

        public void setDis_channel(String dis_channel) {
            this.dis_channel = dis_channel;
        }

        public String getMaterial_name() {
            return material_name;
        }

        public void setMaterial_name(String material_name) {
            this.material_name = material_name;
        }

        public String getProducer_company() {
            return producer_company;
        }

        public void setProducer_company(String producer_company) {
            this.producer_company = producer_company;
        }

        public String getTeam1() {
            return team1;
        }

        public void setTeam1(String team1) {
            this.team1 = team1;
        }

        public String getUnit_tp() {
            return unit_tp;
        }

        public void setUnit_tp(String unit_tp) {
            this.unit_tp = unit_tp;
        }

        public String getUnit_vat() {
            return unit_vat;
        }

        public void setUnit_vat(String unit_vat) {
            this.unit_vat = unit_vat;
        }

        public String getMrp() {
            return mrp;
        }

        public void setMrp(String mrp) {
            this.mrp = mrp;
        }

        public String getBrand_name() {
            return brand_name;
        }

        public void setBrand_name(String brand_name) {
            this.brand_name = brand_name;
        }

        public String getBrand_description() {
            return brand_description;
        }

        public void setBrand_description(String brand_description) {
            this.brand_description = brand_description;
        }
    }
}
