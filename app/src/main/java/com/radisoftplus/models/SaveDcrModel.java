package com.radisoftplus.models;
import java.util.List;

public class SaveDcrModel {
    String work_area_t;
    String call_date;
    Integer sessions;
    Integer location;
    String doctor_id;
    String call_type;
    String remarks;
    Integer draft;

    public Integer getDraft() {
        return draft;
    }

    public void setDraft(Integer draft) {
        this.draft = draft;
    }

    private List<CallDetail> call_detail = null;

    public String getWork_area_t() {
        return work_area_t;
    }

    public void setWork_area_t(String work_area_t) {
        this.work_area_t = work_area_t;
    }

    public String getCall_date() {
        return call_date;
    }

    public void setCall_date(String call_date) {
        this.call_date = call_date;
    }

    public Integer getSessions() {
        return sessions;
    }

    public void setSessions(Integer sessions) {
        this.sessions = sessions;
    }

    public Integer getLocation() {
        return location;
    }

    public void setLocation(Integer location) {
        this.location = location;
    }

    public String getDoctor_id() {
        return doctor_id;
    }

    public void setDoctor_id(String doctor_id) {
        this.doctor_id = doctor_id;
    }

    public String getCall_type() {
        return call_type;
    }

    public void setCall_type(String call_type) {
        this.call_type = call_type;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }



    public List<CallDetail> getCall_detail() {
        return call_detail;
    }

    public void setCall_detail(List<CallDetail> call_detail) {
        this.call_detail = call_detail;
    }

    public static class CallDetail {
        String product_id;
        String gadget_id;
        Integer gd_qty;

        public String getProduct_id() {
            return product_id;
        }

        public void setProduct_id(String product_id) {
            this.product_id = product_id;
        }

        public String getGadget_id() {
            return gadget_id;
        }

        public void setGadget_id(String gadget_id) {
            this.gadget_id = gadget_id;
        }

        public Integer getGd_qty() {
            return gd_qty;
        }

        public void setGd_qty(Integer gd_qty) {
            this.gd_qty = gd_qty;
        }
    }
}

