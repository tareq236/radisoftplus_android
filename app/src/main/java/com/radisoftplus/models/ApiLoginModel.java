package com.radisoftplus.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ApiLoginModel {
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("error")
    @Expose
    private String error;
    @SerializedName("result")
    @Expose
    private Result result;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public class Result {
        @SerializedName("work_area_t")
        @Expose
        private String workAreaT;

        @SerializedName("sap_user_code")
        @Expose
        private int sapUserCode;

        @SerializedName("sap_next_user_code")
        @Expose
        private int sapNextUserCode;

        @SerializedName("name")
        @Expose
        private String name;

        @SerializedName("address")
        @Expose
        private String address;

        @SerializedName("mobile_number")
        @Expose
        private String mobileNumber;

        @SerializedName("designation_id")
        @Expose
        private int designationId;

        @SerializedName("designation")
        @Expose
        private String designation;

        @SerializedName("group_name")
        @Expose
        private String groupName;

        @SerializedName("password")
        @Expose
        private String password;

        @SerializedName("opt")
        @Expose
        private String opt;

        @SerializedName("token")
        @Expose
        private String token;

        @SerializedName("device_type")
        @Expose
        private String deviceType;

        @SerializedName("rm_code")
        @Expose
        private String rmCode;

        @SerializedName("zm_code")
        @Expose
        private String zmCode;

        @SerializedName("sm_code")
        @Expose
        private String smCode;

        @SerializedName("gm_code")
        @Expose
        private String gmCode;

        @SerializedName("rm_address")
        @Expose
        private String rmAddress;

        @SerializedName("zm_address")
        @Expose
        private String zmAddress;

        @SerializedName("sm_address")
        @Expose
        private String smAddress;

        @SerializedName("gm_address")
        @Expose
        private String gmAddress;

        @SerializedName("password_change")
        @Expose
        private int passwordChange;

        public String getWorkAreaT() {
            return workAreaT;
        }

        public void setWorkAreaT(String workAreaT) {
            this.workAreaT = workAreaT;
        }

        public int getSapUserCode() {
            return sapUserCode;
        }

        public void setSapUserCode(int sapUserCode) {
            this.sapUserCode = sapUserCode;
        }

        public int getSapNextUserCode() {
            return sapNextUserCode;
        }

        public void setSapNextUserCode(int sapNextUserCode) {
            this.sapNextUserCode = sapNextUserCode;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getMobileNumber() {
            return mobileNumber;
        }

        public void setMobileNumber(String mobileNumber) {
            this.mobileNumber = mobileNumber;
        }

        public int getDesignationId() {
            return designationId;
        }

        public void setDesignationId(int designationId) {
            this.designationId = designationId;
        }

        public String getDesignation() {
            return designation;
        }

        public void setDesignation(String designation) {
            this.designation = designation;
        }

        public String getGroupName() {
            return groupName;
        }

        public void setGroupName(String groupName) {
            this.groupName = groupName;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getOpt() {
            return opt;
        }

        public void setOpt(String opt) {
            this.opt = opt;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public String getDeviceType() {
            return deviceType;
        }

        public void setDeviceType(String deviceType) {
            this.deviceType = deviceType;
        }

        public String getRmCode() {
            return rmCode;
        }

        public void setRmCode(String rmCode) {
            this.rmCode = rmCode;
        }

        public String getZmCode() {
            return zmCode;
        }

        public void setZmCode(String zmCode) {
            this.zmCode = zmCode;
        }

        public String getSmCode() {
            return smCode;
        }

        public void setSmCode(String smCode) {
            this.smCode = smCode;
        }

        public String getGmCode() {
            return gmCode;
        }

        public void setGmCode(String gmCode) {
            this.gmCode = gmCode;
        }

        public String getRmAddress() {
            return rmAddress;
        }

        public void setRmAddress(String rmAddress) {
            this.rmAddress = rmAddress;
        }

        public String getZmAddress() {
            return zmAddress;
        }

        public void setZmAddress(String zmAddress) {
            this.zmAddress = zmAddress;
        }

        public String getSmAddress() {
            return smAddress;
        }

        public void setSmAddress(String smAddress) {
            this.smAddress = smAddress;
        }

        public String getGmAddress() {
            return gmAddress;
        }

        public void setGmAddress(String gmAddress) {
            this.gmAddress = gmAddress;
        }

        public int getPasswordChange() {
            return passwordChange;
        }

        public void setPasswordChange(int passwordChange) {
            this.passwordChange = passwordChange;
        }
    }

}
