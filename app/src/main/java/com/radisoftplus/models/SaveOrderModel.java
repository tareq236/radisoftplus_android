package com.radisoftplus.models;

import java.util.List;

public class SaveOrderModel {
    String work_area_t;
    String ref_work_area_t;
    String order_date;
    String order_type;
    String customer_id;
    String employee_id;
    String device_type;
    private List<OrderList> order_list = null;

    public String getWork_area_t() {
        return work_area_t;
    }

    public void setWork_area_t(String work_area_t) {
        this.work_area_t = work_area_t;
    }

    public String getRef_work_area_t() {
        return ref_work_area_t;
    }

    public void setRef_work_area_t(String ref_work_area_t) {
        this.ref_work_area_t = ref_work_area_t;
    }

    public String getOrder_date() {
        return order_date;
    }

    public void setOrder_date(String order_date) {
        this.order_date = order_date;
    }

    public String getOrder_type() {
        return order_type;
    }

    public void setOrder_type(String order_type) {
        this.order_type = order_type;
    }

    public String getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(String customer_id) {
        this.customer_id = customer_id;
    }

    public String getEmployee_id() {
        return employee_id;
    }

    public void setEmployee_id(String employee_id) {
        this.employee_id = employee_id;
    }

    public String getDevice_type() {
        return device_type;
    }

    public void setDevice_type(String device_type) {
        this.device_type = device_type;
    }

    public List<OrderList> getOrder_list() {
        return order_list;
    }

    public void setOrder_list(List<OrderList> order_list) {
        this.order_list = order_list;
    }

    public static class OrderList {
        String del_plant;
        String product_id;
        String unit_tp;
        String unit_vat;
        String mrp;
        Integer qty;

        public String getDel_plant() {
            return del_plant;
        }

        public void setDel_plant(String del_plant) {
            this.del_plant = del_plant;
        }

        public String getProduct_id() {
            return product_id;
        }

        public void setProduct_id(String product_id) {
            this.product_id = product_id;
        }

        public String getUnit_tp() {
            return unit_tp;
        }

        public void setUnit_tp(String unit_tp) {
            this.unit_tp = unit_tp;
        }

        public String getUnit_vat() {
            return unit_vat;
        }

        public void setUnit_vat(String unit_vat) {
            this.unit_vat = unit_vat;
        }

        public String getMrp() {
            return mrp;
        }

        public void setMrp(String mrp) {
            this.mrp = mrp;
        }

        public Integer getQty() {
            return qty;
        }

        public void setQty(Integer qty) {
            this.qty = qty;
        }
    }
}
