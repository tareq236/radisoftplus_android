package com.radisoftplus.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ApiFindCustomerModel {
    @SerializedName("error")
    @Expose
    private String error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("result")
    @Expose
    private List<Result> result = null;
    @SerializedName("success")
    @Expose
    private Boolean success;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Result> getResult() {
        return result;
    }

    public void setResult(List<Result> result) {
        this.result = result;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public class Result {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("partner")
        @Expose
        private String partner;
        @SerializedName("customer")
        @Expose
        private List<Customer> customer = null;
        @SerializedName("team")
        @Expose
        private String team;
        @SerializedName("work_area")
        @Expose
        private String workArea;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getPartner() {
            return partner;
        }

        public void setPartner(String partner) {
            this.partner = partner;
        }

        public List<Customer> getCustomer() {
            return customer;
        }

        public void setCustomer(List<Customer> customer) {
            this.customer = customer;
        }

        public String getTeam() {
            return team;
        }

        public void setTeam(String team) {
            this.team = team;
        }

        public String getWorkArea() {
            return workArea;
        }

        public void setWorkArea(String workArea) {
            this.workArea = workArea;
        }



    }

    public class Customer {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("partner")
        @Expose
        private String partner;
        @SerializedName("customer_sales_org")
        @Expose
        private List<Object> customerSalesOrg = null;
        @SerializedName("name1")
        @Expose
        private String name1;
        @SerializedName("name2")
        @Expose
        private String name2;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getPartner() {
            return partner;
        }

        public void setPartner(String partner) {
            this.partner = partner;
        }

        public List<Object> getCustomerSalesOrg() {
            return customerSalesOrg;
        }

        public void setCustomerSalesOrg(List<Object> customerSalesOrg) {
            this.customerSalesOrg = customerSalesOrg;
        }

        public String getName1() {
            return name1;
        }

        public void setName1(String name1) {
            this.name1 = name1;
        }

        public String getName2() {
            return name2;
        }

        public void setName2(String name2) {
            this.name2 = name2;
        }

        @Override
        public String toString() {
            return  name1;
        }

    }

}
