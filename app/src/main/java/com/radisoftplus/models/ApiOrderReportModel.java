package com.radisoftplus.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ApiOrderReportModel {
    @SerializedName("error")
    @Expose
    private String error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("result")
    @Expose
    private List<Result> result = null;
    @SerializedName("success")
    @Expose
    private Boolean success;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Result> getResult() {
        return result;
    }

    public void setResult(List<Result> result) {
        this.result = result;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public class CustomerDetails {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("partner")
        @Expose
        private String partner;
        @SerializedName("customer_sales_org")
        @Expose
        private Object customerSalesOrg;
        @SerializedName("name1")
        @Expose
        private String name1;
        @SerializedName("name2")
        @Expose
        private String name2;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getPartner() {
            return partner;
        }

        public void setPartner(String partner) {
            this.partner = partner;
        }

        public Object getCustomerSalesOrg() {
            return customerSalesOrg;
        }

        public void setCustomerSalesOrg(Object customerSalesOrg) {
            this.customerSalesOrg = customerSalesOrg;
        }

        public String getName1() {
            return name1;
        }

        public void setName1(String name1) {
            this.name1 = name1;
        }

        public String getName2() {
            return name2;
        }

        public void setName2(String name2) {
            this.name2 = name2;
        }

    }

    public class Order {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("order_id")
        @Expose
        private Integer orderId;
        @SerializedName("product_id")
        @Expose
        private String productId;
        @SerializedName("product_details")
        @Expose
        private ProductDetails productDetails;
        @SerializedName("unit_tp")
        @Expose
        private String unitTp;
        @SerializedName("unit_vat")
        @Expose
        private String unitVat;
        @SerializedName("mrp")
        @Expose
        private String mrp;
        @SerializedName("qty")
        @Expose
        private Integer qty;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getOrderId() {
            return orderId;
        }

        public void setOrderId(Integer orderId) {
            this.orderId = orderId;
        }

        public String getProductId() {
            return productId;
        }

        public void setProductId(String productId) {
            this.productId = productId;
        }

        public ProductDetails getProductDetails() {
            return productDetails;
        }

        public void setProductDetails(ProductDetails productDetails) {
            this.productDetails = productDetails;
        }

        public String getUnitTp() {
            return unitTp;
        }

        public void setUnitTp(String unitTp) {
            this.unitTp = unitTp;
        }

        public String getUnitVat() {
            return unitVat;
        }

        public void setUnitVat(String unitVat) {
            this.unitVat = unitVat;
        }

        public String getMrp() {
            return mrp;
        }

        public void setMrp(String mrp) {
            this.mrp = mrp;
        }

        public Integer getQty() {
            return qty;
        }

        public void setQty(Integer qty) {
            this.qty = qty;
        }

    }

    public class ProductDetails {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("matnr")
        @Expose
        private String matnr;
        @SerializedName("plant")
        @Expose
        private String plant;
        @SerializedName("sales_org")
        @Expose
        private String salesOrg;
        @SerializedName("dis_channel")
        @Expose
        private String disChannel;
        @SerializedName("material_name")
        @Expose
        private String materialName;
        @SerializedName("producer_company")
        @Expose
        private String producerCompany;
        @SerializedName("team1")
        @Expose
        private String team1;
        @SerializedName("unit_tp")
        @Expose
        private String unitTp;
        @SerializedName("unit_vat")
        @Expose
        private String unitVat;
        @SerializedName("mrp")
        @Expose
        private String mrp;
        @SerializedName("brand_name")
        @Expose
        private String brandName;
        @SerializedName("brand_description")
        @Expose
        private String brandDescription;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getMatnr() {
            return matnr;
        }

        public void setMatnr(String matnr) {
            this.matnr = matnr;
        }

        public String getPlant() {
            return plant;
        }

        public void setPlant(String plant) {
            this.plant = plant;
        }

        public String getSalesOrg() {
            return salesOrg;
        }

        public void setSalesOrg(String salesOrg) {
            this.salesOrg = salesOrg;
        }

        public String getDisChannel() {
            return disChannel;
        }

        public void setDisChannel(String disChannel) {
            this.disChannel = disChannel;
        }

        public String getMaterialName() {
            return materialName;
        }

        public void setMaterialName(String materialName) {
            this.materialName = materialName;
        }

        public String getProducerCompany() {
            return producerCompany;
        }

        public void setProducerCompany(String producerCompany) {
            this.producerCompany = producerCompany;
        }

        public String getTeam1() {
            return team1;
        }

        public void setTeam1(String team1) {
            this.team1 = team1;
        }

        public String getUnitTp() {
            return unitTp;
        }

        public void setUnitTp(String unitTp) {
            this.unitTp = unitTp;
        }

        public String getUnitVat() {
            return unitVat;
        }

        public void setUnitVat(String unitVat) {
            this.unitVat = unitVat;
        }

        public String getMrp() {
            return mrp;
        }

        public void setMrp(String mrp) {
            this.mrp = mrp;
        }

        public String getBrandName() {
            return brandName;
        }

        public void setBrandName(String brandName) {
            this.brandName = brandName;
        }

        public String getBrandDescription() {
            return brandDescription;
        }

        public void setBrandDescription(String brandDescription) {
            this.brandDescription = brandDescription;
        }

    }

    public class RefUserDetails {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("work_area_t")
        @Expose
        private String workAreaT;
        @SerializedName("rm_code")
        @Expose
        private String rmCode;
        @SerializedName("zm_code")
        @Expose
        private String zmCode;
        @SerializedName("sm_code")
        @Expose
        private String smCode;
        @SerializedName("gm_code")
        @Expose
        private String gmCode;
        @SerializedName("rm_address")
        @Expose
        private String rmAddress;
        @SerializedName("zm_address")
        @Expose
        private String zmAddress;
        @SerializedName("sm_address")
        @Expose
        private String smAddress;
        @SerializedName("gm_address")
        @Expose
        private String gmAddress;
        @SerializedName("sap_user_code")
        @Expose
        private Integer sapUserCode;
        @SerializedName("sap_next_user_code")
        @Expose
        private Integer sapNextUserCode;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("address")
        @Expose
        private String address;
        @SerializedName("mobile_number")
        @Expose
        private String mobileNumber;
        @SerializedName("designation_id")
        @Expose
        private Integer designationId;
        @SerializedName("designation")
        @Expose
        private String designation;
        @SerializedName("group_name")
        @Expose
        private String groupName;
        @SerializedName("opt")
        @Expose
        private String opt;
        @SerializedName("token")
        @Expose
        private String token;
        @SerializedName("device_type")
        @Expose
        private String deviceType;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getWorkAreaT() {
            return workAreaT;
        }

        public void setWorkAreaT(String workAreaT) {
            this.workAreaT = workAreaT;
        }

        public String getRmCode() {
            return rmCode;
        }

        public void setRmCode(String rmCode) {
            this.rmCode = rmCode;
        }

        public String getZmCode() {
            return zmCode;
        }

        public void setZmCode(String zmCode) {
            this.zmCode = zmCode;
        }

        public String getSmCode() {
            return smCode;
        }

        public void setSmCode(String smCode) {
            this.smCode = smCode;
        }

        public String getGmCode() {
            return gmCode;
        }

        public void setGmCode(String gmCode) {
            this.gmCode = gmCode;
        }

        public String getRmAddress() {
            return rmAddress;
        }

        public void setRmAddress(String rmAddress) {
            this.rmAddress = rmAddress;
        }

        public String getZmAddress() {
            return zmAddress;
        }

        public void setZmAddress(String zmAddress) {
            this.zmAddress = zmAddress;
        }

        public String getSmAddress() {
            return smAddress;
        }

        public void setSmAddress(String smAddress) {
            this.smAddress = smAddress;
        }

        public String getGmAddress() {
            return gmAddress;
        }

        public void setGmAddress(String gmAddress) {
            this.gmAddress = gmAddress;
        }

        public Integer getSapUserCode() {
            return sapUserCode;
        }

        public void setSapUserCode(Integer sapUserCode) {
            this.sapUserCode = sapUserCode;
        }

        public Integer getSapNextUserCode() {
            return sapNextUserCode;
        }

        public void setSapNextUserCode(Integer sapNextUserCode) {
            this.sapNextUserCode = sapNextUserCode;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getMobileNumber() {
            return mobileNumber;
        }

        public void setMobileNumber(String mobileNumber) {
            this.mobileNumber = mobileNumber;
        }

        public Integer getDesignationId() {
            return designationId;
        }

        public void setDesignationId(Integer designationId) {
            this.designationId = designationId;
        }

        public String getDesignation() {
            return designation;
        }

        public void setDesignation(String designation) {
            this.designation = designation;
        }

        public String getGroupName() {
            return groupName;
        }

        public void setGroupName(String groupName) {
            this.groupName = groupName;
        }

        public String getOpt() {
            return opt;
        }

        public void setOpt(String opt) {
            this.opt = opt;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public String getDeviceType() {
            return deviceType;
        }

        public void setDeviceType(String deviceType) {
            this.deviceType = deviceType;
        }

    }

    public class Result {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("work_area_t")
        @Expose
        private String workAreaT;
        @SerializedName("user_details")
        @Expose
        private UserDetails userDetails;
        @SerializedName("ref_work_area_t")
        @Expose
        private String refWorkAreaT;
        @SerializedName("ref_user_details")
        @Expose
        private RefUserDetails refUserDetails;
        @SerializedName("order_date")
        @Expose
        private String orderDate;
        @SerializedName("customer_id")
        @Expose
        private String customerId;
        @SerializedName("customer_details")
        @Expose
        private CustomerDetails customerDetails;
        @SerializedName("order_type")
        @Expose
        private String orderType;
        @SerializedName("employee_id")
        @Expose
        private String employeeId;
        @SerializedName("order_list")
        @Expose
        private List<Order> orderList = null;
        @SerializedName("device_type")
        @Expose
        private String deviceType;
        @SerializedName("status")
        @Expose
        private Integer status;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getWorkAreaT() {
            return workAreaT;
        }

        public void setWorkAreaT(String workAreaT) {
            this.workAreaT = workAreaT;
        }

        public UserDetails getUserDetails() {
            return userDetails;
        }

        public void setUserDetails(UserDetails userDetails) {
            this.userDetails = userDetails;
        }

        public String getRefWorkAreaT() {
            return refWorkAreaT;
        }

        public void setRefWorkAreaT(String refWorkAreaT) {
            this.refWorkAreaT = refWorkAreaT;
        }

        public RefUserDetails getRefUserDetails() {
            return refUserDetails;
        }

        public void setRefUserDetails(RefUserDetails refUserDetails) {
            this.refUserDetails = refUserDetails;
        }

        public String getOrderDate() {
            return orderDate;
        }

        public void setOrderDate(String orderDate) {
            this.orderDate = orderDate;
        }

        public String getCustomerId() {
            return customerId;
        }

        public void setCustomerId(String customerId) {
            this.customerId = customerId;
        }

        public CustomerDetails getCustomerDetails() {
            return customerDetails;
        }

        public void setCustomerDetails(CustomerDetails customerDetails) {
            this.customerDetails = customerDetails;
        }

        public String getOrderType() {
            return orderType;
        }

        public void setOrderType(String orderType) {
            this.orderType = orderType;
        }

        public String getEmployeeId() {
            return employeeId;
        }

        public void setEmployeeId(String employeeId) {
            this.employeeId = employeeId;
        }

        public List<Order> getOrderList() {
            return orderList;
        }

        public void setOrderList(List<Order> orderList) {
            this.orderList = orderList;
        }

        public String getDeviceType() {
            return deviceType;
        }

        public void setDeviceType(String deviceType) {
            this.deviceType = deviceType;
        }

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }

    }

    public class UserDetails {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("work_area_t")
        @Expose
        private String workAreaT;
        @SerializedName("rm_code")
        @Expose
        private String rmCode;
        @SerializedName("zm_code")
        @Expose
        private String zmCode;
        @SerializedName("sm_code")
        @Expose
        private String smCode;
        @SerializedName("gm_code")
        @Expose
        private String gmCode;
        @SerializedName("rm_address")
        @Expose
        private String rmAddress;
        @SerializedName("zm_address")
        @Expose
        private String zmAddress;
        @SerializedName("sm_address")
        @Expose
        private String smAddress;
        @SerializedName("gm_address")
        @Expose
        private String gmAddress;
        @SerializedName("sap_user_code")
        @Expose
        private Integer sapUserCode;
        @SerializedName("sap_next_user_code")
        @Expose
        private Integer sapNextUserCode;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("address")
        @Expose
        private String address;
        @SerializedName("mobile_number")
        @Expose
        private String mobileNumber;
        @SerializedName("designation_id")
        @Expose
        private Integer designationId;
        @SerializedName("designation")
        @Expose
        private String designation;
        @SerializedName("group_name")
        @Expose
        private String groupName;
        @SerializedName("opt")
        @Expose
        private String opt;
        @SerializedName("token")
        @Expose
        private String token;
        @SerializedName("device_type")
        @Expose
        private String deviceType;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getWorkAreaT() {
            return workAreaT;
        }

        public void setWorkAreaT(String workAreaT) {
            this.workAreaT = workAreaT;
        }

        public String getRmCode() {
            return rmCode;
        }

        public void setRmCode(String rmCode) {
            this.rmCode = rmCode;
        }

        public String getZmCode() {
            return zmCode;
        }

        public void setZmCode(String zmCode) {
            this.zmCode = zmCode;
        }

        public String getSmCode() {
            return smCode;
        }

        public void setSmCode(String smCode) {
            this.smCode = smCode;
        }

        public String getGmCode() {
            return gmCode;
        }

        public void setGmCode(String gmCode) {
            this.gmCode = gmCode;
        }

        public String getRmAddress() {
            return rmAddress;
        }

        public void setRmAddress(String rmAddress) {
            this.rmAddress = rmAddress;
        }

        public String getZmAddress() {
            return zmAddress;
        }

        public void setZmAddress(String zmAddress) {
            this.zmAddress = zmAddress;
        }

        public String getSmAddress() {
            return smAddress;
        }

        public void setSmAddress(String smAddress) {
            this.smAddress = smAddress;
        }

        public String getGmAddress() {
            return gmAddress;
        }

        public void setGmAddress(String gmAddress) {
            this.gmAddress = gmAddress;
        }

        public Integer getSapUserCode() {
            return sapUserCode;
        }

        public void setSapUserCode(Integer sapUserCode) {
            this.sapUserCode = sapUserCode;
        }

        public Integer getSapNextUserCode() {
            return sapNextUserCode;
        }

        public void setSapNextUserCode(Integer sapNextUserCode) {
            this.sapNextUserCode = sapNextUserCode;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getMobileNumber() {
            return mobileNumber;
        }

        public void setMobileNumber(String mobileNumber) {
            this.mobileNumber = mobileNumber;
        }

        public Integer getDesignationId() {
            return designationId;
        }

        public void setDesignationId(Integer designationId) {
            this.designationId = designationId;
        }

        public String getDesignation() {
            return designation;
        }

        public void setDesignation(String designation) {
            this.designation = designation;
        }

        public String getGroupName() {
            return groupName;
        }

        public void setGroupName(String groupName) {
            this.groupName = groupName;
        }

        public String getOpt() {
            return opt;
        }

        public void setOpt(String opt) {
            this.opt = opt;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public String getDeviceType() {
            return deviceType;
        }

        public void setDeviceType(String deviceType) {
            this.deviceType = deviceType;
        }

    }
}
