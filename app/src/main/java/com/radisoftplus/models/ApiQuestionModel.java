package com.radisoftplus.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ApiQuestionModel {
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("error")
    @Expose
    private String error;
    @SerializedName("results")
    @Expose
    private List<Results> results;
    @SerializedName("ans")
    @Expose
    private String ans;

    public String getAns() {
        return ans;
    }

    public void setAns(String ans) {
        this.ans = ans;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public List<Results> getResults() {
        return results;
    }

    public void setResults(List<Results> results) {
        this.results = results;
    }

    public class Results {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("text_question")
        @Expose
        private String textQuestion;
        @SerializedName("options")
        @Expose
        private List<Option> options;
        @SerializedName("answear")
        @Expose
        private String answear;
        @SerializedName("is_ans_alrady")
        @Expose
        private Integer isAnsAlrady;
        @SerializedName("a_option_1")
        @Expose
        private Integer a_option_1;
        @SerializedName("a_option_2")
        @Expose
        private Integer a_option_2;
        @SerializedName("a_option_3")
        @Expose
        private Integer a_option_3;
        @SerializedName("a_option_4")
        @Expose
        private Integer a_option_4;
        @SerializedName("a_option_5")
        @Expose
        private Integer a_option_5;

        public Integer getA_option_1() {
            return a_option_1;
        }

        public void setA_option_1(Integer a_option_1) {
            this.a_option_1 = a_option_1;
        }

        public Integer getA_option_2() {
            return a_option_2;
        }

        public void setA_option_2(Integer a_option_2) {
            this.a_option_2 = a_option_2;
        }

        public Integer getA_option_3() {
            return a_option_3;
        }

        public void setA_option_3(Integer a_option_3) {
            this.a_option_3 = a_option_3;
        }

        public Integer getA_option_4() {
            return a_option_4;
        }

        public void setA_option_4(Integer a_option_4) {
            this.a_option_4 = a_option_4;
        }

        public Integer getA_option_5() {
            return a_option_5;
        }

        public void setA_option_5(Integer a_option_5) {
            this.a_option_5 = a_option_5;
        }

        public String getAnswear() {
            return answear;
        }

        public void setAnswear(String answear) {
            this.answear = answear;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getTextQuestion() {
            return textQuestion;
        }

        public void setTextQuestion(String textQuestion) {
            this.textQuestion = textQuestion;
        }

        public List<Option> getOptions() {
            return options;
        }

        public void setOptions(List<Option> options) {
            this.options = options;
        }

        public Integer getIsAnsAlrady() {
            return isAnsAlrady;
        }

        public void setIsAnsAlrady(Integer isAnsAlrady) {
            this.isAnsAlrady = isAnsAlrady;
        }
    }


    public class Option {

        @SerializedName("option")
        @Expose
        private String option;

        public String getOption() {
            return option;
        }

        public void setOption(String option) {
            this.option = option;
        }
    }
}
