package com.radisoftplus.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SalesOrgModel {
    @SerializedName("sales_org")
    @Expose
    private String sales_org;
    @SerializedName("del_plant")
    @Expose
    private String delPlant;

    public String getSales_org() {
        return sales_org;
    }

    public void setSales_org(String sales_org) {
        this.sales_org = sales_org;
    }

    public String getDelPlant() {
        return delPlant;
    }

    public void setDelPlant(String delPlant) {
        this.delPlant = delPlant;
    }
}
