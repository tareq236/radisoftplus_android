package com.radisoftplus.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AnswareModel {
    @SerializedName("text_question")
    @Expose
    private String  text_question;
    @SerializedName("options")
    @Expose
    private String options;
    @SerializedName("answear")
    @Expose
    private String answear;
    @SerializedName("is_correct")
    @Expose
    private String is_correct;

    public String getText_question() {
        return text_question;
    }

    public void setText_question(String text_question) {
        this.text_question = text_question;
    }

    public String getOptions() {
        return options;
    }

    public void setOptions(String options) {
        this.options = options;
    }

    public String getAnswear() {
        return answear;
    }

    public void setAnswear(String answear) {
        this.answear = answear;
    }

    public String getIs_correct() {
        return is_correct;
    }

    public void setIs_correct(String is_correct) {
        this.is_correct = is_correct;
    }
}
