package com.radisoftplus.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ApiGadgetUtilizationModel {

    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("error")
    @Expose
    private String error;
    @SerializedName("result")
    @Expose
    private List<Result> result;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public List<Result> getResult() {
        return result;
    }

    public void setResult(List<Result> result) {
        this.result = result;
    }

    public class Result {
        @SerializedName("BRAND")
        @Expose
        private String brand;

        @SerializedName("GADGET")
        @Expose
        private String gadget;

        @SerializedName("ALLOCATE")
        @Expose
        private String allocate;

        @SerializedName("USES")
        @Expose
        private String uses;

        @SerializedName("STOCK")
        @Expose
        private String stock;

        public String getBrand() {
            return brand;
        }

        public void setBrand(String brand) {
            this.brand = brand;
        }

        public String getGadget() {
            return gadget;
        }

        public void setGadget(String gadget) {
            this.gadget = gadget;
        }

        public String getAllocate() {
            return allocate;
        }

        public void setAllocate(String allocate) {
            this.allocate = allocate;
        }

        public String getUses() {
            return uses;
        }

        public void setUses(String uses) {
            this.uses = uses;
        }

        public String getStock() {
            return stock;
        }

        public void setStock(String stock) {
            this.stock = stock;
        }
    }



}
