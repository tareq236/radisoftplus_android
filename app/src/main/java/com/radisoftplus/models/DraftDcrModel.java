package com.radisoftplus.models;

import java.util.List;

public class DraftDcrModel {
    Integer id;
    String work_area_t;
    String call_date;
    String sessions;
    String chamber_type;
    String doctor_name;
    String dr_child_id;
    String call_type;
    String remarks;
    String json_details;
    String created_at;

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getWork_area_t() {
        return work_area_t;
    }

    public void setWork_area_t(String work_area_t) {
        this.work_area_t = work_area_t;
    }

    public String getCall_date() {
        return call_date;
    }

    public void setCall_date(String call_date) {
        this.call_date = call_date;
    }

    public String getSessions() {
        return sessions;
    }

    public void setSessions(String sessions) {
        this.sessions = sessions;
    }

    public String getChamber_type() {
        return chamber_type;
    }

    public void setChamber_type(String chamber_type) {
        this.chamber_type = chamber_type;
    }

    public String getDoctor_name() {
        return doctor_name;
    }

    public void setDoctor_name(String doctor_name) {
        this.doctor_name = doctor_name;
    }

    public String getDr_child_id() {
        return dr_child_id;
    }

    public void setDr_child_id(String dr_child_id) {
        this.dr_child_id = dr_child_id;
    }

    public String getCall_type() {
        return call_type;
    }

    public void setCall_type(String call_type) {
        this.call_type = call_type;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getJson_details() {
        return json_details;
    }

    public void setJson_details(String json_details) {
        this.json_details = json_details;
    }
}
