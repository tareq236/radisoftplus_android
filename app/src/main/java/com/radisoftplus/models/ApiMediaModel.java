package com.radisoftplus.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ApiMediaModel {
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("error")
    @Expose
    private String error;
    @SerializedName("results")
    @Expose
    private List<Results> results;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public List<Results> getResults() {
        return results;
    }

    public void setResults(List<Results> results) {
        this.results = results;
    }

    public class Results {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("file_type")
        @Expose
        private String file_type;
        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("thumbnail")
        @Expose
        private String thumbnail;
        @SerializedName("item_type")
        @Expose
        private String item_type;
        @SerializedName("is_new")
        @Expose
        private String is_new;
        @SerializedName("media_list")
        @Expose
        private List<MediaList> media_list;

        public String getIs_new() {
            return is_new;
        }

        public void setIs_new(String is_new) {
            this.is_new = is_new;
        }

        public String getItem_type() {
            return item_type;
        }

        public void setItem_type(String item_type) {
            this.item_type = item_type;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getFile_type() {
            return file_type;
        }

        public void setFile_type(String file_type) {
            this.file_type = file_type;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getThumbnail() {
            return thumbnail;
        }

        public void setThumbnail(String thumbnail) {
            this.thumbnail = thumbnail;
        }

        public List<MediaList> getMedia_list() {
            return media_list;
        }

        public void setMedia_list(List<MediaList> media_list) {
            this.media_list = media_list;
        }
    }

    public class MediaList {
        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("s_title")
        @Expose
        private String s_title;
        @SerializedName("file_path")
        @Expose
        private String file_path;
        @SerializedName("file_thumbnail")
        @Expose
        private String file_thumbnail;

        public String getS_title() {
            return s_title;
        }

        public void setS_title(String s_title) {
            this.s_title = s_title;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getFile_path() {
            return file_path;
        }

        public void setFile_path(String file_path) {
            this.file_path = file_path;
        }

        public String getFile_thumbnail() {
            return file_thumbnail;
        }

        public void setFile_thumbnail(String file_thumbnail) {
            this.file_thumbnail = file_thumbnail;
        }
    }
}
