package com.radisoftplus.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ApiFindMaterialModel {
    @SerializedName("error")
    @Expose
    private String error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("result")
    @Expose
    private List<Result> result = null;
    @SerializedName("success")
    @Expose
    private Boolean success;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Result> getResult() {
        return result;
    }

    public void setResult(List<Result> result) {
        this.result = result;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public class Result {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("matnr")
        @Expose
        private String matnr;
        @SerializedName("plant")
        @Expose
        private String plant;
        @SerializedName("sales_org")
        @Expose
        private String salesOrg;
        @SerializedName("dis_channel")
        @Expose
        private String disChannel;
        @SerializedName("material_name")
        @Expose
        private String materialName;
        @SerializedName("producer_company")
        @Expose
        private String producerCompany;
        @SerializedName("team1")
        @Expose
        private String team1;
        @SerializedName("unit_tp")
        @Expose
        private String unitTp;
        @SerializedName("unit_vat")
        @Expose
        private String unitVat;
        @SerializedName("mrp")
        @Expose
        private String mrp;
        @SerializedName("brand_name")
        @Expose
        private String brandName;
        @SerializedName("brand_description")
        @Expose
        private String brandDescription;
        @SerializedName("PMName")
        @Expose
        private String PMName;
        @SerializedName("PMCode")
        @Expose
        private String PMCode;

        public String getPMName() {
            return PMName;
        }

        public void setPMName(String PMName) {
            this.PMName = PMName;
        }

        public String getPMCode() {
            return PMCode;
        }

        public void setPMCode(String PMCode) {
            this.PMCode = PMCode;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getMatnr() {
            return matnr;
        }

        public void setMatnr(String matnr) {
            this.matnr = matnr;
        }

        public String getPlant() {
            return plant;
        }

        public void setPlant(String plant) {
            this.plant = plant;
        }

        public String getSalesOrg() {
            return salesOrg;
        }

        public void setSalesOrg(String salesOrg) {
            this.salesOrg = salesOrg;
        }

        public String getDisChannel() {
            return disChannel;
        }

        public void setDisChannel(String disChannel) {
            this.disChannel = disChannel;
        }

        public String getMaterialName() {
            return materialName;
        }

        public void setMaterialName(String materialName) {
            this.materialName = materialName;
        }

        public String getProducerCompany() {
            return producerCompany;
        }

        public void setProducerCompany(String producerCompany) {
            this.producerCompany = producerCompany;
        }

        public String getTeam1() {
            return team1;
        }

        public void setTeam1(String team1) {
            this.team1 = team1;
        }

        public String getUnitTp() {
            return unitTp;
        }

        public void setUnitTp(String unitTp) {
            this.unitTp = unitTp;
        }

        public String getUnitVat() {
            return unitVat;
        }

        public void setUnitVat(String unitVat) {
            this.unitVat = unitVat;
        }

        public String getMrp() {
            return mrp;
        }

        public void setMrp(String mrp) {
            this.mrp = mrp;
        }

        public String getBrandName() {
            return brandName;
        }

        public void setBrandName(String brandName) {
            this.brandName = brandName;
        }

        public String getBrandDescription() {
            return brandDescription;
        }

        public void setBrandDescription(String brandDescription) {
            this.brandDescription = brandDescription;
        }

        @Override
        public String toString() {
            return  materialName;
        }

        @Override
        public boolean equals(Object obj) {
            if(obj instanceof ApiFindMaterialModel){
                ApiFindMaterialModel.Result c = (ApiFindMaterialModel.Result) obj;
                if(c.getMaterialName().equals(materialName)
                        && c.getMrp()==mrp && c.getUnitTp()==unitTp
                        && c.getUnitVat()==unitVat &&  c.getId()==id
                )
                    return true;
            }

            return false;
        }
    }
}
