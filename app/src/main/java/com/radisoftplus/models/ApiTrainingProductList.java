package com.radisoftplus.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ApiTrainingProductList {
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("error")
    @Expose
    private String error;
    @SerializedName("results")
    @Expose
    private List<Results> results;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public List<Results> getResults() {
        return results;
    }

    public void setResults(List<Results> results) {
        this.results = results;
    }

    public static class Results {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("training_product_name")
        @Expose
        private String trainingProductName;
        @SerializedName("patients_number")
        @Expose
        private String patients_number;
        @SerializedName("rx_number")
        @Expose
        private String rx_number;
        @SerializedName("is_new")
        @Expose
        private String is_new;


        public String getIs_new() {
            return is_new;
        }

        public void setIs_new(String is_new) {
            this.is_new = is_new;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getTrainingProductName() {
            return trainingProductName;
        }

        public void setTrainingProductName(String trainingProductName) {
            this.trainingProductName = trainingProductName;
        }

        public String getPatients_number() {
            return patients_number;
        }

        public void setPatients_number(String patients_number) {
            this.patients_number = patients_number;
        }

        public String getRx_number() {
            return rx_number;
        }

        public void setRx_number(String rx_number) {
            this.rx_number = rx_number;
        }
    }
}
