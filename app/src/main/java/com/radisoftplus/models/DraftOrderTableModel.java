package com.radisoftplus.models;

import java.util.List;

public class DraftOrderTableModel {
    Integer id;
    String order_date;
    String work_area;
    String address;
    String chemist_name;
    String chemist_id;
    String payment_mode;
    String json_details;
    String created_at;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOrder_date() {
        return order_date;
    }

    public void setOrder_date(String order_date) {
        this.order_date = order_date;
    }

    public String getWork_area() {
        return work_area;
    }

    public void setWork_area(String work_area) {
        this.work_area = work_area;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getChemist_name() {
        return chemist_name;
    }

    public void setChemist_name(String chemist_name) {
        this.chemist_name = chemist_name;
    }

    public String getChemist_id() {
        return chemist_id;
    }

    public void setChemist_id(String chemist_id) {
        this.chemist_id = chemist_id;
    }

    public String getPayment_mode() {
        return payment_mode;
    }

    public void setPayment_mode(String payment_mode) {
        this.payment_mode = payment_mode;
    }

    public String getJson_details() {
        return json_details;
    }

    public void setJson_details(String json_details) {
        this.json_details = json_details;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }
}
