package com.radisoftplus.models;

public class OrderListModel {
    String del_plant;
    String matnr;
    String material_name;
    String mrp;
    String unit_tp;
    String unit_vat;
    int qty ;

    public String getDel_plant() {
        return del_plant;
    }

    public void setDel_plant(String del_plant) {
        this.del_plant = del_plant;
    }

    public String getMrp() {
        return mrp;
    }

    public void setMrp(String mrp) {
        this.mrp = mrp;
    }

    public String getMatnr() {
        return matnr;
    }

    public void setMatnr(String matnr) {
        this.matnr = matnr;
    }

    public String getMaterial_name() {
        return material_name;
    }

    public void setMaterial_name(String material_name) {
        this.material_name = material_name;
    }

    public String getUnit_tp() {
        return unit_tp;
    }

    public void setUnit_tp(String unit_tp) {
        this.unit_tp = unit_tp;
    }

    public String getUnit_vat() {
        return unit_vat;
    }

    public void setUnit_vat(String unit_vat) {
        this.unit_vat = unit_vat;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }
}
