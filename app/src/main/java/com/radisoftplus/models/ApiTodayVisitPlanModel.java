package com.radisoftplus.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ApiTodayVisitPlanModel {
    @SerializedName("error")
    @Expose
    private String error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("result")
    @Expose
    private List<Result> result = null;
    @SerializedName("success")
    @Expose
    private Boolean success;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Result> getResult() {
        return result;
    }

    public void setResult(List<Result> result) {
        this.result = result;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public class Result {
        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("dr_child_id")
        @Expose
        private String drChildId;
        @SerializedName("work_area_t")
        @Expose
        private String workAreaT;
        @SerializedName("plan_date")
        @Expose
        private String planDate;
        @SerializedName("day_num")
        @Expose
        private Integer dayNum;
        @SerializedName("day_text")
        @Expose
        private String dayText;
        @SerializedName("chamber_details")
        @Expose
        private ChamberDetails chamberDetails;
        @SerializedName("monthly_visit_plan_list")
        @Expose
        private List<MonthlyVisitPlanList> monthlyVisitPlanList=null;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getDrChildId() {
            return drChildId;
        }

        public void setDrChildId(String drChildId) {
            this.drChildId = drChildId;
        }

        public String getWorkAreaT() {
            return workAreaT;
        }

        public void setWorkAreaT(String workAreaT) {
            this.workAreaT = workAreaT;
        }

        public String getPlanDate() {
            return planDate;
        }

        public void setPlanDate(String planDate) {
            this.planDate = planDate;
        }

        public Integer getDayNum() {
            return dayNum;
        }

        public void setDayNum(Integer dayNum) {
            this.dayNum = dayNum;
        }

        public String getDayText() {
            return dayText;
        }

        public void setDayText(String dayText) {
            this.dayText = dayText;
        }

        public ChamberDetails getChamberDetails() {
            return chamberDetails;
        }

        public void setChamberDetails(ChamberDetails chamberDetails) {
            this.chamberDetails = chamberDetails;
        }

        public List<MonthlyVisitPlanList> getMonthlyVisitPlanList() {
            return monthlyVisitPlanList;
        }

        public void setMonthlyVisitPlanList(List<MonthlyVisitPlanList> monthlyVisitPlanList) {
            this.monthlyVisitPlanList = monthlyVisitPlanList;
        }
    }

    public class MonthlyVisitPlanList {
        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("monthly_visit_plan_id")
        @Expose
        private Integer monthlyVisitPlanId;
        @SerializedName("brand_id")
        @Expose
        private Integer brandId;
        @SerializedName("brand_name")
        @Expose
        private String brandName;
        @SerializedName("brand_code")
        @Expose
        private String brandCode;
        @SerializedName("gadget_id")
        @Expose
        private Integer gadgetId;
        @SerializedName("gadget_name")
        @Expose
        private String gadgetName;
        @SerializedName("gadget_code")
        @Expose
        private String gadgetCode;
        @SerializedName("gadget_period")
        @Expose
        private String gadgetPeriod;
        @SerializedName("gadget_qty")
        @Expose
        private Integer gadgetQty;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getMonthlyVisitPlanId() {
            return monthlyVisitPlanId;
        }

        public void setMonthlyVisitPlanId(Integer monthlyVisitPlanId) {
            this.monthlyVisitPlanId = monthlyVisitPlanId;
        }

        public Integer getBrandId() {
            return brandId;
        }

        public void setBrandId(Integer brandId) {
            this.brandId = brandId;
        }

        public String getBrandName() {
            return brandName;
        }

        public void setBrandName(String brandName) {
            this.brandName = brandName;
        }

        public String getBrandCode() {
            return brandCode;
        }

        public void setBrandCode(String brandCode) {
            this.brandCode = brandCode;
        }

        public Integer getGadgetId() {
            return gadgetId;
        }

        public void setGadgetId(Integer gadgetId) {
            this.gadgetId = gadgetId;
        }

        public String getGadgetName() {
            return gadgetName;
        }

        public void setGadgetName(String gadgetName) {
            this.gadgetName = gadgetName;
        }

        public String getGadgetCode() {
            return gadgetCode;
        }

        public void setGadgetCode(String gadgetCode) {
            this.gadgetCode = gadgetCode;
        }

        public String getGadgetPeriod() {
            return gadgetPeriod;
        }

        public void setGadgetPeriod(String gadgetPeriod) {
            this.gadgetPeriod = gadgetPeriod;
        }

        public Integer getGadgetQty() {
            return gadgetQty;
        }

        public void setGadgetQty(Integer gadgetQty) {
            this.gadgetQty = gadgetQty;
        }
    }

    public class ChamberDetails {
        @SerializedName("dr_child_id")
        @Expose
        private String drChildId;
        @SerializedName("dr_master_id")
        @Expose
        private String drMasterId;
        @SerializedName("doctor_details")
        @Expose
        private DoctorDetails doctorDetails;
        @SerializedName("speciality_code")
        @Expose
        private String specialityCode;
        @SerializedName("speciality_details")
        @Expose
        private SpecialityDetails specialityDetails;
        @SerializedName("ch_addr1")
        @Expose
        private String chAddr1;
        @SerializedName("ch_addr2")
        @Expose
        private String chAddr2;
        @SerializedName("ch_phone")
        @Expose
        private String chPhone;
        @SerializedName("ch_pabx")
        @Expose
        private String chPabx;
        @SerializedName("ch_email")
        @Expose
        private String chEmail;

        public String getDrChildId() {
            return drChildId;
        }

        public void setDrChildId(String drChildId) {
            this.drChildId = drChildId;
        }

        public String getDrMasterId() {
            return drMasterId;
        }

        public void setDrMasterId(String drMasterId) {
            this.drMasterId = drMasterId;
        }

        public DoctorDetails getDoctorDetails() {
            return doctorDetails;
        }

        public void setDoctorDetails(DoctorDetails doctorDetails) {
            this.doctorDetails = doctorDetails;
        }

        public String getSpecialityCode() {
            return specialityCode;
        }

        public void setSpecialityCode(String specialityCode) {
            this.specialityCode = specialityCode;
        }

        public SpecialityDetails getSpecialityDetails() {
            return specialityDetails;
        }

        public void setSpecialityDetails(SpecialityDetails specialityDetails) {
            this.specialityDetails = specialityDetails;
        }

        public String getChAddr1() {
            return chAddr1;
        }

        public void setChAddr1(String chAddr1) {
            this.chAddr1 = chAddr1;
        }

        public String getChAddr2() {
            return chAddr2;
        }

        public void setChAddr2(String chAddr2) {
            this.chAddr2 = chAddr2;
        }

        public String getChPhone() {
            return chPhone;
        }

        public void setChPhone(String chPhone) {
            this.chPhone = chPhone;
        }

        public String getChPabx() {
            return chPabx;
        }

        public void setChPabx(String chPabx) {
            this.chPabx = chPabx;
        }

        public String getChEmail() {
            return chEmail;
        }

        public void setChEmail(String chEmail) {
            this.chEmail = chEmail;
        }
    }

    public class DoctorDetails {
        @SerializedName("dr_master_id")
        @Expose
        private String drMasterId;
        @SerializedName("doctor_name1")
        @Expose
        private String doctorName1;
        @SerializedName("doctor_name2")
        @Expose
        private String doctorName2;
        @SerializedName("gender")
        @Expose
        private String gender;
        @SerializedName("cell_phone1")
        @Expose
        private String cellPhone1;
        @SerializedName("cell_phone2")
        @Expose
        private String cellPhone2;
        @SerializedName("email1")
        @Expose
        private String email1;
        @SerializedName("email2")
        @Expose
        private String email2;
        @SerializedName("education")
        @Expose
        private String education;
        @SerializedName("professional_degrees")
        @Expose
        private String professionalDegrees;
        @SerializedName("medical_college")
        @Expose
        private String medicalCollege;

        public String getDrMasterId() {
            return drMasterId;
        }

        public void setDrMasterId(String drMasterId) {
            this.drMasterId = drMasterId;
        }

        public String getDoctorName1() {
            return doctorName1;
        }

        public void setDoctorName1(String doctorName1) {
            this.doctorName1 = doctorName1;
        }

        public String getDoctorName2() {
            return doctorName2;
        }

        public void setDoctorName2(String doctorName2) {
            this.doctorName2 = doctorName2;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getCellPhone1() {
            return cellPhone1;
        }

        public void setCellPhone1(String cellPhone1) {
            this.cellPhone1 = cellPhone1;
        }

        public String getCellPhone2() {
            return cellPhone2;
        }

        public void setCellPhone2(String cellPhone2) {
            this.cellPhone2 = cellPhone2;
        }

        public String getEmail1() {
            return email1;
        }

        public void setEmail1(String email1) {
            this.email1 = email1;
        }

        public String getEmail2() {
            return email2;
        }

        public void setEmail2(String email2) {
            this.email2 = email2;
        }

        public String getEducation() {
            return education;
        }

        public void setEducation(String education) {
            this.education = education;
        }

        public String getProfessionalDegrees() {
            return professionalDegrees;
        }

        public void setProfessionalDegrees(String professionalDegrees) {
            this.professionalDegrees = professionalDegrees;
        }

        public String getMedicalCollege() {
            return medicalCollege;
        }

        public void setMedicalCollege(String medicalCollege) {
            this.medicalCollege = medicalCollege;
        }
    }

    public class SpecialityDetails {
        @SerializedName("speciality_description")
        @Expose
        private String specialityDescription;

        public String getSpecialityDescription() {
            return specialityDescription;
        }

        public void setSpecialityDescription(String specialityDescription) {
            this.specialityDescription = specialityDescription;
        }
    }

}
