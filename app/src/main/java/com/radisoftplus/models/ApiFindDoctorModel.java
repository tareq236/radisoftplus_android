package com.radisoftplus.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ApiFindDoctorModel {
    @SerializedName("error")
    @Expose
    private String error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("result")
    @Expose
    private List<Result> result = null;
    @SerializedName("success")
    @Expose
    private Boolean success;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Result> getResult() {
        return result;
    }

    public void setResult(List<Result> result) {
        this.result = result;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public class Result {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("dr_child_id")
        @Expose
        private String dr_child_id;
        @SerializedName("dr_master_id")
        @Expose
        private String dr_master_id;
        @SerializedName("doctor_name1")
        @Expose
        private String doctor_name1;
        @SerializedName("doctor_name2")
        @Expose
        private String doctor_name2;
        @SerializedName("ch_addr1")
        @Expose
        private String ch_addr1;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getDr_child_id() {
            return dr_child_id;
        }

        public void setDr_child_id(String dr_child_id) {
            this.dr_child_id = dr_child_id;
        }

        public String getDr_master_id() {
            return dr_master_id;
        }

        public void setDr_master_id(String dr_master_id) {
            this.dr_master_id = dr_master_id;
        }

        public String getDoctor_name1() {
            return doctor_name1;
        }

        public void setDoctor_name1(String doctor_name1) {
            this.doctor_name1 = doctor_name1;
        }

        public String getDoctor_name2() {
            return doctor_name2;
        }

        public void setDoctor_name2(String doctor_name2) {
            this.doctor_name2 = doctor_name2;
        }

        public String getCh_addr1() {
            return ch_addr1;
        }

        public void setCh_addr1(String ch_addr1) {
            this.ch_addr1 = ch_addr1;
        }
    }


}
