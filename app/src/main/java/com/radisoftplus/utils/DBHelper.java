package com.radisoftplus.utils;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "radisoft.db";
    private static final int DATABASE_VERSION = 1;

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // Create your database tables here
        String createOrderTableQuery = "CREATE TABLE draft_order (id INTEGER PRIMARY KEY, order_date TEXT, work_area TEXT, address TEXT, chemist_name TEXT,chemist_id TEXT, payment_mode TEXT, json_details TEXT, created_at DATETIME DEFAULT CURRENT_TIMESTAMP)";
        String createDcrTableQuery = "CREATE TABLE draft_dcr (id INTEGER PRIMARY KEY, work_area TEXT, call_date TEXT, sessions TEXT, chamber_type TEXT, doctor_name TEXT, dr_child_id TEXT, call_type TEXT, remarks TEXT, json_details TEXT, created_at DATETIME DEFAULT CURRENT_TIMESTAMP)";
        try {
            db.execSQL(createOrderTableQuery);
            db.execSQL(createDcrTableQuery);
        } catch (SQLException e) {
            // Handle any exceptions that may occur during the execution of the SQL statements
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Handle database upgrades here
    }
}

