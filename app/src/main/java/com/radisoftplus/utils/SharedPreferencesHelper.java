package com.radisoftplus.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public final class SharedPreferencesHelper {

	private static final String PREFS_FILE_NAME = "radiant_puzzal_game";
	private static final String logindata = "logindata";
	private static final String USERNAME = "username";
	private static final String PASSWORD = "password";
	private static final String WORKAREAID = "work_area_id";
	private static final String DELIVERYDATE = "delivery_date";
	private static final String CHEMISTNAME = "chemist_name";
	private static final String PAYMENTMODE = "payment_mode";
	private static final String EMPLOYEEID = "employee_id";

	public static void setWorkAreaId(final Context ctx, final String data) {
		final SharedPreferences prefs = ctx.getSharedPreferences(
				PREFS_FILE_NAME, Context.MODE_PRIVATE);
		final Editor editor = prefs.edit();
		editor.putString(WORKAREAID, data);
		editor.commit();
	}

	public static void setDeliveryDate(final Context ctx, final String data) {
		final SharedPreferences prefs = ctx.getSharedPreferences(
				PREFS_FILE_NAME, Context.MODE_PRIVATE);
		final Editor editor = prefs.edit();
		editor.putString(DELIVERYDATE, data);
		editor.commit();
	}

	public static void setChemistName(final Context ctx, final String data) {
		final SharedPreferences prefs = ctx.getSharedPreferences(
				PREFS_FILE_NAME, Context.MODE_PRIVATE);
		final Editor editor = prefs.edit();
		editor.putString(CHEMISTNAME, data);
		editor.commit();
	}
	public static void setPaymentMode(final Context ctx, final String data) {
		final SharedPreferences prefs = ctx.getSharedPreferences(
				PREFS_FILE_NAME, Context.MODE_PRIVATE);
		final Editor editor = prefs.edit();
		editor.putString(PAYMENTMODE, data);
		editor.commit();
	}
	public static void setEmployeeId(final Context ctx, final String data) {
		final SharedPreferences prefs = ctx.getSharedPreferences(
				PREFS_FILE_NAME, Context.MODE_PRIVATE);
		final Editor editor = prefs.edit();
		editor.putString(EMPLOYEEID, data);
		editor.commit();
	}
	public static String getEmpCode(final Context ctx) {
		return ctx.getSharedPreferences(PREFS_FILE_NAME, Context.MODE_PRIVATE)
				.getString(USERNAME, "");
	}

	public static void setEmpCode(final Context ctx, final String data) {
		final SharedPreferences prefs = ctx.getSharedPreferences(
				PREFS_FILE_NAME, Context.MODE_PRIVATE);
		final Editor editor = prefs.edit();
		editor.putString(USERNAME, data);
		editor.commit();
	}

	public static String getPassword(final Context ctx) {
		return ctx.getSharedPreferences(PREFS_FILE_NAME, Context.MODE_PRIVATE)
				.getString(PASSWORD, "");
	}

	public static void setPassword(final Context ctx, final String data) {
		final SharedPreferences prefs = ctx.getSharedPreferences(
				PREFS_FILE_NAME, Context.MODE_PRIVATE);
		final Editor editor = prefs.edit();
		editor.putString(PASSWORD, data);
		editor.commit();
	}

	public static String GetLoginData(final Context ctx) {
		return ctx.getSharedPreferences(
				SharedPreferencesHelper.PREFS_FILE_NAME, Context.MODE_PRIVATE)
				.getString(SharedPreferencesHelper.logindata, "");
	}

	public static void SetLoginData(final Context ctx, final String logindata) {
		final SharedPreferences prefs = ctx.getSharedPreferences(
				SharedPreferencesHelper.PREFS_FILE_NAME, Context.MODE_PRIVATE);
		final Editor editor = prefs.edit();
		editor.putString(SharedPreferencesHelper.logindata, logindata);
		editor.commit();
	}

	public static void logOut(Context c) {

		final SharedPreferences prefs = c.getSharedPreferences(PREFS_FILE_NAME,
				Context.MODE_PRIVATE);
		prefs.edit().putBoolean("LOGIN", false).commit();

	}

	public static void setLogin(Context c) {

		final SharedPreferences prefs = c.getSharedPreferences(PREFS_FILE_NAME,
				Context.MODE_PRIVATE);
		prefs.edit().putBoolean("LOGIN", true).commit();
	}

	public static boolean isLogged(Context c) {

		final SharedPreferences prefs = c.getSharedPreferences(PREFS_FILE_NAME,
				Context.MODE_PRIVATE);

		return prefs.getBoolean("LOGIN", false);
	}
}
