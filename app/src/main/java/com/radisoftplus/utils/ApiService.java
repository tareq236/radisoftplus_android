package com.radisoftplus.utils;

import com.google.gson.JsonObject;
import com.radisoftplus.models.ApiDcrListModel;
import com.radisoftplus.models.ApiGadgetUtilizationModel;
import com.radisoftplus.models.SaveDcrModel;
import com.radisoftplus.models.ApiCheckBrandCommonModel;
import com.radisoftplus.models.ApiFindDoctorModel;
import com.radisoftplus.models.ApiFindMaterialModel;
import com.radisoftplus.models.ApiFindCustomerModel;
import com.radisoftplus.models.ApiFindWorkAreaModel;
import com.radisoftplus.models.ApiLoginModel;
import com.radisoftplus.models.ApiMediaModel;
import com.radisoftplus.models.ApiMonthList;
import com.radisoftplus.models.ApiOrderListModel;
import com.radisoftplus.models.ApiOrderReportModel;
import com.radisoftplus.models.ApiQuestionModel;
import com.radisoftplus.models.ApiQuestionResultModel;
import com.radisoftplus.models.ApiQuizsModel;
import com.radisoftplus.models.ApiSaveCustomerLocationModel;
import com.radisoftplus.models.ApiSaveDcrModel;
import com.radisoftplus.models.ApiSaveOrderModel;
import com.radisoftplus.models.ApiTodayOrderInfoModel;
import com.radisoftplus.models.ApiTodayVisitPlanModel;
import com.radisoftplus.models.ApiTrainingProductList;
import com.radisoftplus.models.SaveOrderModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface ApiService {

    @Headers({
            "Accept: application/json",
            "Content-Type: application/json"
    })
    @POST("v1/login")
    Call<ApiLoginModel> API_LOGIN_CALL(@Body JsonObject body);

    @Headers({
            "Accept: application/json",
            "Content-Type: application/json"
    })
    @POST("v1/change_first_password")
    Call<ApiLoginModel> API_CHANGE_FIRST_PASSWORD_CALL(@Body JsonObject body);

    @Headers({
            "Accept: application/json",
            "Content-Type: application/json"
    })
    @POST("v1/check_mobile")
    Call<ApiLoginModel> API_CHECK_MOBILE_CALL(@Body JsonObject body);

    @Headers({
            "Accept: application/json",
            "Content-Type: application/json"
    })
    @POST("v1/save_opt")
    Call<ApiLoginModel> API_SAVE_LOGIN_OTP_CALL(@Body JsonObject body);

    @Headers({
            "Accept: application/json",
            "Content-Type: application/json"
    })
    @POST("v1/login_with_mobile")
    Call<ApiLoginModel> API_LOGIN_MODEL_CALL(@Body JsonObject body);

    @Headers({
            "Accept: application/json",
            "Content-Type: application/json"
    })
    @POST("v1/doctor_list_for_today_visit_plan")
    Call<ApiTodayVisitPlanModel> API_TODAY_VISIT_PLAN_MODEL_CALL(@Body JsonObject body);

    @GET("v1/daily_order/today_order_info_by_mio/{id}")
    Call<ApiTodayOrderInfoModel> API_TODAY_ORDER_INFO_MODEL_CALL(
            @Path("id") String workArea
    );


    @Headers({
            "Accept: application/json",
            "Content-Type: application/json"
    })
    @POST("v1/daily_order/find_work_area")
    Call<ApiFindWorkAreaModel> API_FIND_WORK_AREA_MODEL_CALL(@Body JsonObject body);

    @Headers({
            "Accept: application/json",
            "Content-Type: application/json"
    })
    @POST("v1/daily_order/find_customer")
    Call<ApiFindCustomerModel> API_FIND_CUSTOMER_MODEL_CALL(@Body JsonObject body);

    @Headers({
            "Accept: application/json",
            "Content-Type: application/json"
    })
    @POST("v1/doctor_call/find_doctor")
    Call<ApiFindDoctorModel> API_FIND_DOCTOR_MODEL_CALL(@Body JsonObject body);

    @POST("v1/daily_order/find_material")
    Call<ApiFindMaterialModel> API_FIND_MATERIAL_MODEL_CALL(@Body JsonObject body);

    @POST("v1/daily_order/find_brand")
    Call<ApiFindMaterialModel> API_FIND_BRAND_MODEL_CALL(@Body JsonObject body);

    @POST("v1/doctor_call/find_pma")
    Call<ApiFindMaterialModel> API_FIND_PMA_MODEL_CALL(@Body JsonObject body);

    @Headers({
            "Accept: application/json",
            "Content-Type: application/json"
    })
    @POST("v1/daily_order/save_order")
    Call<ApiSaveOrderModel> API_SAVE_ORDER_MODEL_CALL(@Body List<SaveOrderModel> body);

    @Headers({
            "Accept: application/json",
            "Content-Type: application/json"
    })
    @POST("v1/doctor_call/save_call_doctor")
    Call<ApiSaveDcrModel> API_SAVE_DCR_MODEL_CALL(@Body List<SaveDcrModel> body);


    @Headers({
            "Accept: application/json",
            "Content-Type: application/json"
    })
    @POST("v1/daily_order/order_list")
    Call<ApiOrderListModel> API_ORDER_LIST_MODEL_CALL(@Body JsonObject body);
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json"
    })
    @POST("v1/doctor_call/dc_sent_list")
    Call<ApiDcrListModel> API_DCR_LIST_MODEL_CALL(@Body JsonObject body);

    @Headers({
            "Accept: application/json",
            "Content-Type: application/json"
    })
    @POST("v1/doctor_call/report_gadget_utilization")
    Call<ApiGadgetUtilizationModel> API_GADGET_UTILIZATION_MODEL_CALL(@Body JsonObject body);

    @POST("v1/daily_order/order_summary")
    Call<ApiOrderReportModel> API_ORDER_SUMMARY(@Body JsonObject body);

    @GET("v1/order_app/other_function/promotional_items_month")
    Call<ApiMonthList> API_MONTH_LIST_CALL();


    @GET("v1/order_app/other_function/training_product_details/{id}/{month}")
    Call<ApiMediaModel> API_TRAINING_PRODUCT_DETAILS_CALL(@Path("id") String id,@Path("month") String month);

    @GET("v1/order_app/other_function/training_product_details_common/{team}/{month}")
    Call<ApiMediaModel> API_TRAINING_PRODUCT_DETAILS_COMMON_CALL(@Path("team") String id,@Path("month") String month);

    @GET("v1/order_app/other_function/training_product_list/{team}/{month}")
    Call<ApiTrainingProductList> API_TRAINING_PRODUCT_LIST_CALL(@Path("team") String team,@Path("month") String month);

    @GET("v1/order_app/other_function/check_brand_common/{team}/{month}")
    Call<ApiCheckBrandCommonModel> API_CHECK_BRAND_COMMON_MODEL_CALL(@Path("team") String team, @Path("month") String month);


//    @POST("order_app/other_function/quiz_multi_questions")
//    Call<ApiQuestionModel> API_QUESTION_MODEL_CALL(@Body JsonObject body);
//
//    @POST("order_app/other_function/quiz_multi_questions_answear")
//    Call<ApiAnswareModel> API_ANSWARE_MODEL_CALL(@Body JsonObject body);

    @POST("v1/order_app/other_function/quiz_multi_questions")
    Call<ApiQuestionModel> API_QUIZ_QUESTION_MODEL_CALL(@Body JsonObject body);

    @Headers({
            "Accept: application/json",
            "Content-Type: application/json"
    })
    @POST("v1/order_app/other_function/quiz_multi_questions_answear")
    Call<ApiQuestionModel> API_QUIZ_ANSWARE_MODEL_CALL(@Body JsonObject body);

    @GET("v1/order_app/other_function/quiz_result_list/{id}")
    Call<ApiQuestionResultModel> API_QUIZ_RESULT_MODEL_CALL(@Path("id") String id);

    @Headers({
            "Accept: application/json",
            "Content-Type: application/json"
    })
    @POST("v1/order_app/other_function/quizs")
    Call<ApiQuizsModel> API_QUIZS_MODEL_CALL(@Body JsonObject body);

    @POST("v1/order_app/other_function/customer_location")
    Call<ApiSaveCustomerLocationModel> API_SAVE_CUSTOMER_LOCATION_MODEL_CALL(@Body JsonObject body);
}
