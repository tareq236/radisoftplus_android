package com.radisoftplus.utils;

import android.app.Dialog;
import android.content.Context;
import android.view.Window;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.radisoftplus.R;


public class BusyDialog {

    private final Dialog dialog;
    private TextView title_text;

    public BusyDialog(Context c, boolean cancelable, String title) {
        dialog = new Dialog(c, android.R.style.Theme_Translucent);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_progress_dialog);
        dialog.setCancelable(cancelable);
        title_text = (TextView) dialog.findViewById(R.id.title_text_progress);
        title_text.setText(title);

    }

    public void show() {
        try {
            dialog.show();
        } catch (Exception e) {

        }
    }

    public void dismiss() {
        dialog.cancel();
    }

    public boolean isShowing(){
        if (dialog.isShowing())
            return true;
        else return false;
    }

}
