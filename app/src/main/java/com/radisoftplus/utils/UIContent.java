package com.radisoftplus.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.PopupWindow;


import com.radisoftplus.R;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;


public class UIContent {
    public static String dataEmptyString = "Some data field are empty, please check again";
    public static String invalidQuantity = "Please enter brand name or sales unit or doctors name or child id";
    private Context mContext;
    PopupWindow pw;

    public UIContent(Context mContext) {
        this.mContext = mContext;
    }

    public  void showExitDialog(final Context c, final String title,
                                final String message) {
        final AlertDialog.Builder aBuilder = new AlertDialog.Builder(c, R.style.AppCompatAlertDialogStyle);
        aBuilder.setTitle(title);
        // aBuilder.setIcon(R.drawable.ic_launcher);
        aBuilder.setMessage(message);

        aBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(final DialogInterface dialog, final int which) {
                dialog.dismiss();
                // activity.finish();


            }

        });
//        aBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialogInterface, int i) {
//                dialogInterface.dismiss();
//            }
//        });

        aBuilder.show();
    }

    public  boolean isOnline() {

        ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        return activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
    }


    public void hideSoftKeyboard(EditText editText) {
        if (editText == null)
            return;

        InputMethodManager imm = (InputMethodManager)mContext.getSystemService(Service.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
    }
    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager =(InputMethodManager)mContext.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }



    public String setCurrency(double itemName){
        String currency = " ";
        Locale locale = new Locale("bn", "BD");
        NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(locale);
        currency = currencyFormatter.format(itemName);

       return  currency;
    }

    public void showSoftKeyboard() {
        InputMethodManager imm = (InputMethodManager)mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);


    }

    public String parseLastOrderDate(String givenDate){
        String parseDate = " ";
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        try {
            Date newDate = format.parse(givenDate);
            format = new SimpleDateFormat("dd MMM yyyy HH:mm aa");
            parseDate = format.format(newDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return  parseDate;
    }

    public String parseDate(String givenDate){
        String parseDate = " ";
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        try {
            Date newDate = format.parse(givenDate);
            format = new SimpleDateFormat("dd-MMM-yy HH:mm:ss");
            parseDate = format.format(newDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return  parseDate;
    }
    public String parseOrderDate(String givenDate){
        String parseDate = " ";
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        try {
            Date newDate = format.parse(givenDate);
            format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            parseDate = format.format(newDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return  parseDate;
    }



    public String parseDateOfBirth(String givenDate){
        String parseDate = " ";
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        try {
            Date newDate = format.parse(givenDate);
            format = new SimpleDateFormat("dd-MM-yyyy");
            parseDate = format.format(newDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return  parseDate;
    }




    public String parseDOB(String givenDate){
        String parseDate = " ";
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        try {
            Date newDate = format.parse(givenDate);
            format = new SimpleDateFormat("yyyy-MM-dd");
            parseDate = format.format(newDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return  parseDate;
    }
    public String parseInSampCDate(String givenDate){
        String parseDate = " ";
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        try {
            Date newDate = format.parse(givenDate);
            format = new SimpleDateFormat("dd-MMM");
            parseDate = format.format(newDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return  parseDate;
    }


    public String getCurrentDate() {
        DateFormat dateFormat = new SimpleDateFormat("dd-MMM");
        Date date = new Date();
        return dateFormat.format(date);
    }


    public int getAge(String dobString){

        Date date = null;
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        try {
            date = sdf.parse(dobString);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if(date == null) return 0;

        Calendar dob = Calendar.getInstance();
        Calendar today = Calendar.getInstance();

        dob.setTime(date);

        int year = dob.get(Calendar.YEAR);
        int month = dob.get(Calendar.MONTH);
        int day = dob.get(Calendar.DAY_OF_MONTH);

        dob.set(year, month+1, day);

        int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);

        if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)){
            age--;
        }



        return age;
    }


    public double convertToDouble(String temp){
        double result = 0;
        if(temp!=null && temp.trim().length()>0){
            String a = temp;
            //replace all commas if present with no comma
            String s = a.replaceAll(",","").trim();
            // if there are any empty spaces also take it out.
            String f = s.replaceAll(" ", "");
            //now convert the string to double

          result = Double.parseDouble(f);


        }

        return result; // return the result
    }

    public String convertDecimalFormat(double inNumber){
        String result = " ";
        if(inNumber>0){
           result  = String.format("%,.2f", inNumber);
        }

        else {
            result  = String.format("%,.2f", 0.00);
        }

        return  result;
    }
    public   int convertToInteger(String temp){
        int result = 0;
        if(temp!=null && temp.trim().length()>0){
            String a = temp;
            //replace all commas if present with no comma
            String s = a.replaceAll(",","").trim();
            // if there are any empty spaces also take it out.
            String f = s.replaceAll(" ", "");
            //now convert the string to double
             result = Integer.parseInt(f);
        }

        return result; // return the result
    }



    public String getFormatedNumber(String number){
        if(!number.isEmpty()) {
            double val = Double.parseDouble(number);
            return NumberFormat.getNumberInstance(Locale.US).format(val);
        }else{
            return "0";
        }
    }


    public boolean isValidate(String email)
    {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

}
