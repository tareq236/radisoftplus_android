package com.radisoftplus.utils;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetroClient {
    private static Retrofit getRetrofitInstance() {
//        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
//        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
//        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        String authToken = "|#mjdfodf`lfz#;#789:1.GHIJK.23456.BCDEF#-#nfttbhf#;#Uif!Sbejtpgu!Qmvt!mjdfotf!lfz!gps!uif!Efwfmpqnfou!Wfstjpo!ibt!fyqjsfe/!Qmfbtf!pcubjo!b!ofx!mjdfotf!lfz!gspn!zpvs!tfswjdf!qspwjefs/!)Uijt!jt!opu!bqqmjdbcmf!up!uif!Qspevdujpo!wfstjpo*/#-#fyqjsz`ebuf#;#3135.13.27#~";

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();
                Request.Builder requestBuilder = original.newBuilder()
                        .header("Authorization", "Bearer " + authToken) // Add your token here
                        .method(original.method(), original.body());
                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();

        return new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();
    }

    public static ApiService getApiService() {
        return getRetrofitInstance().create(ApiService.class);
    }

}
