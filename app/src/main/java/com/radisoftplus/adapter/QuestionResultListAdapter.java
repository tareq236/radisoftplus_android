package com.radisoftplus.adapter;


import static com.radisoftplus.utils.SaveLocalStorage.getValueFromSharedPreferences;
import static com.radisoftplus.utils.SaveLocalStorage.saveToSharedPreferences;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;


import com.radisoftplus.R;
import com.radisoftplus.activities.TrainingProductDetailsActivity;
import com.radisoftplus.activities.TrainingProductDetailsCommonActivity;
import com.radisoftplus.models.AnswareModel;
import com.radisoftplus.utils.UIContent;

import java.util.ArrayList;
import java.util.List;

public class QuestionResultListAdapter extends RecyclerView.Adapter<QuestionResultListAdapter.ViewHolder>{

    public List<AnswareModel> dataList = new ArrayList<>();
    Context mContext;
    UIContent uiContent;
    private String item_name;
    private LayoutInflater mInflater;

    public QuestionResultListAdapter(List<AnswareModel> rList, Context mContext) {
        this.dataList = rList;
        this.mContext = mContext;
        this.mInflater = LayoutInflater.from(mContext);
        uiContent = new UIContent(mContext);
    }

    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup data, int viewType) {
        View view = mInflater.inflate(R.layout.rview_answare_item, data, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        item_name  = dataList.get(position).getText_question();
        holder.txvItemName.setText(String.valueOf(item_name));

    }

    // total number of rows
    @Override
    public int getItemCount() {
        return dataList.size();
    }

    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txvItemName;
        LinearLayout cardViewLayout;

        ViewHolder(View itemView) {
            super(itemView);
            txvItemName = (TextView) itemView.findViewById(R.id.txv_item_name);
            cardViewLayout = itemView.findViewById(R.id.card_view);

        }

    }

}
