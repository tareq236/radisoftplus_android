package com.radisoftplus.adapter;

import static com.radisoftplus.utils.SaveLocalStorage.getValueFromSharedPreferences;
import static com.radisoftplus.utils.SaveLocalStorage.saveToSharedPreferences;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.radisoftplus.R;

import com.radisoftplus.activities.QuizEnterDoctorActivity;
import com.radisoftplus.activities.QuizStartActivity;
import com.radisoftplus.models.ApiQuizsModel;
import com.radisoftplus.utils.UIContent;

import java.util.ArrayList;
import java.util.List;

public class QuizsAdapter extends RecyclerView.Adapter<QuizsAdapter.ViewHolder> {

    public List<ApiQuizsModel.Results> dataList = new ArrayList<>();
    Context mContext;
    UIContent uiContent;
    private Integer id;
    private String title = " ";
    private String type ;
    private String created_at ;
    private LayoutInflater mInflater;

    public QuizsAdapter(List<ApiQuizsModel.Results> dataList, Context mContext) {
        this.dataList = dataList;
        this.mContext = mContext;
        this.mInflater = LayoutInflater.from(mContext);
        uiContent = new UIContent(mContext);
    }

    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup data, int viewType) {
        View view = mInflater.inflate(R.layout.rview_item, data, false);
        return new ViewHolder(view);
    }


    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        holder.txv_item_name.setText(dataList.get(position).getQuiz_title());

        holder.cardViewLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String qs_from_screen   = getValueFromSharedPreferences("qs_from_screen", mContext);
                if(qs_from_screen.equals("Quiz")){

                    saveToSharedPreferences("quizs_id",dataList.get(position).getId().toString(), mContext);
                    saveToSharedPreferences("quiz_type",dataList.get(position).getType(), mContext);
                    saveToSharedPreferences("quiz_image",dataList.get(position).getImage(), mContext);
                    saveToSharedPreferences("quiz_dr_child_id","001122", mContext);
                    saveToSharedPreferences("quiz_doctor_name","Test Doctor", mContext);
                    saveToSharedPreferences("quiz_doctor_cell_phone","01717116732", mContext);
                    saveToSharedPreferences("quiz_doctor_speciality","MEDICINE", mContext);
                    Intent intent = new Intent(mContext, QuizStartActivity.class);
                    mContext.startActivity(intent);
                }else{
                    Intent intent = new Intent(mContext, QuizEnterDoctorActivity.class);
                    saveToSharedPreferences("quizs_id",dataList.get(position).getId().toString(), mContext);
                    saveToSharedPreferences("quiz_type",dataList.get(position).getType(), mContext);
                    saveToSharedPreferences("quiz_image",dataList.get(position).getImage(), mContext);
                    mContext.startActivity(intent);
                }
            }
        });
    }

    // total number of rows
    @Override
    public int getItemCount() {
        return dataList.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txv_item_name;
        LinearLayout cardViewLayout;

        ViewHolder(View itemView) {
            super(itemView);
            txv_item_name = (TextView) itemView.findViewById(R.id.txv_item_name);
            cardViewLayout = (LinearLayout) itemView.findViewById(R.id.card_view);

        }

    }

}
