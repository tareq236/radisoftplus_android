package com.radisoftplus.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.radisoftplus.R;
import com.radisoftplus.activities.DcrScreenTwoActivity;
import com.radisoftplus.activities.OrderListActivity;
import com.radisoftplus.models.ApiFindMaterialModel;
import com.radisoftplus.models.OrderListModel;
import com.radisoftplus.utils.UIContent;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import androidx.recyclerview.widget.RecyclerView;

import static com.radisoftplus.utils.SaveLocalStorage.getValueFromSharedPreferences;
import static com.radisoftplus.utils.SaveLocalStorage.saveToSharedPreferences;

public class SearchBrAdapter extends RecyclerView.Adapter<SearchBrAdapter.ViewHolder>{
    public List<ApiFindMaterialModel.Result> dataList = new ArrayList<>();
    Context mContext;
    UIContent uiContent;
    private String item_name;
    private LayoutInflater mInflater;


    public SearchBrAdapter(List<ApiFindMaterialModel.Result> rList, Context mContext) {
        this.dataList = rList;
        this.mContext = mContext;
        this.mInflater = LayoutInflater.from(mContext);
        uiContent = new UIContent(mContext);
    }

    // inflates the row layout from xml when needed
    @Override
    public SearchBrAdapter.ViewHolder onCreateViewHolder(ViewGroup data, int viewType) {
        View view = mInflater.inflate(R.layout.rview_item, data, false);
        return new SearchBrAdapter.ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(SearchBrAdapter.ViewHolder holder, final int position) {

        item_name = dataList.get(position).getPMName();
        holder.txvProductName.setText(String.valueOf(item_name));

        holder.cardViewLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveToSharedPreferences("dcr-select-br-name", dataList.get(position).getPMName(), mContext);
                saveToSharedPreferences("dcr-select-br-code", dataList.get(position).getPMCode(), mContext);
                Intent intent = new Intent(mContext, DcrScreenTwoActivity.class);
                mContext.startActivity(intent);
                ((Activity) mContext).finish();
            }
        });
    }

    // total number of rows
    @Override
    public int getItemCount() {
        return dataList.size();
    }

    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txvProductName;
        LinearLayout cardViewLayout;

        ViewHolder(View itemView) {
            super(itemView);
            txvProductName = (TextView) itemView.findViewById(R.id.txv_item_name);
            cardViewLayout = itemView.findViewById(R.id.card_view);
        }
    }
}
