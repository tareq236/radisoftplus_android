package com.radisoftplus.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.radisoftplus.R;
import com.radisoftplus.activities.OrderReportActivity;
import com.radisoftplus.activities.SentOrderDetailsActivity;
import com.radisoftplus.models.ApiOrderReportModel;
import com.radisoftplus.utils.UIContent;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import static com.radisoftplus.utils.SaveLocalStorage.saveToSharedPreferences;

public class OrderReportAdapter extends RecyclerView.Adapter<OrderReportAdapter.ViewHolder>{
    public List<ApiOrderReportModel.Result> dataList = new ArrayList<>();
    Context mContext;
    UIContent uiContent;

    private LayoutInflater mInflater;

    public OrderReportAdapter(List<ApiOrderReportModel.Result> rList, Context mContext) {
        this.dataList = rList;
        this.mContext = mContext;
        this.mInflater = LayoutInflater.from(mContext);
        uiContent = new UIContent(mContext);
    }

    // inflates the row layout from xml when needed
    @Override
    public OrderReportAdapter.ViewHolder onCreateViewHolder(ViewGroup data, int viewType) {
        View view = mInflater.inflate(R.layout.rview_order_report_item, data, false);
        return new OrderReportAdapter.ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(OrderReportAdapter.ViewHolder holder, final int position) {

        int id = dataList.get(position).getId();
        String orderType = dataList.get(position).getOrderType();
        String customerId = dataList.get(position).getCustomerId();
        int status = dataList.get(position).getStatus();

        List<ApiOrderReportModel.Order> orderList = dataList.get(position).getOrderList();
        double totatlTP = 0;
        double sum = 0;
        for(int i=0;i<orderList.size();i++){
            double unitTp = uiContent.convertToDouble(orderList.get(i).getUnitTp());
            sum = sum + unitTp;
            totatlTP = sum;
        }

        if(status==1){
            holder.tvS.setText("Y");
        }else {
            holder.tvS.setText("N");
        }

        if (mContext instanceof OrderReportActivity) {
            ((OrderReportActivity)mContext).totalCal(String.valueOf(new DecimalFormat("##.##").format(totatlTP)),String.valueOf(orderList.size()));
        }

        holder.tvId.setText(String.valueOf(id));
        holder.tvPM.setText(orderType);
        holder.tvChemist.setText(customerId);
        holder.tvTotalTP.setText(String.valueOf(new DecimalFormat("##.##").format(totatlTP)));
        holder.tvTo.setText(String.valueOf(orderList.size()));


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                mContext.startActivity(new Intent(mContext, SentOrderDetailsActivity.class)
//                        .putExtra("sent_from","orderreport"));

                GsonBuilder gsonBuilder = new GsonBuilder();
                Gson gson = gsonBuilder.create();
                String JSONObjectString = gson.toJson(dataList.get(position).getOrderList());
                saveToSharedPreferences("sent-order-product-list", JSONObjectString, mContext);
                Intent intent = new Intent(mContext, SentOrderDetailsActivity.class);
                intent.putExtra("sent-order-order_id", dataList.get(position).getId().toString());
                intent.putExtra("sent-order-order_date", dataList.get(position).getOrderDate());
                intent.putExtra("sent-order-emp_name", dataList.get(position).getRefUserDetails().getName());
                intent.putExtra("sent-order-emp_id", dataList.get(position).getRefUserDetails().getWorkAreaT());
                intent.putExtra("sent-order-chemist_id", dataList.get(position).getCustomerDetails().getPartner());
                intent.putExtra("sent-order-chemist_name", dataList.get(position).getCustomerDetails().getName1());
                intent.putExtra("sent-order-payment_mode", dataList.get(position).getOrderType());
                intent.putExtra("sent-order-crd_employee_id", dataList.get(position).getEmployeeId());
                intent.putExtra("sent_from","orderreport");
                mContext.startActivity(intent);
                ((Activity) mContext).finish();
            }
        });


    }

    // total number of rows
    @Override
    public int getItemCount() {
        return dataList.size();
    }

    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvId,tvPM,tvChemist,tvTo,tvTotalTP,tvS;


        ViewHolder(View itemView) {
            super(itemView);
            tvId = (TextView) itemView.findViewById(R.id.tv_id);
            tvPM = (TextView) itemView.findViewById(R.id.tv_pm);
            tvChemist = (TextView) itemView.findViewById(R.id.tv_chemist);
            tvTo = (TextView) itemView.findViewById(R.id.tv_to);
            tvTotalTP = (TextView) itemView.findViewById(R.id.tv_totaltp);
            tvS = (TextView) itemView.findViewById(R.id.tv_s);

        }

    }
}
