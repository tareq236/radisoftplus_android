package com.radisoftplus.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.radisoftplus.R;
import com.radisoftplus.activities.SentOrderDetailsActivity;
import com.radisoftplus.models.ApiOrderListModel;
import com.radisoftplus.utils.UIContent;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import androidx.recyclerview.widget.RecyclerView;

import static com.radisoftplus.utils.SaveLocalStorage.saveToSharedPreferences;

public class SentOrderListAdapter extends RecyclerView.Adapter<SentOrderListAdapter.ViewHolder>{
    public List<ApiOrderListModel.Result> dataList = new ArrayList<>();
    Context mContext;
    UIContent uiContent;
    private String order_id;
    private String order_date;
    private String emp_name;
    private String chemist_name;
    private LayoutInflater mInflater;

    public SentOrderListAdapter(List<ApiOrderListModel.Result> rList, Context mContext) {
        this.dataList = rList;
        this.mContext = mContext;
        this.mInflater = LayoutInflater.from(mContext);
        uiContent = new UIContent(mContext);
    }

    // inflates the row layout from xml when needed
    @Override
    public SentOrderListAdapter.ViewHolder onCreateViewHolder(ViewGroup data, int viewType) {
        View view = mInflater.inflate(R.layout.rview_sent_order_item, data, false);
        return new SentOrderListAdapter.ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(SentOrderListAdapter.ViewHolder holder, final int position) {

        holder.txvOrderId.setText("Order ID: " + dataList.get(position).getId().toString());

        String inputDate = dataList.get(position).getOrder_date();
        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");
        inputFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        SimpleDateFormat outputFormat = new SimpleDateFormat("dd/MMM/yyyy");
        try {
            Date date = inputFormat.parse(inputDate);
            String outputDate = outputFormat.format(date);
            holder.txvOrderDate.setText("Delivery date: " + outputDate);
        } catch (ParseException e) {
            holder.txvOrderDate.setText("Delivery date: " + dataList.get(position).getOrder_date());
            e.printStackTrace();
        }


        holder.txvEmpName.setText("Employee name: "+dataList.get(position).getRef_user_details().getName() + "("+dataList.get(position).getRef_user_details().getWork_area_t()+")");
        holder.txvChemistName.setText("Chemist: "+dataList.get(position).getCustomer_details().getName1() + "("+dataList.get(position).getCustomer_details().getPartner()+")");

        holder.cardViewLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GsonBuilder gsonBuilder = new GsonBuilder();
                Gson gson = gsonBuilder.create();
                String JSONObjectString = gson.toJson(dataList.get(position).getOrder_list());
                saveToSharedPreferences("sent-order-product-list", JSONObjectString, mContext);
                Intent intent = new Intent(mContext, SentOrderDetailsActivity.class);
                intent.putExtra("sent-order-order_id", dataList.get(position).getId().toString());
                intent.putExtra("sent-order-order_date", dataList.get(position).getOrder_date());
                intent.putExtra("sent-order-emp_name", dataList.get(position).getRef_user_details().getName());
                intent.putExtra("sent-order-emp_id", dataList.get(position).getRef_user_details().getWork_area_t());
                intent.putExtra("sent-order-chemist_id", dataList.get(position).getCustomer_details().getPartner());
                intent.putExtra("sent-order-chemist_name", dataList.get(position).getCustomer_details().getName1());
                intent.putExtra("sent-order-payment_mode", dataList.get(position).getOrder_type());
                intent.putExtra("sent-order-crd_employee_id", dataList.get(position).getEmployee_id());
                intent.putExtra("sent_from","orderlist");
                mContext.startActivity(intent);
                ((Activity) mContext).finish();
            }
        });

    }

    // total number of rows
    @Override
    public int getItemCount() {
        return dataList.size();
    }

    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txvOrderId;
        TextView txvOrderDate;
        TextView txvEmpName;
        TextView txvChemistName;
        LinearLayout cardViewLayout;

        ViewHolder(View itemView) {
            super(itemView);
            txvOrderId = (TextView) itemView.findViewById(R.id.txv_order_id);
            txvOrderDate = (TextView) itemView.findViewById(R.id.txv_order_date);
            txvEmpName = (TextView) itemView.findViewById(R.id.txv_employee_name);
            txvChemistName = (TextView) itemView.findViewById(R.id.txv_chemist_name);
            cardViewLayout = itemView.findViewById(R.id.card_view);
        }

    }
}
