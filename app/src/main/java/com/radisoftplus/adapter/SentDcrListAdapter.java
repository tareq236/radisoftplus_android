package com.radisoftplus.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.radisoftplus.R;
import com.radisoftplus.activities.SentDcrDetailsActivity;
import com.radisoftplus.activities.SentOrderDetailsActivity;
import com.radisoftplus.models.ApiDcrListModel;
import com.radisoftplus.utils.UIContent;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import androidx.recyclerview.widget.RecyclerView;

import static com.radisoftplus.utils.SaveLocalStorage.saveToSharedPreferences;

public class SentDcrListAdapter extends RecyclerView.Adapter<SentDcrListAdapter.ViewHolder>{
    public List<ApiDcrListModel.Result> dataList = new ArrayList<>();
    Context mContext;
    UIContent uiContent;
    private String call_date;
    private String call_session;
    private String emp_name;
    private String chemist_name;
    private LayoutInflater mInflater;

    public SentDcrListAdapter(List<ApiDcrListModel.Result> rList, Context mContext) {
        this.dataList = rList;
        this.mContext = mContext;
        this.mInflater = LayoutInflater.from(mContext);
        uiContent = new UIContent(mContext);
    }

    // inflates the row layout from xml when needed
    @Override
    public SentDcrListAdapter.ViewHolder onCreateViewHolder(ViewGroup data, int viewType) {
        View view = mInflater.inflate(R.layout.rview_sent_dcr_item, data, false);
        return new SentDcrListAdapter.ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(SentDcrListAdapter.ViewHolder holder, final int position) {

//        String sessions = "";
//        Integer sessionsNum = dataList.get(position).getSessions();
//        if(sessionsNum == 1){ sessions = "Morning"; }else{ sessions = "Evening";}

        holder.txvCallDate.setText("Call ID: " + dataList.get(position).getId());


        String inputDate = dataList.get(position).getCallDate();
        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");
        inputFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        SimpleDateFormat outputFormat = new SimpleDateFormat("dd/MMM/yyyy");
        try {
            Date date = inputFormat.parse(inputDate);
            String outputDate = outputFormat.format(date);
            holder.txvCallSession.setText(outputDate);
        } catch (ParseException e) {
            holder.txvCallSession.setText("Call date: " + dataList.get(position).getCallDate());
            e.printStackTrace();
        }

        holder.txvEmpName.setText("Employee name: "+dataList.get(position).getUserDetails().getName() + "("+dataList.get(position).getUserDetails().getWorkAreaT()+")");
        holder.txvDoctorName.setText("Doctor name: "+dataList.get(position).getCallDoctorDetails().getDoctorDetails().getDoctorName1() +" "+ dataList.get(position).getCallDoctorDetails().getDoctorDetails().getDoctorName2() + "("+dataList.get(position).getDoctorId()+")");

        holder.cardViewLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GsonBuilder gsonBuilder = new GsonBuilder();
                Gson gson = gsonBuilder.create();
                String JSONObjectString = gson.toJson(dataList.get(position));
                saveToSharedPreferences("sent-dcr-details", JSONObjectString, mContext);
                Intent intent = new Intent(mContext, SentDcrDetailsActivity.class);

                mContext.startActivity(intent);
                ((Activity) mContext).finish();
            }
        });

    }

    // total number of rows
    @Override
    public int getItemCount() {
        return dataList.size();
    }

    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txvCallDate;
        TextView txvCallSession;
        TextView txvEmpName;
        TextView txvDoctorName;
        LinearLayout cardViewLayout;

        ViewHolder(View itemView) {
            super(itemView);
            txvCallDate = (TextView) itemView.findViewById(R.id.txv_call_date);
            txvCallSession = (TextView) itemView.findViewById(R.id.txv_call_session);
            txvEmpName = (TextView) itemView.findViewById(R.id.txv_employee_name);
            txvDoctorName = (TextView) itemView.findViewById(R.id.txv_doctor_name);
            cardViewLayout = itemView.findViewById(R.id.card_view);
        }

    }
}
