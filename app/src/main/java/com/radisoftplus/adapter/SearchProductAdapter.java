package com.radisoftplus.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.radisoftplus.R;
import com.radisoftplus.activities.OrderListActivity;
import com.radisoftplus.models.ApiFindMaterialModel;
import com.radisoftplus.models.OrderListModel;
import com.radisoftplus.utils.UIContent;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import androidx.recyclerview.widget.RecyclerView;

import static com.radisoftplus.utils.SaveLocalStorage.getValueFromSharedPreferences;

public class SearchProductAdapter extends RecyclerView.Adapter<SearchProductAdapter.ViewHolder>{
    public List<ApiFindMaterialModel.Result> dataList = new ArrayList<>();
    Context mContext;
    UIContent uiContent;
    private String item_name;
    private String item_details;
    private LayoutInflater mInflater;
    List<OrderListModel> bList = new ArrayList<>();

    public SearchProductAdapter(List<ApiFindMaterialModel.Result> rList, Context mContext) {
        this.dataList = rList;
        this.mContext = mContext;
        this.mInflater = LayoutInflater.from(mContext);
        uiContent = new UIContent(mContext);

        String order_list = getValueFromSharedPreferences("order-list", mContext);
        if(order_list != null){
            try {
                JSONArray mainObject = new JSONArray(order_list);
                for (int i = 0; i < mainObject.length(); i++) {
                    JSONObject object = mainObject.getJSONObject(i);
                    OrderListModel orderListModel = new OrderListModel();
                    orderListModel.setMatnr(object.getString("matnr"));
                    orderListModel.setMrp(object.getString("mrp"));
                    orderListModel.setUnit_tp(object.getString("unit_tp"));
                    orderListModel.setUnit_vat(object.getString("unit_vat"));
                    orderListModel.setMaterial_name(object.getString("material_name"));
                    orderListModel.setQty(object.getInt("qty"));
                    bList.add(orderListModel);
                }
            } catch (Exception e) {
                Log.w("exception-verify data: ", e.getMessage());
            }
        }
    }

    // inflates the row layout from xml when needed
    @Override
    public SearchProductAdapter.ViewHolder onCreateViewHolder(ViewGroup data, int viewType) {
        View view = mInflater.inflate(R.layout.rview_product_item, data, false);
        return new SearchProductAdapter.ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(SearchProductAdapter.ViewHolder holder, final int position) {

        item_name = dataList.get(position).getMaterialName();
        item_details = "MRP: " + dataList.get(position).getMrp() + " TP: "+ dataList.get(position).getUnitTp() + " Vat: " + dataList.get(position).getUnitVat();
        holder.txvProductName.setText(String.valueOf(item_name));
        holder.txvProductDetails.setText(String.valueOf(item_details));

        for (int i=0;i<bList.size();i++){
            if(bList.get(i).getMatnr().equals(dataList.get(position).getMatnr())){
                holder.llContainView.setBackgroundColor(Color.parseColor("#FFE68CAB"));
                holder.txvProductName.setBackgroundColor(Color.parseColor("#FFE68CAB"));
                holder.txvProductDetails.setBackgroundColor(Color.parseColor("#FFE68CAB"));
            }
        }

        holder.cardViewLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (bList.size() == 0){
                    Intent intent = new Intent(mContext, OrderListActivity.class);
                    intent.putExtra("order-from-screen", "SearchProductAdapter");
                    intent.putExtra("sales_org", dataList.get(position).getSalesOrg());
                    intent.putExtra("matnr", dataList.get(position).getMatnr());
                    intent.putExtra("mrp", dataList.get(position).getMrp());
                    intent.putExtra("unit_tp", dataList.get(position).getUnitTp());
                    intent.putExtra("unit_vat", dataList.get(position).getUnitVat());
                    intent.putExtra("product_name", dataList.get(position).getMaterialName());
                    intent.putExtra("product_details", "MRP: " + dataList.get(position).getMrp() + " TP: "+ dataList.get(position).getUnitTp() + " Vat: " + dataList.get(position).getUnitVat());
                    mContext.startActivity(intent);
                    ((Activity) mContext).finish();
                }else{
                    Boolean isAvailable = false;
                    for (int i=0;i<bList.size();i++){
                        if(dataList.get(position).getMatnr().equals(bList.get(i).getMatnr())){
                            isAvailable = true;
                            break;
                        }
                    }
                    if(!isAvailable){
                        Intent intent = new Intent(mContext, OrderListActivity.class);
                        intent.putExtra("order-from-screen", "SearchProductAdapter");
                        intent.putExtra("sales_org", dataList.get(position).getSalesOrg());
                        intent.putExtra("matnr", dataList.get(position).getMatnr());
                        intent.putExtra("mrp", dataList.get(position).getMrp());
                        intent.putExtra("unit_tp", dataList.get(position).getUnitTp());
                        intent.putExtra("unit_vat", dataList.get(position).getUnitVat());
                        intent.putExtra("product_name", dataList.get(position).getMaterialName());
                        intent.putExtra("product_details", "MRP: " + dataList.get(position).getMrp() + " TP: "+ dataList.get(position).getUnitTp() + " Vat: " + dataList.get(position).getUnitVat());
                        mContext.startActivity(intent);
                        ((Activity) mContext).finish();
                    }
                }
            }
        });
    }

    // total number of rows
    @Override
    public int getItemCount() {
        return dataList.size();
    }

    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txvProductName;
        TextView txvProductDetails;
        LinearLayout cardViewLayout;
        LinearLayout llContainView;

        ViewHolder(View itemView) {
            super(itemView);
            txvProductName = (TextView) itemView.findViewById(R.id.txv_product_name);
            txvProductDetails = (TextView) itemView.findViewById(R.id.txv_product_details);
            cardViewLayout = itemView.findViewById(R.id.card_view);
            llContainView = itemView.findViewById(R.id.ll_contain_view);

        }

    }
}
