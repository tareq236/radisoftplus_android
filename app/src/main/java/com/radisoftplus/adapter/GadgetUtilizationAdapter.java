package com.radisoftplus.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.radisoftplus.R;
import com.radisoftplus.activities.SentOrderDetailsActivity;
import com.radisoftplus.models.ApiGadgetUtilizationModel;
import com.radisoftplus.models.ApiGadgetUtilizationModel;
import com.radisoftplus.utils.UIContent;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

import static com.radisoftplus.utils.SaveLocalStorage.saveToSharedPreferences;

public class GadgetUtilizationAdapter extends RecyclerView.Adapter<GadgetUtilizationAdapter.ViewHolder>{
    public List<ApiGadgetUtilizationModel.Result> dataList = new ArrayList<>();
    Context mContext;
    UIContent uiContent;
    private LayoutInflater mInflater;

    public GadgetUtilizationAdapter(List<ApiGadgetUtilizationModel.Result> rList, Context mContext) {
        this.dataList = rList;
        this.mContext = mContext;
        this.mInflater = LayoutInflater.from(mContext);
        uiContent = new UIContent(mContext);
    }

    // inflates the row layout from xml when needed
    @Override
    public GadgetUtilizationAdapter.ViewHolder onCreateViewHolder(ViewGroup data, int viewType) {
        View view = mInflater.inflate(R.layout.rview_gadget_utilization_item, data, false);
        return new GadgetUtilizationAdapter.ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(GadgetUtilizationAdapter.ViewHolder holder, final int position) {

        holder.brandTextView.setText(dataList.get(position).getBrand());
        holder.gadgetTextView.setText(dataList.get(position).getGadget());
        holder.allocateTextView.setText(dataList.get(position).getAllocate());
        holder.usesTextView.setText(dataList.get(position).getUses());
        holder.stockTextView.setText(dataList.get(position).getStock());

    }

    // total number of rows
    @Override
    public int getItemCount() {
        return dataList.size();
    }

    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView brandTextView ;
        TextView gadgetTextView;
        TextView allocateTextView ;
        TextView usesTextView ;
        TextView stockTextView;

        ViewHolder(View itemView) {
            super(itemView);
            brandTextView = itemView.findViewById(R.id.brandTextView);
            gadgetTextView = itemView.findViewById(R.id.gadgetTextView);
            allocateTextView = itemView.findViewById(R.id.allocateTextView);
            usesTextView = itemView.findViewById(R.id.usesTextView);
            stockTextView = itemView.findViewById(R.id.stockTextView);

        }

    }
}
