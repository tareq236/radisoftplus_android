package com.radisoftplus.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.radisoftplus.R;
import com.radisoftplus.models.ApiFindWorkAreaModel;

import java.util.List;

public class WorkAreaIdAdapter extends RecyclerView.Adapter<WorkAreaIdAdapter.ViewHolder>{

    Context mContext;
    private LayoutInflater mInflater;
    List<ApiFindWorkAreaModel.Result> list;

    public WorkAreaIdAdapter(Context mContext,List<ApiFindWorkAreaModel.Result> list) {
        this.mContext = mContext;
        this.mInflater = LayoutInflater.from(mContext);
        this.list = list;
    }

    // inflates the row layout from xml when needed
    @Override
    public WorkAreaIdAdapter.ViewHolder onCreateViewHolder(ViewGroup list, int viewType) {
        View view = mInflater.inflate(R.layout.item_workarea_id, list, false);
        return new WorkAreaIdAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        String workAreaIdName = list.get(position).getName();
        String id = list.get(position).getWorkAreaT();
        if(workAreaIdName!=null){
            holder.txvWorkAreaId.setText(id+" - "+workAreaIdName);
        }
    }


    // total number of rows
    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txvWorkAreaId ;

        ViewHolder(View itemView) {
            super(itemView);
            txvWorkAreaId = (TextView) itemView.findViewById(R.id.txvWorkAreaId);


        }
    }
}
