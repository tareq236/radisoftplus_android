package com.radisoftplus.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.radisoftplus.R;
import com.radisoftplus.activities.OrderActivity;
import com.radisoftplus.activities.OrderDraftActivity;
import com.radisoftplus.activities.OrderListActivity;
import com.radisoftplus.activities.SentOrderDetailsActivity;
import com.radisoftplus.models.ApiOrderListModel;
import com.radisoftplus.models.DraftOrderModel;
import com.radisoftplus.models.DraftOrderTableModel;
import com.radisoftplus.utils.UIContent;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import androidx.recyclerview.widget.RecyclerView;

import static com.radisoftplus.utils.SaveLocalStorage.saveToSharedPreferences;

public class DraftOrderListAdapter extends RecyclerView.Adapter<DraftOrderListAdapter.ViewHolder>{
    public List<DraftOrderTableModel> dataList = new ArrayList<>();
    Context mContext;
    UIContent uiContent;
    private String order_id;
    private String order_date;
    private String emp_name;
    private String chemist_name;
    private LayoutInflater mInflater;

    public DraftOrderListAdapter(List<DraftOrderTableModel> rList, Context mContext) {
        this.dataList = rList;
        this.mContext = mContext;
        this.mInflater = LayoutInflater.from(mContext);
        uiContent = new UIContent(mContext);
    }

    // inflates the row layout from xml when needed
    @Override
    public DraftOrderListAdapter.ViewHolder onCreateViewHolder(ViewGroup data, int viewType) {
        View view = mInflater.inflate(R.layout.rview_draft_order_item, data, false);
        return new DraftOrderListAdapter.ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(DraftOrderListAdapter.ViewHolder holder, final int position) {

        String inputCreatedAtDate = dataList.get(position).getCreated_at();
        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        inputFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        SimpleDateFormat outputFormat = new SimpleDateFormat("dd/MMM/yyyy HH:mm:ss");
        try {
            Date createdAtDate = inputFormat.parse(inputCreatedAtDate);
            String outputCreatedAtDate = outputFormat.format(createdAtDate);
            holder.txvCreatedAt.setText("Created at: "+outputCreatedAtDate);
        } catch (ParseException e) {
            holder.txvCreatedAt.setText("Created at: "+dataList.get(position).getCreated_at());
            e.printStackTrace();
        }

        String inputOrderDate = dataList.get(position).getCreated_at();
        SimpleDateFormat inputOrderFormat = new SimpleDateFormat("yyyy-MM-dd");
        inputOrderFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        SimpleDateFormat outputOrderFormat = new SimpleDateFormat("dd/MMM/yyyy");
        try {
            Date orderDate = inputFormat.parse(inputOrderDate);
            String outputOrderDate = outputOrderFormat.format(orderDate);
            holder.txvOrderDate.setText("Delivery date: " + outputOrderDate);
        } catch (ParseException e) {
            holder.txvOrderDate.setText("Delivery date: " + dataList.get(position).getOrder_date());
            e.printStackTrace();
        }

        holder.txvOrderId.setText("ID: " + dataList.get(position).getId());
        holder.txvEmpName.setText("Employee name: "+dataList.get(position).getAddress() + "("+dataList.get(position).getWork_area()+")");
        holder.txvChemistName.setText("Chemist: "+dataList.get(position).getChemist_name() + "("+dataList.get(position).getChemist_id()+")");

        holder.cardViewLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, OrderActivity.class);
                intent.putExtra("is_draft", true);
                saveToSharedPreferences("draft-order-id",dataList.get(position).getId().toString(), mContext);
                saveToSharedPreferences("find-word_area-work_area",dataList.get(position).getWork_area(), mContext);
                saveToSharedPreferences("find-word_area-address",dataList.get(position).getAddress(), mContext);
                saveToSharedPreferences("find-customer-partner", dataList.get(position).getChemist_id(), mContext);
                saveToSharedPreferences("find-customer-customer_name", dataList.get(position).getChemist_name(), mContext);
                String orderDate = dataList.get(position).getOrder_date();
                String[] dateComponents = orderDate.split("-");
                saveToSharedPreferences("order-delivery_date",dateComponents[1] + "/" + dateComponents[2] + "/" + dateComponents[0], mContext);
                saveToSharedPreferences("order-payment_mode", dataList.get(position).getPayment_mode(), mContext);
                String jsonString = dataList.get(position).getJson_details();
                Gson gson = new Gson();
                DraftOrderModel[] orders = gson.fromJson(jsonString, DraftOrderModel[].class);
                saveToSharedPreferences("order-emp_id_cr_team", orders[0].getEmployee_id(), mContext);
                Gson gsonOrderList = new Gson();
                String JSONObjectString = gsonOrderList.toJson(orders[0].getOrder_list());
                saveToSharedPreferences("order-list",JSONObjectString, mContext);

                mContext.startActivity(intent);
                ((Activity) mContext).finish();
            }
        });

        holder.imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int id = dataList.get(position).getId();
                dataList.remove(holder.getPosition());
                notifyItemRemoved(holder.getPosition());
                notifyItemRangeChanged(holder.getPosition(), dataList.size());
                if (mContext instanceof OrderDraftActivity) {
                    ((OrderDraftActivity)mContext).deleteItem(id);
                }
            }
        });
    }

    // total number of rows
    @Override
    public int getItemCount() {
        return dataList.size();
    }

    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txvOrderId;
        TextView txvOrderDate;
        TextView txvEmpName;
        TextView txvChemistName;
        TextView txvCreatedAt;
        ImageView imgDelete;
        LinearLayout cardViewLayout;

        ViewHolder(View itemView) {
            super(itemView);
            txvOrderId = (TextView) itemView.findViewById(R.id.txv_order_id);
            txvOrderDate = (TextView) itemView.findViewById(R.id.txv_order_date);
            txvEmpName = (TextView) itemView.findViewById(R.id.txv_employee_name);
            txvChemistName = (TextView) itemView.findViewById(R.id.txv_chemist_name);
            txvCreatedAt = (TextView) itemView.findViewById(R.id.txv_created_at);
            imgDelete = (ImageView) itemView.findViewById(R.id.img_delete);
            cardViewLayout = itemView.findViewById(R.id.card_view);
        }

    }
}
