package com.radisoftplus.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.radisoftplus.R;
import com.radisoftplus.activities.GetCustomerLocationActivity;
import com.radisoftplus.activities.OrderActivity;
import com.radisoftplus.models.ApiFindCustomerModel;
import com.radisoftplus.utils.UIContent;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

import static com.radisoftplus.utils.SaveLocalStorage.saveToSharedPreferences;

public class SearchChemistAdapter extends RecyclerView.Adapter<SearchChemistAdapter.ViewHolder>{

    public List<ApiFindCustomerModel.Result> dataList = new ArrayList<>();
    Context mContext;
    UIContent uiContent;
    private String item_name;
    private LayoutInflater mInflater;
    String mOrderFromScreen="";

    public void addData(List<ApiFindCustomerModel.Result> lDataList) {
        dataList.addAll(lDataList);
        notifyDataSetChanged();
    }

    public void clearData() {
        dataList.clear();
        notifyDataSetChanged();
    }

    public SearchChemistAdapter( Context mContext, String orderFromScreen) {
        this.mContext = mContext;
        this.mInflater = LayoutInflater.from(mContext);
        uiContent = new UIContent(mContext);
        mOrderFromScreen = orderFromScreen;
    }

    // inflates the row layout from xml when needed
    @Override
    public SearchChemistAdapter.ViewHolder onCreateViewHolder(ViewGroup data, int viewType) {
        View view = mInflater.inflate(R.layout.rview_item, data, false);
        return new SearchChemistAdapter.ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(SearchChemistAdapter.ViewHolder holder, @SuppressLint("RecyclerView") final int position) {

        item_name = dataList.get(position).getPartner()+ "(" + dataList.get(position).getCustomer().get(0).getName1() + ")";
        holder.txvItemName.setText(String.valueOf(item_name));

        holder.cardViewLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mOrderFromScreen.equals("OrderActivity")){
                    Gson gson = new Gson();
                    String customerSalesOrgJson = gson.toJson(dataList.get(position).getCustomer().get(0).getCustomerSalesOrg());
                    saveToSharedPreferences("customer-sales-org", customerSalesOrgJson, mContext);
                    saveToSharedPreferences("find-customer-partner", dataList.get(position).getPartner(), mContext);
                    saveToSharedPreferences("find-customer-customer_name", dataList.get(position).getCustomer().get(0).getName1(), mContext);
                    Intent intent = new Intent(mContext, OrderActivity.class);
                    mContext.startActivity(intent);
                    ((Activity) mContext).finish();
                }else{
                    Gson gson = new Gson();
                    String customerSalesOrgJson = gson.toJson(dataList.get(position).getCustomer().get(0).getCustomerSalesOrg());
                    saveToSharedPreferences("customer-sales-org", customerSalesOrgJson, mContext);
                    saveToSharedPreferences("find-customer-partner", dataList.get(position).getPartner(), mContext);
                    saveToSharedPreferences("find-customer-customer_name", dataList.get(position).getCustomer().get(0).getName1(), mContext);
                    Intent intent = new Intent(mContext, GetCustomerLocationActivity.class);
                    mContext.startActivity(intent);
                    ((Activity) mContext).finish();
                }
            }
        });
    }

    // total number of rows
    @Override
    public int getItemCount() {
        return dataList.size();
    }

    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txvItemName;
        LinearLayout cardViewLayout;

        ViewHolder(View itemView) {
            super(itemView);
            txvItemName = (TextView) itemView.findViewById(R.id.txv_item_name);
            cardViewLayout = itemView.findViewById(R.id.card_view);

        }
    }
}
