package com.radisoftplus.adapter;

import static com.radisoftplus.utils.SaveLocalStorage.saveToSharedPreferences;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;


import com.radisoftplus.R;
import com.radisoftplus.activities.MediaListActivity;
import com.radisoftplus.models.ApiMediaModel;
import com.radisoftplus.utils.UIContent;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class MediaAdapter extends RecyclerView.Adapter<MediaAdapter.ViewHolder>{

    public List<ApiMediaModel.Results> dataList = new ArrayList<>();
    public String url;
    Context mContext;
    UIContent uiContent;
    private String title;
    private LayoutInflater mInflater;

    public MediaAdapter(List<ApiMediaModel.Results> rList, String url, Context mContext) {
        this.dataList = rList;
        this.url = url;
        this.mContext = mContext;
        this.mInflater = LayoutInflater.from(mContext);
        uiContent = new UIContent(mContext);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup data, int viewType) {
        View view = mInflater.inflate(R.layout.media_item, data, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        title  = dataList.get(position).getTitle();
        holder.txvTitle.setText(String.valueOf(title));

        String imageURL = url+ dataList.get(position).getThumbnail();
        Picasso.get()
                .load(imageURL)
                .resize(1000, 1000)
                .centerInside()
                .into(holder.img);

        holder.cardViewLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveToSharedPreferences("media_id_temp",dataList.get(position).getId().toString(), mContext);
                Intent intent = new Intent(mContext, MediaListActivity.class);
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txvTitle;
        ImageView img;
        LinearLayout cardViewLayout;

        ViewHolder(View itemView) {
            super(itemView);
            txvTitle = (TextView) itemView.findViewById(R.id.txv_title);
            img = (ImageView) itemView.findViewById(R.id.img);
            cardViewLayout = itemView.findViewById(R.id.card_view);

        }

    }
}