package com.radisoftplus.adapter;



import static com.radisoftplus.utils.SaveLocalStorage.getValueFromSharedPreferences;
import static com.radisoftplus.utils.SaveLocalStorage.saveToSharedPreferences;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.radisoftplus.R;
import com.radisoftplus.activities.MediaImageDetailsActivity;
import com.radisoftplus.activities.MediaPdfDetailsActivity;
import com.radisoftplus.activities.MediaVideoDetailsActivity;
import com.radisoftplus.models.ApiMediaModel;
import com.radisoftplus.utils.UIContent;
import com.squareup.picasso.Picasso;


import java.util.ArrayList;
import java.util.List;

public class MediaListAdapter extends RecyclerView.Adapter<MediaListAdapter.ViewHolder>{

    public List<ApiMediaModel.MediaList> dataList = new ArrayList<>();
    public String url;
    Context mContext;
    UIContent uiContent;
    private String title;
    private LayoutInflater mInflater;

    public MediaListAdapter(List<ApiMediaModel.MediaList> rList, String url, Context mContext) {
        this.dataList = rList;
        this.url = url;
        this.mContext = mContext;
        this.mInflater = LayoutInflater.from(mContext);
        uiContent = new UIContent(mContext);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup data, int viewType) {
        View view = mInflater.inflate(R.layout.media_item, data, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        if(dataList.get(position).getS_title() == null){
            holder.txvTitle.setVisibility(View.GONE);
        }else{
            holder.txvTitle.setText(dataList.get(position).getS_title());
        }

        String imageURL = "";
        if(dataList.get(position).getFile_thumbnail() == null){
            imageURL = url+ dataList.get(position).getFile_path();
        }else{
            imageURL = url+ dataList.get(position).getFile_thumbnail();
        }
        Picasso.get()
                .load(imageURL)
                .resize(1000, 1000)
                .centerInside()
                .into(holder.img);

        holder.cardViewLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveToSharedPreferences("media_path_temp",dataList.get(position).getFile_path(), mContext);
                saveToSharedPreferences("media_url_temp",url, mContext);
                String media_type   = getValueFromSharedPreferences("media_type_activity_media_temp", mContext);
                if(media_type.equals("pdf")){
                    Intent intent = new Intent(mContext, MediaPdfDetailsActivity.class);
                    mContext.startActivity(intent);
                }else if(media_type.equals("image")){
                    Intent intent = new Intent(mContext, MediaImageDetailsActivity.class);
                    mContext.startActivity(intent);
                }else if(media_type.equals("video")){
                    Intent intent = new Intent(mContext, MediaVideoDetailsActivity.class);
                    mContext.startActivity(intent);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txvTitle;
        ImageView img;
        LinearLayout cardViewLayout;

        ViewHolder(View itemView) {
            super(itemView);
            txvTitle = (TextView) itemView.findViewById(R.id.txv_title);
            img = (ImageView) itemView.findViewById(R.id.img);
            cardViewLayout = itemView.findViewById(R.id.card_view);

        }

    }
}