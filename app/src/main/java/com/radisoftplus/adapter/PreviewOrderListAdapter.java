package com.radisoftplus.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.radisoftplus.R;
import com.radisoftplus.activities.OrderListActivity;
import com.radisoftplus.models.OrderListModel;
import com.radisoftplus.utils.UIContent;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

import static com.radisoftplus.utils.SaveLocalStorage.saveToSharedPreferences;

public class PreviewOrderListAdapter extends RecyclerView.Adapter<PreviewOrderListAdapter.ViewHolder>{
    public List<OrderListModel> dataList = new ArrayList<>();
    Context mContext;
    UIContent uiContent;
    private String item_name;
    private String item_details;
    private LayoutInflater mInflater;

    public PreviewOrderListAdapter(List<OrderListModel> rList, Context mContext) {
        this.dataList = rList;
        this.mContext = mContext;
        this.mInflater = LayoutInflater.from(mContext);
        uiContent = new UIContent(mContext);
    }

    // inflates the row layout from xml when needed
    @Override
    public PreviewOrderListAdapter.ViewHolder onCreateViewHolder(ViewGroup data, int viewType) {
        View view = mInflater.inflate(R.layout.rview_preview_order_item, data, false);
        return new PreviewOrderListAdapter.ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(PreviewOrderListAdapter.ViewHolder holder, final int position) {

        item_name = dataList.get(position).getMaterial_name();
        item_details =  dataList.get(position).getUnit_tp() + "+" + dataList.get(position).getUnit_vat() + "x" + dataList.get(position).getQty() + " = " + String.format("%.2f", ((Float.parseFloat(dataList.get(position).getUnit_tp())+Float.parseFloat(dataList.get(position).getUnit_vat())) * dataList.get(position).getQty()));
        holder.txvItemName.setText(String.valueOf(item_name));
        holder.txvItemDetails.setText(String.valueOf(item_details));

    }

    // total number of rows
    @Override
    public int getItemCount() {
        return dataList.size();
    }

    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txvItemName;
        TextView txvItemDetails;

        ViewHolder(View itemView) {
            super(itemView);
            txvItemName = (TextView) itemView.findViewById(R.id.tv_item_name);
            txvItemDetails = (TextView) itemView.findViewById(R.id.tv_item_details);
        }

    }
}
