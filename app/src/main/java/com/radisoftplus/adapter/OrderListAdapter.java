package com.radisoftplus.adapter;

import static android.content.ContentValues.TAG;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.text.Editable;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.radisoftplus.R;
import com.radisoftplus.activities.OrderListActivity;
import com.radisoftplus.models.ApiFindMaterialModel;
import com.radisoftplus.models.OrderListModel;
import com.radisoftplus.utils.UIContent;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

import static com.radisoftplus.utils.SaveLocalStorage.saveToSharedPreferences;

public class OrderListAdapter extends RecyclerView.Adapter<OrderListAdapter.ViewHolder>{
    public List<OrderListModel> dataList = new ArrayList<>();
    Context mContext;
    UIContent uiContent;
    private String item_name;
    private String item_details;
    private LayoutInflater mInflater;

    public OrderListAdapter(List<OrderListModel> rList, Context mContext) {
        this.dataList = rList;
        this.mContext = mContext;
        this.mInflater = LayoutInflater.from(mContext);
        uiContent = new UIContent(mContext);
    }

    // inflates the row layout from xml when needed
    @Override
    public OrderListAdapter.ViewHolder onCreateViewHolder(ViewGroup data, int viewType) {
        View view = mInflater.inflate(R.layout.rview_order_list_item, data, false);
        return new OrderListAdapter.ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(OrderListAdapter.ViewHolder holder, final int position) {
        item_name = dataList.get(position).getMaterial_name();
        item_details =  "TP: "+ dataList.get(position).getUnit_tp() + " Vat: " + dataList.get(position).getUnit_vat();
        holder.txvProductName.setText(String.valueOf(item_name));
        holder.txvProductDetails.setText(String.valueOf(item_details));
        holder.txvCountQty.setText(String.valueOf(dataList.get(position).getQty()));

        holder.txvCountQty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alert = new AlertDialog.Builder(mContext);
                final EditText edittext = new EditText(mContext);
                edittext.setInputType(InputType.TYPE_CLASS_NUMBER);
                edittext.setGravity(Gravity.CENTER);

                alert.setMessage(item_name+"\n"+item_details);
                alert.setTitle("Enter Qty");

                alert.setView(edittext);

                alert.setPositiveButton("Update", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        if(edittext.getText().toString().equals("0") || edittext.getText().toString().equals("")){
                            Toast toast = Toast.makeText(mContext, "Qty not empty or 0", Toast.LENGTH_SHORT);
                            toast.show();
                        }else{
                            dataList.get(holder.getLayoutPosition()).setQty(Integer.parseInt(edittext.getText().toString()));
                            notifyItemRangeChanged(holder.getPosition(), dataList.size());
                            GsonBuilder gsonBuilder = new GsonBuilder();
                            Gson gson = gsonBuilder.create();
                            String JSONObjectString = gson.toJson(dataList);
                            saveToSharedPreferences("order-list",JSONObjectString, mContext);
                            if (mContext instanceof OrderListActivity) {
                                ((OrderListActivity)mContext).totalCal();
                            }
                        }
                    }
                });

                alert.setNegativeButton("Close", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        // what ever you want to do with No option.
                    }
                });

                alert.show();
            }
        });

        holder.imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dataList.remove(holder.getPosition());
                notifyItemRemoved(holder.getPosition());
                notifyItemRangeChanged(holder.getPosition(), dataList.size());
                GsonBuilder gsonBuilder = new GsonBuilder();
                Gson gson = gsonBuilder.create();
                String JSONObjectString = gson.toJson(dataList);
                saveToSharedPreferences("order-list",JSONObjectString, mContext);
                if (mContext instanceof OrderListActivity) {
                    ((OrderListActivity)mContext).totalDeleteCal(holder.getPosition());
                }
            }
        });

        holder.impPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dataList.get(holder.getLayoutPosition()).setQty(dataList.get(holder.getLayoutPosition()).getQty()+1);
                notifyItemRangeChanged(holder.getPosition(), dataList.size());
                GsonBuilder gsonBuilder = new GsonBuilder();
                Gson gson = gsonBuilder.create();
                String JSONObjectString = gson.toJson(dataList);
                saveToSharedPreferences("order-list",JSONObjectString, mContext);
                if (mContext instanceof OrderListActivity) {
                    ((OrderListActivity)mContext).totalCal();
                }

            }
        });

        holder.imgMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ((dataList.get(holder.getLayoutPosition()).getQty()-1) != 0){
                    dataList.get(holder.getLayoutPosition()).setQty(dataList.get(holder.getLayoutPosition()).getQty()-1);
                    notifyItemRangeChanged(holder.getPosition(), dataList.size());
                    GsonBuilder gsonBuilder = new GsonBuilder();
                    Gson gson = gsonBuilder.create();
                    String JSONObjectString = gson.toJson(dataList);
                    saveToSharedPreferences("order-list",JSONObjectString, mContext);
                    if (mContext instanceof OrderListActivity) {
                        ((OrderListActivity)mContext).totalCal();
                    }
                }
            }
        });
    }

    // total number of rows
    @Override
    public int getItemCount() {
        return dataList.size();
    }

    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txvProductName;
        TextView txvProductDetails;
        TextView txvCountQty;
        ImageView imgDelete;
        ImageView impPlus;
        ImageView imgMinus;

        ViewHolder(View itemView) {
            super(itemView);
            txvProductName = (TextView) itemView.findViewById(R.id.txv_product_name);
            txvProductDetails = (TextView) itemView.findViewById(R.id.txv_product_details);
            imgDelete = (ImageView) itemView.findViewById(R.id.img_delete);
            impPlus = (ImageView) itemView.findViewById(R.id.img_plus);
            imgMinus = (ImageView) itemView.findViewById(R.id.img_minus);
            txvCountQty = (TextView) itemView.findViewById(R.id.tv_count_qty);
        }

    }
}
