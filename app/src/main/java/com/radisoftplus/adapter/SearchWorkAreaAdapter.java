package com.radisoftplus.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.radisoftplus.R;
import com.radisoftplus.activities.OrderActivity;
import com.radisoftplus.activities.SearchWorkAreaActivity;
import com.radisoftplus.models.ApiFindWorkAreaModel;
import com.radisoftplus.utils.UIContent;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

import static com.radisoftplus.utils.SaveLocalStorage.saveToSharedPreferences;

public class SearchWorkAreaAdapter extends RecyclerView.Adapter<SearchWorkAreaAdapter.ViewHolder>{

    public List<ApiFindWorkAreaModel.Result> dataList = new ArrayList<>();
    Context mContext;
    UIContent uiContent;
    private String item_name;
    private LayoutInflater mInflater;

    public SearchWorkAreaAdapter(List<ApiFindWorkAreaModel.Result> rList, Context mContext) {
        this.dataList = rList;
        this.mContext = mContext;
        this.mInflater = LayoutInflater.from(mContext);
        uiContent = new UIContent(mContext);
    }

    // inflates the row layout from xml when needed
    @Override
    public SearchWorkAreaAdapter.ViewHolder onCreateViewHolder(ViewGroup data, int viewType) {
        View view = mInflater.inflate(R.layout.rview_item, data, false);
        return new SearchWorkAreaAdapter.ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(SearchWorkAreaAdapter.ViewHolder holder, final int position) {

        item_name  = dataList.get(position).getWorkAreaT() + "(" + dataList.get(position).getName() + ")";
        holder.txvItemName.setText(String.valueOf(item_name));

        holder.cardViewLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveToSharedPreferences("find-word_area-work_area",dataList.get(position).getWorkAreaT(), mContext);
                saveToSharedPreferences("find-word_area-address",dataList.get(position).getName(), mContext);
                Intent intent = new Intent(mContext, OrderActivity.class);
                mContext.startActivity(intent);
            }
        });
    }

    // total number of rows
    @Override
    public int getItemCount() {
        return dataList.size();
    }

    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txvItemName;
        LinearLayout cardViewLayout;

        ViewHolder(View itemView) {
            super(itemView);
            txvItemName = (TextView) itemView.findViewById(R.id.txv_item_name);
            cardViewLayout = itemView.findViewById(R.id.card_view);

        }

    }

}
