package com.radisoftplus.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.radisoftplus.R;
import com.radisoftplus.activities.VisitPlanDetailsActivity;
import com.radisoftplus.models.ApiTodayVisitPlanModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

public class VisitPlanListAdapter extends RecyclerView.Adapter<VisitPlanListAdapter.ViewHolder>{

    public List<ApiTodayVisitPlanModel.Result> dataList = new ArrayList<>();
    Context mContext;
    private String doctor_name;
    private String speciality_description;
    private String category;
    private LayoutInflater mInflater;

    public VisitPlanListAdapter(List<ApiTodayVisitPlanModel.Result> _list, Context mContext) {
        this.dataList = _list;
        this.mContext = mContext;
        this.mInflater = LayoutInflater.from(mContext);
    }

    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup list, int viewType) {
        View view = mInflater.inflate(R.layout.visit_list_item, list, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, @SuppressLint("RecyclerView") final int position) {

        doctor_name  = dataList.get(position).getChamberDetails().getDoctorDetails().getDoctorName1() + " " + dataList.get(position).getChamberDetails().getDoctorDetails().getDoctorName2();
        speciality_description = dataList.get(position).getChamberDetails().getSpecialityDetails().getSpecialityDescription();

        holder.txvDoctorName.setText(String.valueOf(doctor_name).trim());
        holder.txvSpeciality.setText("Speciality: " + speciality_description);
        holder.txvCategory.setText("Category: A");

        String gadgetListText = "";
        for (int i=0;i<dataList.get(position).getMonthlyVisitPlanList().size();i++){
            if(i == 0){
                holder.txvBrand1.setText(dataList.get(position).getMonthlyVisitPlanList().get(i).getBrandName());
            }
            if(i == 1){
                holder.txvBrand2.setText(dataList.get(position).getMonthlyVisitPlanList().get(i).getBrandName());
            }
            if(i == 2){
                holder.txvBrand3.setText(dataList.get(position).getMonthlyVisitPlanList().get(i).getBrandName());
            }
            if(i == 3){
                holder.txvBrand4.setText(dataList.get(position).getMonthlyVisitPlanList().get(i).getBrandName());
            }
            gadgetListText = gadgetListText + ", " + dataList.get(position).getMonthlyVisitPlanList().get(i).getGadgetName();
        }
        holder.txvGadget.setText(gadgetListText.substring(2));

        holder.cardViewLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, VisitPlanDetailsActivity.class);
                intent.putExtra("doctor_name",dataList.get(position).getChamberDetails().getDoctorDetails().getDoctorName1() + " " + dataList.get(position).getChamberDetails().getDoctorDetails().getDoctorName2());
                intent.putExtra("speciality_description",dataList.get(position).getChamberDetails().getSpecialityDetails().getSpecialityDescription());
                intent.putExtra("category","A");

                Gson gson = new Gson();
                String myJson = gson.toJson(dataList.get(position).getMonthlyVisitPlanList());
                intent.putExtra("monthly_visit_plan_list", myJson);

                mContext.startActivity(intent);
            }
        });
    }

    // total number of rows
    @Override
    public int getItemCount() {
        return dataList.size();
    }

    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txvDoctorName, txvSpeciality, txvCategory;
        TextView txvBrand1, txvBrand2, txvBrand3, txvBrand4;
        TextView txvGadget;
        CardView cardViewLayout;

        ViewHolder(View itemView) {
            super(itemView);
            txvDoctorName = (TextView) itemView.findViewById(R.id.txv_doctor_name);
            txvSpeciality = (TextView) itemView.findViewById(R.id.txv_speciality);
            txvCategory = (TextView) itemView.findViewById(R.id.txv_category);
            txvBrand1 = (TextView) itemView.findViewById(R.id.txv_brand1);
            txvBrand2 = (TextView) itemView.findViewById(R.id.txv_brand2);
            txvBrand3 = (TextView) itemView.findViewById(R.id.txv_brand3);
            txvBrand4 = (TextView) itemView.findViewById(R.id.txv_brand4);
            txvGadget = (TextView) itemView.findViewById(R.id.txv_gadget_list);
            cardViewLayout = itemView.findViewById(R.id.card_view);

        }
    }
}
