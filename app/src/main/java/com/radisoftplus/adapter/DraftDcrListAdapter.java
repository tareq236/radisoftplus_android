package com.radisoftplus.adapter;

import static com.radisoftplus.utils.SaveLocalStorage.saveToSharedPreferences;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.radisoftplus.R;

import com.radisoftplus.activities.DcrDraftActivity;

import com.radisoftplus.activities.DcrScreenOneActivity;
import com.radisoftplus.models.DraftDcrModel;

import com.radisoftplus.utils.UIContent;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import androidx.recyclerview.widget.RecyclerView;

public class DraftDcrListAdapter extends RecyclerView.Adapter<DraftDcrListAdapter.ViewHolder>{
    public List<DraftDcrModel> dataList = new ArrayList<>();
    Context mContext;
    UIContent uiContent;
    private String call_date;
    private String doctor_name;
    private String dr_child_id;
    private LayoutInflater mInflater;

    public DraftDcrListAdapter(List<DraftDcrModel> rList, Context mContext) {
        this.dataList = rList;
        this.mContext = mContext;
        this.mInflater = LayoutInflater.from(mContext);
        uiContent = new UIContent(mContext);
    }

    // inflates the row layout from xml when needed
    @Override
    public DraftDcrListAdapter.ViewHolder onCreateViewHolder(ViewGroup data, int viewType) {
        View view = mInflater.inflate(R.layout.rview_draft_order_item, data, false);
        return new DraftDcrListAdapter.ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(DraftDcrListAdapter.ViewHolder holder, final int position) {

        String[] callDateArray = dataList.get(position).getCall_date().split("-", 3);
        String callDate = callDateArray[1]+"/"+callDateArray[2]+"/"+callDateArray[0];

        String inputCreatedAtDate = dataList.get(position).getCreated_at();
        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        inputFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        SimpleDateFormat outputFormat = new SimpleDateFormat("dd/MMM/yyyy HH:mm:ss");
        try {
            Date createdAtDate = inputFormat.parse(inputCreatedAtDate);
            String outputCreatedAtDate = outputFormat.format(createdAtDate);
            holder.txvCreatedAt.setText("Created at: "+outputCreatedAtDate);
        } catch (ParseException e) {
            holder.txvCreatedAt.setText("Created at: "+dataList.get(position).getCreated_at());
            e.printStackTrace();
        }

        String inputOrderDate = dataList.get(position).getCreated_at();
        SimpleDateFormat inputOrderFormat = new SimpleDateFormat("yyyy-MM-dd");
        inputOrderFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        SimpleDateFormat outputOrderFormat = new SimpleDateFormat("dd/MMM/yyyy");
        try {
            Date orderDate = inputFormat.parse(inputOrderDate);
            String outputOrderDate = outputOrderFormat.format(orderDate);
            holder.txvDcrDate.setText("Call date: " + outputOrderDate);
        } catch (ParseException e) {
            holder.txvDcrDate.setText("Call date: " + callDate);
            e.printStackTrace();
        }

        holder.txvDcrId.setText("ID: " + dataList.get(position).getId());
        holder.txvEmpName.setText("Doctor name: "+dataList.get(position).getDoctor_name() + "("+dataList.get(position).getDr_child_id()+")");
        holder.txvChemistName.setText("Sessions: "+dataList.get(position).getSessions());
        holder.cardViewLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, DcrScreenOneActivity.class);
                intent.putExtra("is_draft", true);

                String[] callDateArray = dataList.get(position).getCall_date().split("-", 3);
                String callDate = callDateArray[1]+"/"+callDateArray[2]+"/"+callDateArray[0];

                saveToSharedPreferences("dcr-draft-id",dataList.get(position).getId().toString(), mContext);
                saveToSharedPreferences("dcr-call-date",callDate, mContext);
                saveToSharedPreferences("dcr-sessions",dataList.get(position).getSessions(), mContext);
                saveToSharedPreferences("dcr-chamber-type",dataList.get(position).getChamber_type(), mContext);
                saveToSharedPreferences("dcr-call-type",dataList.get(position).getCall_type(), mContext);
                saveToSharedPreferences("dcr-dr-child-id",dataList.get(position).getDr_child_id(), mContext);
                saveToSharedPreferences("dcr-remarks",dataList.get(position).getRemarks(), mContext);
                saveToSharedPreferences("find-doctor-name", dataList.get(position).getDoctor_name(), mContext);
                saveToSharedPreferences("find-dr-child-id", dataList.get(position).getDr_child_id(), mContext);
                saveToSharedPreferences("br-list",dataList.get(position).getJson_details(), mContext);

                mContext.startActivity(intent);
                ((Activity) mContext).finish();
            }
        });

        holder.imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int id = dataList.get(position).getId();
                dataList.remove(holder.getPosition());
                notifyItemRemoved(holder.getPosition());
                notifyItemRangeChanged(holder.getPosition(), dataList.size());
                if (mContext instanceof DcrDraftActivity) {
                    ((DcrDraftActivity)mContext).deleteItem(id);
                }
            }
        });
    }

    // total number of rows
    @Override
    public int getItemCount() {
        return dataList.size();
    }

    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txvDcrId;
        TextView txvDcrDate;
        TextView txvEmpName;
        TextView txvChemistName;
        TextView txvCreatedAt;
        ImageView imgDelete;
        LinearLayout cardViewLayout;

        ViewHolder(View itemView) {
            super(itemView);
            txvDcrId = (TextView) itemView.findViewById(R.id.txv_order_id);
            txvDcrDate = (TextView) itemView.findViewById(R.id.txv_order_date);
            txvEmpName = (TextView) itemView.findViewById(R.id.txv_employee_name);
            txvChemistName = (TextView) itemView.findViewById(R.id.txv_chemist_name);
            txvCreatedAt = (TextView) itemView.findViewById(R.id.txv_created_at);
            imgDelete = (ImageView) itemView.findViewById(R.id.img_delete);
            cardViewLayout = itemView.findViewById(R.id.card_view);
        }

    }
}
