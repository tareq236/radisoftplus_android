package com.radisoftplus.adapter;


import static com.radisoftplus.utils.SaveLocalStorage.getValueFromSharedPreferences;
import static com.radisoftplus.utils.SaveLocalStorage.saveToSharedPreferences;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;


import com.radisoftplus.R;
import com.radisoftplus.activities.TrainingProductDetailsActivity;
import com.radisoftplus.activities.TrainingProductDetailsCommonActivity;
import com.radisoftplus.models.ApiTrainingProductList;
import com.radisoftplus.utils.UIContent;

import java.util.ArrayList;
import java.util.List;

public class TrainingProductListAdapter extends RecyclerView.Adapter<TrainingProductListAdapter.ViewHolder>{

    public List<ApiTrainingProductList.Results> dataList = new ArrayList<>();
    Context mContext;
    UIContent uiContent;
    private String item_name;
    private LayoutInflater mInflater;

    public TrainingProductListAdapter(List<ApiTrainingProductList.Results> rList, Context mContext) {
        this.dataList = rList;
        this.mContext = mContext;
        this.mInflater = LayoutInflater.from(mContext);
        uiContent = new UIContent(mContext);
    }

    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup data, int viewType) {
        View view = mInflater.inflate(R.layout.rview_item, data, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        item_name  = dataList.get(position).getTrainingProductName();
        String is_new  = dataList.get(position).getIs_new();
        holder.txvItemName.setText(String.valueOf(item_name));
        if(is_new.equals("New")){
            holder.imgNew.setVisibility(View.VISIBLE);
        }else{
            holder.imgNew.setVisibility(View.GONE);
        }

        holder.cardViewLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveToSharedPreferences("training_product_id",dataList.get(position).getId().toString(), mContext);
                saveToSharedPreferences("training_product_name",dataList.get(position).getTrainingProductName(), mContext);
                String go_to_training = getValueFromSharedPreferences("go_to_training", mContext);
                if(go_to_training.equals("go_to_brands_training")) {
                    Intent intent = new Intent(mContext, TrainingProductDetailsActivity.class);
                    intent.putExtra("id",dataList.get(position).getId().toString());
                    intent.putExtra("item_name",dataList.get(position).getTrainingProductName());
                    mContext.startActivity(intent);
                }else{
                    Intent intent = new Intent(mContext, TrainingProductDetailsCommonActivity.class);
                    intent.putExtra("id",dataList.get(position).getId().toString());
                    intent.putExtra("item_name",dataList.get(position).getTrainingProductName());
                    mContext.startActivity(intent);
                }


            }
        });
    }

    // total number of rows
    @Override
    public int getItemCount() {
        return dataList.size();
    }

    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txvItemName;
        LinearLayout cardViewLayout;
        ImageView imgNew;

        ViewHolder(View itemView) {
            super(itemView);
            txvItemName = (TextView) itemView.findViewById(R.id.txv_item_name);
            imgNew = (ImageView) itemView.findViewById(R.id.img_new);
            cardViewLayout = itemView.findViewById(R.id.card_view);

        }

    }

}
