package com.radisoftplus.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.text.Editable;
import android.text.InputType;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.radisoftplus.R;
import com.radisoftplus.activities.OrderListActivity;
import com.radisoftplus.models.ApiFindMaterialModel;
import com.radisoftplus.models.BrListModel;
import com.radisoftplus.utils.UIContent;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

import static com.radisoftplus.utils.SaveLocalStorage.saveToSharedPreferences;

public class BrListAdapter extends RecyclerView.Adapter<BrListAdapter.ViewHolder>{
    public List<BrListModel> dataList = new ArrayList<>();
    Context mContext;
    UIContent uiContent;
    private String brand_name;
    private String gadget_name;
    private LayoutInflater mInflater;

    public BrListAdapter(List<BrListModel> rList, Context mContext) {
        this.dataList = rList;
        this.mContext = mContext;
        this.mInflater = LayoutInflater.from(mContext);
        uiContent = new UIContent(mContext);
    }

    // inflates the row layout from xml when needed
    @Override
    public BrListAdapter.ViewHolder onCreateViewHolder(ViewGroup data, int viewType) {
        View view = mInflater.inflate(R.layout.rview_dcr_list_item, data, false);
        return new BrListAdapter.ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(BrListAdapter.ViewHolder holder, final int position) {

        brand_name = dataList.get(position).getBrand_name();
        gadget_name =  dataList.get(position).getBr_name();
        holder.txvBrandName.setText(String.valueOf(brand_name));
        holder.txvGadgetName.setText(String.valueOf(gadget_name));

        holder.imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dataList.remove(holder.getPosition());
                notifyItemRemoved(holder.getPosition());
                notifyItemRangeChanged(holder.getPosition(), dataList.size());
                GsonBuilder gsonBuilder = new GsonBuilder();
                Gson gson = gsonBuilder.create();
                String JSONObjectString = gson.toJson(dataList);
                saveToSharedPreferences("br-list",JSONObjectString, mContext);
            }
        });

    }

    // total number of rows
    @Override
    public int getItemCount() {
        return dataList.size();
    }

    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txvBrandName;
        TextView txvGadgetName;
        ImageView imgDelete;

        ViewHolder(View itemView) {
            super(itemView);
            txvBrandName = (TextView) itemView.findViewById(R.id.txv_brand_name);
            txvGadgetName = (TextView) itemView.findViewById(R.id.txv_gadget_name);
            imgDelete = (ImageView) itemView.findViewById(R.id.img_delete);
        }

    }
}
