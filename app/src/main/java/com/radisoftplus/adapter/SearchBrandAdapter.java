package com.radisoftplus.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.radisoftplus.R;
import com.radisoftplus.activities.DcrScreenTwoActivity;
import com.radisoftplus.activities.OrderListActivity;
import com.radisoftplus.models.ApiFindMaterialModel;
import com.radisoftplus.models.BrListModel;
import com.radisoftplus.models.OrderListModel;
import com.radisoftplus.utils.UIContent;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import androidx.recyclerview.widget.RecyclerView;

import static com.radisoftplus.utils.SaveLocalStorage.getValueFromSharedPreferences;
import static com.radisoftplus.utils.SaveLocalStorage.saveToSharedPreferences;

public class SearchBrandAdapter extends RecyclerView.Adapter<SearchBrandAdapter.ViewHolder>{
    public List<ApiFindMaterialModel.Result> dataList = new ArrayList<>();
    Context mContext;
    UIContent uiContent;
    private String item_name;
    private LayoutInflater mInflater;
    List<BrListModel> bList = new ArrayList<>();

    public SearchBrandAdapter(List<ApiFindMaterialModel.Result> rList, Context mContext) {
        this.dataList = rList;
        this.mContext = mContext;
        this.mInflater = LayoutInflater.from(mContext);
        uiContent = new UIContent(mContext);

        String br_list = getValueFromSharedPreferences("br-list", mContext);
        if(br_list != null){
            try {
                JSONArray mainObject = new JSONArray(br_list);
                for (int i = 0; i < mainObject.length(); i++) {
                    JSONObject object = mainObject.getJSONObject(i);
                    BrListModel brListModel = new BrListModel();
                    brListModel.setBrand_name(object.getString("brand_name"));
                    brListModel.setBr_name(object.getString("br_name"));
                    brListModel.setBr_code(object.getString("br_code"));
                    brListModel.setQty(object.getInt("qty"));
                    bList.add(brListModel);
                }
            } catch (Exception e) {
                Log.w("exception-verify data: ", e.getMessage());
            }
        }

    }

    // inflates the row layout from xml when needed
    @Override
    public SearchBrandAdapter.ViewHolder onCreateViewHolder(ViewGroup data, int viewType) {
        View view = mInflater.inflate(R.layout.rview_item, data, false);
        return new SearchBrandAdapter.ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(SearchBrandAdapter.ViewHolder holder, final int position) {

        item_name = dataList.get(position).getBrandName();
        holder.txvProductName.setText(String.valueOf(item_name));

        for (int i=0;i<bList.size();i++){
            if(bList.get(i).getBrand_name().equals(dataList.get(position).getBrandName())){
                holder.rvContainView.setBackgroundColor(Color.parseColor("#FFE68CAB"));
                holder.txvProductName.setBackgroundColor(Color.parseColor("#FFE68CAB"));
            }
        }

        holder.cardViewLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (bList.size() == 0){
                    saveToSharedPreferences("dcr-select-brand-name", dataList.get(position).getBrandName(), mContext);
                    Intent intent = new Intent(mContext, DcrScreenTwoActivity.class);
                    mContext.startActivity(intent);
                    ((Activity) mContext).finish();
                }else{
                    Boolean isAvailable = false;

                    for (int i=0;i<bList.size();i++){
                        if(bList.get(i).getBrand_name().equals(dataList.get(position).getBrandName())){
                            isAvailable = true;
                            break;
                        }
                    }
                    if(!isAvailable){
                        saveToSharedPreferences("dcr-select-brand-name", dataList.get(position).getBrandName(), mContext);
                        Intent intent = new Intent(mContext, DcrScreenTwoActivity.class);
                        mContext.startActivity(intent);
                        ((Activity) mContext).finish();
                    }
                }
            }
        });
    }

    // total number of rows
    @Override
    public int getItemCount() {
        return dataList.size();
    }

    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txvProductName;
        LinearLayout cardViewLayout;
        RelativeLayout rvContainView;

        ViewHolder(View itemView) {
            super(itemView);
            txvProductName = (TextView) itemView.findViewById(R.id.txv_item_name);
            cardViewLayout = itemView.findViewById(R.id.card_view);
            rvContainView = itemView.findViewById(R.id.rv_contain_view);
        }
    }
}
